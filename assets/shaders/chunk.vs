#version 430 core

layout (location = 0) in vec4 position_and_normal;
layout (location = 1) in vec4 texels;

out vec3 FragPosition;

out vec3 LightColor;

out vec2 TexCoord;
out float TexIndex;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

uniform vec3 ambient = vec3(0.7, 0.7, 0.7);
uniform vec3 diffuse_light_pos = vec3(75.0, 25.0, 100.0);
uniform vec3 diffuse_albedo = vec3(0.8, 0.8, 0.8);

uniform uint enable_lighting = 0;

void main() {
    vec4 position = view * model * vec4(position_and_normal.xyz, 1.0f);
    FragPosition = vec3(model * vec4(position_and_normal.xyz, 1.0f));

    uint normal_bits = uint(position_and_normal.w);
    int normal_sign = 1 - int((normal_bits & 0x00000008) >> 3) * 2;
    vec3 normal = vec3(
        normal_sign * int(uint(normal_bits & 0x00000004) >> 2),
        normal_sign * int(uint(normal_bits & 0x00000002) >> 1),
        normal_sign * int(normal_bits & 0x00000001));

    vec3 light = normalize(diffuse_light_pos - position.xyz);
    vec3 diffuse = max(dot(normal, diffuse_light_pos), 0.0) * diffuse_albedo;

    if (enable_lighting == 1) {
        LightColor = ambient + diffuse;
    } else {
        LightColor = vec3(1.0, 1.0, 1.0);
    }

    TexCoord = texels.xy / 255.0f;
    TexIndex = texels.z;

    gl_Position = projection * position;
}
