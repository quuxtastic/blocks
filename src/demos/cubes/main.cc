#include "platform/platform.hh"

#include <SOIL/SOIL.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "platform/window.hh"
#include "render/shader.hh"
#include "render/texture.hh"
#include "render/mesh.hh"
#include "render/buffer.hh"
#include "render/camera.hh"

class BlocksDemo : public Platform_interface {
public:
    static const int WINDOW_WIDTH = 800;
    static const int WINDOW_HEIGHT = 600;

    void do_pause(bool mode) {
        paused = mode;
        if (mode) {
            main_window().free_mouse();
            main_window().set_title("blocks (paused)");
        } else {
            main_window().grab_mouse();
            main_window().set_title("blocks");
        }
    }

    virtual void setup() {
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

#ifdef DEBUG
        glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);
#endif

        new Window(WINDOW_WIDTH, WINDOW_HEIGHT, "blocks");
        main_window().grab_mouse();

        main_window().set_context();

        enable_gl_debug();

        shader = Shader::create_from_file("assets/shaders/main.program");

        auto texture_props = [] (GLuint, GLuint type) {
            glTexParameteri(type, GL_TEXTURE_WRAP_S, GL_REPEAT);
            glTexParameteri(type, GL_TEXTURE_WRAP_T, GL_REPEAT);
            glTexParameteri(type, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            glTexParameteri(type, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        };

        bricks = Texture::d2(GL_TEXTURE_2D, GL_RGB,
            "assets/bricks.jpg", SOIL_LOAD_RGB,
            GL_RGB, GL_UNSIGNED_BYTE, texture_props);
        happyface = Texture::d2(GL_TEXTURE_2D, GL_RGB,
            "assets/awesomeface.png", SOIL_LOAD_RGB,
            GL_RGB, GL_UNSIGNED_BYTE, texture_props);

        static const float verts[] = {
            -0.5f, -0.5f, -0.5f,  0.0f, 0.0f,
             0.5f, -0.5f, -0.5f,  1.0f, 0.0f,
             0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
             0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
            -0.5f,  0.5f, -0.5f,  0.0f, 1.0f,
            -0.5f, -0.5f, -0.5f,  0.0f, 0.0f,

            -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
             0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
             0.5f,  0.5f,  0.5f,  1.0f, 1.0f,
             0.5f,  0.5f,  0.5f,  1.0f, 1.0f,
            -0.5f,  0.5f,  0.5f,  0.0f, 1.0f,
            -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,

            -0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
            -0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
            -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
            -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
            -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
            -0.5f,  0.5f,  0.5f,  1.0f, 0.0f,

             0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
             0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
             0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
             0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
             0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
             0.5f,  0.5f,  0.5f,  1.0f, 0.0f,

            -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
             0.5f, -0.5f, -0.5f,  1.0f, 1.0f,
             0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
             0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
            -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
            -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,

            -0.5f,  0.5f, -0.5f,  0.0f, 1.0f,
             0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
             0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
             0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
            -0.5f,  0.5f,  0.5f,  0.0f, 0.0f,
            -0.5f,  0.5f, -0.5f,  0.0f, 1.0f
        };

        Buffer buf = Buffer::create(GL_ARRAY_BUFFER, verts,
            36 * 5 * sizeof(float), GL_STATIC_DRAW);

        cube = Mesh::create([&](Mesh mesh) {
            mesh.attach(buf);

            int stride = 5 * sizeof(float);
            mesh.set_attr(0, 3, GL_FLOAT, GL_FALSE, stride, 0);
            mesh.set_attr(1, 2, GL_FLOAT, GL_FALSE, stride, 3 * sizeof(float));
        });

        main_window().on_key([this] (int key, int, int action, int) {
            if (key == GLFW_KEY_Q && action == GLFW_PRESS) {
                main_window().close();
            }
            if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
                this->do_pause(!this->paused);
            }
        });

        main_window().on_resize([this] (int, int) {
            auto fbsize = main_window().get_fb_size();
            glViewport(0, 0, fbsize.first, fbsize.second);

            this->camera.set_viewport(fbsize.first, fbsize.second);
        });

        main_window().on_mouse([this] (double dx, double dy) {
            if (!this->paused) {
                this->camera.yaw(dx);
                this->camera.pitch(-dy);
            }
        });

        main_window().on_focus([this] (int focused) {
            if (!focused) this->do_pause(true);
        });

        glViewport(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);
        camera.set_viewport(WINDOW_WIDTH, WINDOW_HEIGHT);
        camera.set_zoom_speed(50.0f);

        glEnable(GL_DEPTH_TEST);

        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
    }

    virtual void cleanup() {
    }

    virtual void main_loop() {
        float last_time = glfwGetTime();

        while (!main_window().should_close()) {
            float cur_time = glfwGetTime();
            float dtime = cur_time - last_time;
            last_time = cur_time;

            main_window().poll_events();

            if (main_window().key_state(GLFW_KEY_W)) camera.move_z(dtime);
            if (main_window().key_state(GLFW_KEY_S)) camera.move_z(-dtime);
            if (main_window().key_state(GLFW_KEY_A)) camera.move_x(-dtime);
            if (main_window().key_state(GLFW_KEY_D)) camera.move_x(dtime);

            if (main_window().key_state(GLFW_KEY_Z)) {
                camera.zoom(dtime);
            } else {
                camera.zoom(-dtime);
            }

            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

            shader.bind();

            shader.set_uniform("view", camera.get_view());
            shader.set_uniform("projection", camera.get_projection());

            bricks.bind(0);
            shader.set_uniform("firstTexture", 0);
            happyface.bind(1);
            shader.set_uniform("secondTexture", 1);

            cube.bind();

            const glm::vec3 cubes[] = {
                { 0.0f,  0.0f,  0.0f},
                { 2.0f,  5.0f, -15.0f},
                {-1.5f, -2.2f, -2.5f},
                {-3.8f, -2.0f, -12.3f},
                { 2.4f, -0.4f, -3.5f},
                {-1.7f,  3.0f, -7.5f},
                { 1.3f, -2.0f, -2.5f},
                { 1.5f,  2.0f, -2.5f},
                { 1.5f,  0.2f, -1.5f},
                {-1.3f,  1.0f, -1.5f}
            };
            for (size_t i = 0; i < 10; ++i) {
                glm::mat4 model = glm::translate(glm::mat4(1.f), cubes[i]);
                float angle = glm::radians(20.0f * (i+1) * glfwGetTime());
                model = glm::rotate(model, angle, glm::vec3(1.0f, 0.3f, 0.5f));
                shader.set_uniform("model", model);

                glDrawArrays(GL_TRIANGLES, 0, 36);
            }

            main_window().swap();
        }
    }
private:
    Shader shader;
    Texture bricks, happyface;

    Mesh cube;
    Camera camera;

    bool paused = false;
};
Platform_interface& Platform_interface::create_platform() {
    return *(new BlocksDemo);
}
void Platform_interface::destroy_platform(Platform_interface& interface) {
    delete &interface;
}
