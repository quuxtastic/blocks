#include <vector>

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#include "util/log.hh"
#include "util/error.hh"
#include "util/string-utils.hh"

static void glfw_error_callback(int error, const char* desc) {
    ERR("platform", "GLFW error (%s): %s", error, desc);
}

GLFWwindow* window;

VkInstance vk_instance;

std::vector<VkExtensionProperties> available_extensions;
std::vector<VkLayerProperties> available_layers;

const char* required_extensions[] = {
    VK_EXT_DEBUG_REPORT_EXTENSION_NAME,
    nullptr
};

bool check_required_extensions() {
    std::vector<std::string> not_found;
    for (const char** cur_req = required_extensions; *cur_req; ++cur_req) {
        bool found = false;
        for (const auto& cur_avail : available_extensions) {
            if (std::string(*cur_req) == cur_avail.extensionName) {
                found = true;
                break;
            }
        }

        if (!found) {
            ERR("setup", "Missing required extension %s", *cur_req);
            not_found.push_back(*cur_req);
        }
    }

    return not_found.size() == 0;
}

static VKAPI_ATTR VkBool32 VKAPI_CALL vk_debug_cb(
    VkDebugReportFlagsEXT,
    VkDebugReportObjectTypeEXT,
    std::uint64_t,
    std::size_t,
    std::int32_t,
    const char*,
    const char* msg,
    void*) {

    ERR("vulkan", msg);

    return VK_FALSE;
}

VkDebugReportCallbackEXT vk_debug_cb_handle;

void vulkan_debug_cb_setup() {
    VkDebugReportCallbackCreateInfoEXT createinfo = {};
    createinfo.sType = VK_STRUCTURE_TYPE_DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT;
    createinfo.flags =
        VK_DEBUG_REPORT_ERROR_BIT_EXT
        | VK_DEBUG_REPORT_WARNING_BIT_EXT;
    createinfo.pfnCallback = vk_debug_cb;

    auto func = (PFN_vkCreateDebugReportCallbackEXT)vkGetInstanceProcAddr(
        vk_instance, "vkCreateDebugReportCallbackEXT");
    if (!func) {
        THROW("setup", "Can't find vkCreateDebugReportCallbackEXT");
    }

    VkResult res = func(vk_instance, &createinfo, nullptr, &vk_debug_cb_handle);
    if (res != VK_SUCCESS) {
        THROW("setup", "Failed to install debug callback");
    }
}

void setup_vulkan() {
    std::uint32_t extension_count = 0;
    vkEnumerateInstanceExtensionProperties(nullptr, &extension_count, nullptr);
    available_extensions.resize(extension_count);
    vkEnumerateInstanceExtensionProperties(nullptr, &extension_count,
        available_extensions.data());

    std::vector<std::string> ext_names;
    for (const auto& cur : available_extensions) {
        ext_names.push_back(std::string(cur.extensionName));
    }
    INFO("setup", "Supported extensions (%d): %s", extension_count,
        join(ext_names, ','));

    if(!check_required_extensions()) {
        THROW("setup", "Missing required extensions");
    }

    unsigned int layer_count = 0;
    vkEnumerateInstanceLayerProperties(&layer_count, nullptr);
    available_layers.resize(layer_count);
    vkEnumerateInstanceLayerProperties(&layer_count, available_layers.data());

    std::vector<std::string> layer_names;
    for (const auto& cur : available_layers) {
        layer_names.push_back(std::string(cur.layerName));
    }
    INFO("setup", "Supported instance layers: %s", join(layer_names, ','));

    VkApplicationInfo appinfo = {};
    appinfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    appinfo.pApplicationName = "First Triangle";
    appinfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
    appinfo.pEngineName = "No engine";
    appinfo.engineVersion = VK_MAKE_VERSION(1, 0, 0);
    appinfo.apiVersion = VK_API_VERSION_1_0;

    VkInstanceCreateInfo createinfo = {};
    createinfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    createinfo.pApplicationInfo = &appinfo;

    std::uint32_t glfw_ext_count = 0;
    auto glfw_extensions = glfwGetRequiredInstanceExtensions(&glfw_ext_count);
    std::vector<const char*> req_extensions(glfw_extensions,
        glfw_extensions + glfw_ext_count);
    for (const char** cur_ext = required_extensions; *cur_ext != nullptr;
            ++cur_ext) {
        req_extensions.push_back(*cur_ext);
    }

    createinfo.enabledExtensionCount = req_extensions.size();
    createinfo.ppEnabledExtensionNames = req_extensions.data();

    std::vector<const char*> validation_layers = {
        "VK_LAYER_LUNARG_standard_validation"
    };
    createinfo.enabledLayerCount = 1;
    createinfo.ppEnabledLayerNames = validation_layers.data();

    VkResult res = vkCreateInstance(&createinfo, nullptr, &vk_instance);
    if (res != VK_SUCCESS) {
        THROW("setup", "Failed to create VkInstance");
    }

    vulkan_debug_cb_setup();
}

void setup() {
    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
    glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);
    window = glfwCreateWindow(800, 600, "Vulkan window", nullptr, nullptr);

    setup_vulkan();
}

void cleanup_vk_debug_cb() {
    auto func = (PFN_vkDestroyDebugReportCallbackEXT)vkGetInstanceProcAddr(
        vk_instance, "vkDestroyDebugReportCallbackEXT");
    func(vk_instance, vk_debug_cb_handle, nullptr);
}

void cleanup() {
    cleanup_vk_debug_cb();

    vkDestroyInstance(vk_instance, nullptr);

    glfwDestroyWindow(window);
}

void main_loop() {
    while (!glfwWindowShouldClose(window)) {
        glfwPollEvents();
    }
}

int main(int, char**) {
    try {
        glfwSetErrorCallback(glfw_error_callback);
        if (!glfwInit()) {
            THROW("platform", "glfwInit() failed");
        }

        setup();

        main_loop();

        cleanup();

        glfwTerminate();
    } catch(std::exception& e) {
        INFO("platform", "Caught error: %s", e.what());
    }

    return 0;
}
