#include "platform/platform.hh"

#include <SOIL/SOIL.h>
#include "render/gl.hh"
#include "platform/window.hh"
#include "render/overlay.hh"
#include "render/texture.hh"

class OverlaysDemo : public Platform_interface {
public:

    static const size_t WINDOW_WIDTH = 800;
    static const size_t WINDOW_HEIGHT = 600;

    virtual void setup() {
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

#ifdef DEBUG
        glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);
#endif

        new Window(WINDOW_WIDTH, WINDOW_HEIGHT, "overlays");
        main_window().set_context();
        glfwSwapInterval(0);

        enable_gl_debug();

        glViewport(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);

        glEnable(GL_DEPTH_TEST);
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);

        main_window().on_key([] (int key, int, int action, int) {
            if (action == GLFW_PRESS) {
                switch (key) {
                case GLFW_KEY_Q:
                    main_window().close();
                    break;
                default: break;
                }
            }
        });

        main_window().on_resize([this] (int, int) {
            auto fbsize = main_window().get_fb_size();
            glViewport(0, 0, fbsize.first, fbsize.second);
            this->overlayRenderer->set_viewport(fbsize.first, fbsize.second);
        });

        my_font = Font::create("assets/FreeSans.ttf", 48);

        overlayRenderer.reset(new OverlayRenderer);

        overlayRenderer->add(std::shared_ptr<Overlay>(new Overlay({
                0, 0, WINDOW_WIDTH / 2, 100,
                Overlay::screen_rect_t::ALIGN_LEFT,
                Overlay::screen_rect_t::ALIGN_BOTTOM,
                1
            }, glm::vec4(1.0f, 1.0f, 1.0f, 1.0f))));

        overlayRenderer->add(std::shared_ptr<Overlay>(new TextOverlay({
            10, 10, 0, 0,
            Overlay::screen_rect_t::ALIGN_RIGHT,
            Overlay::screen_rect_t::ALIGN_BOTTOM,
            0
        }, glm::vec4(1.0f, 1.0f, 1.0f, 1.0f),
        "This is a test!", my_font)));

        std::vector<GLfloat> vertices;
        vertices.push_back(10.0f); vertices.push_back(20.0f);
        vertices.push_back(0.0f); vertices.push_back(0.0f);
        vertices.push_back(10.0f); vertices.push_back(0.0f);
        vertices.push_back(0.0f); vertices.push_back(0.0f);

        vertices.push_back(0.0f); vertices.push_back(10.0f);
        vertices.push_back(0.0f); vertices.push_back(0.0f);
        vertices.push_back(20.0f); vertices.push_back(10.0f);
        vertices.push_back(0.0f); vertices.push_back(0.0f);
        Buffer crosshair_buf = Buffer::create(GL_ARRAY_BUFFER, &vertices[0],
            vertices.size() * sizeof(GLfloat), GL_STATIC_DRAW);

        overlayRenderer->add(std::shared_ptr<Overlay>(new GeomOverlay({
            0, 0, 20, 20,
            Overlay::screen_rect_t::ALIGN_CENTER_LEFT,
            Overlay::screen_rect_t::ALIGN_CENTER_TOP,
            0
        }, glm::vec4(1.0f, 1.0f, 1.0f, 1.0f),
        crosshair_buf, GL_LINES, vertices.size() / 4)));

        Texture happyface = Texture::d2(GL_TEXTURE_2D, GL_RGB8,
            "assets/awesomeface.png", SOIL_LOAD_RGB,
            GL_RGB, GL_UNSIGNED_BYTE, [] (GLuint, GLuint type) {
                glTexParameteri(type, GL_TEXTURE_WRAP_S, GL_REPEAT);
                glTexParameteri(type, GL_TEXTURE_WRAP_T, GL_REPEAT);
                glTexParameteri(type, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
                glTexParameteri(type, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            });

        overlayRenderer->add(std::shared_ptr<Overlay>(new ImageOverlay({
            0, 0, 100, 100,
            Overlay::screen_rect_t::ALIGN_RIGHT,
            Overlay::screen_rect_t::ALIGN_BOTTOM,
            0
        }, glm::vec4(1.0f, 1.0f, 1.0f, 0.9f),
        happyface)));

        overlayRenderer->set_viewport(WINDOW_WIDTH, WINDOW_HEIGHT);

        glEnable(GL_CULL_FACE);
        glFrontFace(GL_CCW);
    }

    virtual void cleanup() {

    }

    virtual void main_loop() {
        float last_time = glfwGetTime();

        while(!main_window().should_close()) {
            float cur_time = glfwGetTime();
            float dtime = cur_time - last_time;
            last_time = cur_time;

            main_window().poll_events();

            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

            overlayRenderer->render(dtime);

            main_window().swap();
        }
    }

private:
    std::unique_ptr<OverlayRenderer> overlayRenderer;
    Font my_font;
};
Platform_interface& Platform_interface::create_platform() {
    return *(new OverlaysDemo);
}
void Platform_interface::destroy_platform(Platform_interface& interface) {
    delete &interface;
}
