#include "platform/platform.hh"

#include <SOIL/SOIL.h>

#include "render/gl.hh"
#include "platform/window.hh"
#include "render/overlay.hh"
#include "terrain/chunk.hh"
#include "render/chunk.hh"
#include "render/tesselate.hh"
#include "platform/keybinding.hh"
#include "render/camera.hh"
#include "terrain/generators.hh"

class TesselationDemo : public Platform_interface {
public:
    static const size_t WINDOW_WIDTH = 800;
    static const size_t WINDOW_HEIGHT = 600;

    enum {
        TESS_SIMPLE_CULL = 0,
        TESS_GREEDY,
        TESS_MAX_TESS_TYPES
    };
    unsigned int tess_type = TESS_SIMPLE_CULL;

    KeybindManager bindings;
    enum {
        BIND_QUIT = 1,
        BIND_PAUSE,

        BIND_WIREFRAME,

        BIND_SWITCH_TESS_MODE,

        BIND_MOVE_FORWARD,
        BIND_MOVE_BACK,
        BIND_MOVE_LEFT,
        BIND_MOVE_RIGHT
    };

    bool move_fwd_on = false,
         move_back_on = false,
         move_left_on = false,
         move_right_on = false,
         wireframe_mode = false,
         paused = false;
    void setup_events() {
        bindings.attach(BIND_QUIT, "Quit", "Close the application",
            [] (size_t, int, int action) {
                if (action == GLFW_PRESS) main_window().close();
            });
        bindings.attach(BIND_PAUSE, "Pause", "Pause the application",
            [this] (size_t, int, int action) {
                if (action == GLFW_PRESS) do_pause(!paused);
            });

        bindings.attach(BIND_WIREFRAME, "Wireframe", "Draw as wireframe",
            [this] (size_t, int, int action) {
                if (action == GLFW_PRESS) {
                    wireframe_mode = !wireframe_mode;
                    glPolygonMode(GL_FRONT_AND_BACK,
                        wireframe_mode ? GL_LINE : GL_FILL);
                }
            });

        bindings.attach(BIND_SWITCH_TESS_MODE, "Tesselation Mode",
            "Change tesselation modes", [this] (size_t, int, int action) {
                if (action == GLFW_PRESS) {
                    tess_type = (tess_type + 1) % TESS_MAX_TESS_TYPES;
                    set_tesselation_mode(tess_type);
                }
            });

        bindings.attach(BIND_MOVE_FORWARD, "Move forward", "Move forward",
            [this] (size_t, int, int action) {
                move_fwd_on = action != GLFW_RELEASE;
            });
        bindings.attach(BIND_MOVE_BACK, "Move backward", "Move backward",
            [this] (size_t, int, int action) {
                move_back_on = action != GLFW_RELEASE;
            });
        bindings.attach(BIND_MOVE_LEFT, "Move left", "Move left",
            [this] (size_t, int, int action) {
                move_left_on = action != GLFW_RELEASE;
            });
        bindings.attach(BIND_MOVE_RIGHT, "Move right", "Move right",
            [this] (size_t, int, int action) {
                move_right_on = action != GLFW_RELEASE;
            });

        bindings.bind_kb(BIND_QUIT, GLFW_KEY_Q);
        bindings.bind_kb(BIND_PAUSE, GLFW_KEY_ESCAPE);

        bindings.bind_kb(BIND_WIREFRAME, GLFW_KEY_L);
        bindings.bind_kb(BIND_SWITCH_TESS_MODE, GLFW_KEY_SPACE);

        bindings.bind_kb(BIND_MOVE_FORWARD, GLFW_KEY_W);
        bindings.bind_kb(BIND_MOVE_BACK, GLFW_KEY_S);
        bindings.bind_kb(BIND_MOVE_LEFT, GLFW_KEY_A);
        bindings.bind_kb(BIND_MOVE_RIGHT, GLFW_KEY_D);

        main_window().on_key([this] (int key, int, int action, int) {
            bindings.dispatch_kb(key, action);
        });
        main_window().on_resize([this] (int, int) {
            auto fbsize = main_window().get_fb_size();
            glViewport(0, 0, fbsize.first, fbsize.second);
            this->camera.set_viewport(fbsize.first, fbsize.second);
            this->overlays->set_viewport(fbsize.first, fbsize.second);
        });

        main_window().on_mouse([this] (double dx, double dy) {
            if (!this->paused) {
                this->camera.yaw(dx);
                this->camera.pitch(-dy);
            }
        });

        main_window().on_mouse_btn([this] (int button, int action, int) {
            bindings.dispatch_mbtn(button, action);
        });

        main_window().on_focus([this] (int focused) {
            if (!focused) this->do_pause(true);
        });
    }

    std::shared_ptr<OverlayRenderer> overlays;
    std::shared_ptr<TextOverlay> hud_tess_mode;
    Font hud_font;
    void setup_overlays() {
        overlays.reset(new OverlayRenderer);
        overlays->set_viewport(WINDOW_WIDTH, WINDOW_HEIGHT);

        hud_font = Font::create("assets/DejaVuSansMono.ttf", 16);

        hud_tess_mode.reset(new TextOverlay({
            0, 10, 0, 0,
            Overlay::screen_rect_t::ALIGN_CENTER_LEFT,
            Overlay::screen_rect_t::ALIGN_TOP,
            0
        }, glm::vec4(1.0f, 1.0f, 1.0f, 1.0f), "Tesselation mode", hud_font));
        overlays->add(hud_tess_mode);
    }

    void do_pause(bool mode) {
        paused = mode;
        if (mode) {
            main_window().free_mouse();
        } else {
            main_window().grab_mouse();
        }
    }

    void set_tesselation_mode(int mode) {
        chunk->dirty(true);
        switch (mode) {
        case TESS_SIMPLE_CULL:
            chunk->tesselate(tesselate_chunk);
            break;
        case TESS_GREEDY:
            chunk->tesselate(tesselate_chunk_greedy);
            break;
        }
    }

    Shader chunk_shader;
    Texture textures;

    virtual void setup() {
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#ifdef DEBUG
        glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);
#endif

        new Window(WINDOW_WIDTH, WINDOW_HEIGHT, "tesselation");
        main_window().grab_mouse();
        setup_events();

        main_window().set_context();
        glfwSwapInterval(0);

        enable_gl_debug();

        chunk_shader = Shader::create_from_files(
            GL_VERTEX_SHADER, "assets/shaders/chunk.vs",
            GL_FRAGMENT_SHADER, "assets/shaders/chunk.ps");

        auto texture_props = [] (GLuint, GLuint type) {
            glTexParameteri(type, GL_TEXTURE_WRAP_S, GL_REPEAT);
            glTexParameteri(type, GL_TEXTURE_WRAP_T, GL_REPEAT);
            glTexParameteri(type, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            glTexParameteri(type, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        };
        textures = Texture::array(GL_TEXTURE_2D_ARRAY, GL_RGB8,
            {"assets/awesomeface.png"},
            SOIL_LOAD_RGB, GL_RGB, GL_UNSIGNED_BYTE, texture_props, 1);

        setup_overlays();

        chunk.reset(new ChunkMesh(0, 0, 0));
        terrain_gen::random_pillars(0, 0, 0, *chunk);
        chunk->terrain_ready(true);

        set_tesselation_mode(tess_type);

        glViewport(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);
        camera.set_viewport(WINDOW_WIDTH, WINDOW_HEIGHT);
        overlays->set_viewport(WINDOW_WIDTH, WINDOW_HEIGHT);

        camera.set_move_speed(20.0f);
        camera.set_pos(glm::vec3(16 * 4, 16 * 4, -32));

        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);

        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);

        glFrontFace(GL_CCW);
        glEnable(GL_CULL_FACE);
    }

    virtual void cleanup() {

    }

    const char* make_tess_mode_str(int mode) {
        switch (mode) {
        case TESS_SIMPLE_CULL:
            return "Simple Cull Tesselation";
        case TESS_GREEDY:
            return "Greedy Tesselation";
        default: break;
        }

        return "";
    }

    Camera camera;

    virtual void main_loop() {
        float last_time = glfwGetTime();

        while (!main_window().should_close()) {
            float cur_time = glfwGetTime();
            float dtime = cur_time - last_time;
            last_time = cur_time;
            main_window().poll_events();

            if (move_fwd_on)    camera.move_z(dtime);
            if (move_back_on)   camera.move_z(-dtime);
            if (move_left_on)   camera.move_x(-dtime);
            if (move_right_on)  camera.move_x(dtime);

            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

            chunk_shader.bind();
            chunk_shader.set_uniform("view", camera.get_view());
            chunk_shader.set_uniform("projection", camera.get_projection());

            //chunk->render(chunk_shader);

            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

            chunk->render(chunk_shader);

            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
            hud_tess_mode->set_text(make_tess_mode_str(tess_type));

            overlays->render(dtime);

            main_window().swap();
        }
    }

    std::unique_ptr<ChunkMesh> chunk;

private:
};
Platform_interface& Platform_interface::create_platform() {
    return *(new TesselationDemo);
}
void Platform_interface::destroy_platform(Platform_interface& interface) {
    delete &interface;
}
