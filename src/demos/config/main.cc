#include "util/config.hh"
#include "util/string-utils.hh"
#include "util/log.hh"

int main(int, char**) {
    ConfigRegistry configs;

    configs.default_section().set("key1", "val1");
    configs.get_section("test_sec", true).set("key2", "val2");
    configs.default_section().set("key3", "val3", false, true);
    configs.default_section().set("key5", 42);
    configs.default_section().set("key6", 10.4);

    configs.dump();

    configs.save("test.conf");

    INFO("main", "=== Part 2 ===");

    ConfigRegistry config2;
    config2.default_section().set("key3", "new value", false, true);
    config2.load("test.conf");
    config2.dump();

    ASSERT(
        config2.default_section().get<std::string>("key1") == "val1");
    ASSERT(
        config2.get_section("test_sec", false).get<std::string>("key2") == "val2");
    ASSERT(
        config2.default_section().get<std::string>("key3") == "new value");
    ASSERT(
        config2.default_section().get<int>("key5") == 42);
    float eps = 0.00001;
    float val = config2.default_section().get<float>("key6");
    ASSERT((10.4f - eps < val) && (10.4f + eps > val));
}
