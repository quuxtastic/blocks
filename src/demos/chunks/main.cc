#include "platform/platform.hh"

#include <SOIL/SOIL.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <chrono>

#include "util/perf.hh"
#include "util/config.hh"

#include "platform/window.hh"
#include "platform/keybinding.hh"

#include "render/shader.hh"
#include "render/texture.hh"
#include "render/mesh.hh"
#include "render/buffer.hh"
#include "render/camera.hh"
#include "render/overlay.hh"
#include "render/font.hh"
#include "render/chunk.hh"

#include "terrain/generators.hh"
#include "terrain/terrain.hh"

#include "render/tesselate.hh"

class ChunksDemo : public Platform_interface {
public:
    static const size_t WORLD_HEIGHT = 1;
    static const size_t VIEW_RADIUS = 3;

    void do_pause(bool mode) {
        paused = mode;
        if (mode) {
            main_window().free_mouse();
            main_window().set_title("chunks (paused)");

            hud_paused_back->show();
            for (auto cur : hud_paused_bindings) {
                cur->show();
            }
        } else {
            main_window().grab_mouse();
            main_window().set_title("chunks");

            hud_paused_back->hide();
            for (auto cur : hud_paused_bindings) {
                cur->hide();
            }
        }
    }

    KeybindManager bindings;

    enum {
        BIND_QUIT = 1,
        BIND_PAUSE,
        BIND_WIREFRAME,
        BIND_SHOW_NORMALS,
        BIND_HEIGHTMAP,
        BIND_SET_BLOCK,
        BIND_CLEAR_BLOCK,
        BIND_MOVE_FORWARD,
        BIND_MOVE_BACK,
        BIND_MOVE_LEFT,
        BIND_MOVE_RIGHT,
        BIND_ZOOM,
        BIND_TOGGLE_LIGHTING
    };

    bool move_fwd_on = false,
         move_back_on = false,
         move_left_on = false,
         move_right_on = false,
         zoom_on = false,
         wireframe = false,
         show_normals = false,
         show_heightmap = true,
         lighting = false;
    void setup_keybindings() {
        bindings.attach(BIND_QUIT, "Quit", "Close the application",
            [] (size_t, int, int action) {
                if (action == GLFW_PRESS) main_window().close();
            });
        bindings.attach(BIND_PAUSE, "Pause", "Pause the application",
            [this] (size_t, int, int action) {
                if (action == GLFW_PRESS) do_pause(!paused);
            });

        bindings.attach(BIND_WIREFRAME, "Wireframe", "Draw in wireframe mode",
            [this] (size_t, int, int action) {
                if (!paused && action == GLFW_PRESS) {
                    wireframe = !wireframe;
                    glPolygonMode(GL_FRONT_AND_BACK,
                        wireframe ? GL_LINE : GL_FILL);
                }
            });
        bindings.attach(BIND_SHOW_NORMALS, "Show Normals", "Draw normal vectors",
            [this] (size_t, int, int action) {
                if (!paused && action == GLFW_PRESS) {
                    show_normals = !show_normals;
                }
            });
        bindings.attach(BIND_HEIGHTMAP, "Heightmap", "Draw terrain as heighmap",
            [this] (size_t, int, int action) {
                if (!paused && action == GLFW_PRESS) {
                    show_heightmap = !show_heightmap;
                }
            });

        bindings.attach(BIND_SET_BLOCK, "Set block", "Add a block to the scene",
            [this] (size_t, int, int action) {
                if (!paused && action == GLFW_PRESS) {
                    Chunk* chunk = terrain.get(
                        selected_block.first.p,
                        selected_block.first.q,
                        selected_block.first.r);
                    if (chunk) chunk->set(
                        selected_block.second.p,
                        selected_block.second.q,
                        selected_block.second.r,
                        Block::STONE);
                }
            });
        bindings.attach(BIND_CLEAR_BLOCK, "Clear block",
            "Remove a block from the scene", [this] (size_t, int, int action) {
                if (!paused && action == GLFW_PRESS) {
                    Chunk* chunk = terrain.get(
                        selected_block.first.p,
                        selected_block.first.q,
                        selected_block.first.r);
                    if (chunk) chunk->set(
                        selected_block.second.p,
                        selected_block.second.q,
                        selected_block.second.r,
                        Block::AIR);
                }
            });

        bindings.attach(BIND_MOVE_FORWARD, "Move forward", "Move forward",
            [this] (size_t, int, int action) {
                move_fwd_on = action != GLFW_RELEASE;
            });
        bindings.attach(BIND_MOVE_BACK, "Move backward", "Move backward",
            [this] (size_t, int, int action) {
                move_back_on = action != GLFW_RELEASE;
            });
        bindings.attach(BIND_MOVE_LEFT, "Move left", "Move left",
            [this] (size_t, int, int action) {
                move_left_on = action != GLFW_RELEASE;
            });
        bindings.attach(BIND_MOVE_RIGHT, "Move right", "Move right",
            [this] (size_t, int, int action) {
                move_right_on = action != GLFW_RELEASE;
            });
        bindings.attach(BIND_ZOOM, "Zoom", "Zoom view",
            [this] (size_t, int, int action) {
                zoom_on = action != GLFW_RELEASE;
            });

        bindings.attach(BIND_TOGGLE_LIGHTING, "Lighting", "Enable lighting",
            [this] (size_t, int, int action) {
                if (!paused && action == GLFW_PRESS) lighting = !lighting;
            });

        // default bindings
        bindings.bind_kb(BIND_QUIT, GLFW_KEY_Q);
        bindings.bind_kb(BIND_PAUSE, GLFW_KEY_ESCAPE, true);

        bindings.bind_kb(BIND_WIREFRAME, GLFW_KEY_L);
        bindings.bind_kb(BIND_SHOW_NORMALS, GLFW_KEY_N);
        bindings.bind_kb(BIND_HEIGHTMAP, GLFW_KEY_H);

        bindings.bind_mouse(BIND_SET_BLOCK, GLFW_MOUSE_BUTTON_LEFT);
        bindings.bind_mouse(BIND_CLEAR_BLOCK, GLFW_MOUSE_BUTTON_RIGHT);

        bindings.bind_kb(BIND_MOVE_FORWARD, GLFW_KEY_W);
        bindings.bind_kb(BIND_MOVE_BACK, GLFW_KEY_S);
        bindings.bind_kb(BIND_MOVE_LEFT, GLFW_KEY_A);
        bindings.bind_kb(BIND_MOVE_RIGHT, GLFW_KEY_D);
        bindings.bind_kb(BIND_ZOOM, GLFW_KEY_Z);

        bindings.bind_kb(BIND_TOGGLE_LIGHTING, GLFW_KEY_SEMICOLON);
    }

    unsigned int window_width, window_height;
    int window_x, window_y;
    ConfigRegistry conf;
    void load_config() {
        conf.load("chunks.conf");

        window_x = conf.default_section()
            .get<int>("window.x", 0, true);
        window_y = conf.default_section()
            .get<int>("window.y", 0, true);
        window_width = conf.default_section()
            .get<int>("window.w", 800, true);
        window_height = conf.default_section()
            .get<int>("window.h", 600, true);

        perlin_gen.setup(conf);
    }

    void save_config() {
        auto window_pos = main_window().get_position();
        auto window_dim = main_window().get_size();

        conf.default_section().set("window.x", window_pos.first);
        conf.default_section().set("window.y", window_pos.second);
        conf.default_section().set("window.w", window_dim.first);
        conf.default_section().set("window.h", window_dim.second);

        conf.save("chunks.conf");
    }

    virtual void setup() {
        load_config();

        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

#ifdef DEBUG
        glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);
#endif

        new Window(window_width, window_height, "chunks");
        main_window().set_position(window_x, window_y);
        main_window().grab_mouse();

        setup_keybindings();

        main_window().set_context();
        glfwSwapInterval(0);

        enable_gl_debug();

        ignore_gl_debug_message(
            // Buffer detailed info message
            GL_DEBUG_SOURCE_API, GL_DEBUG_TYPE_OTHER, 131185);

        chunk_shader = Shader::create_from_files(
            GL_VERTEX_SHADER, "assets/shaders/chunk.vs",
            GL_FRAGMENT_SHADER, "assets/shaders/chunk.ps"
            );
        heightmap_shader = Shader::create_from_files(
            GL_VERTEX_SHADER, "assets/shaders/chunk.vs",
            GL_FRAGMENT_SHADER, "assets/shaders/chunk-heightmap.ps");
        normal_shader = Shader::create_from_file(
            "assets/shaders/normals.program");
        mesh_shader = Shader::create_from_file(
            "assets/shaders/mesh.program");

        auto texture_props = [] (GLuint, GLuint type) {
            glTexParameteri(type, GL_TEXTURE_WRAP_S, GL_REPEAT);
            glTexParameteri(type, GL_TEXTURE_WRAP_T, GL_REPEAT);
            glTexParameteri(type, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            glTexParameteri(type, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        };

        textures = Texture::array(GL_TEXTURE_2D_ARRAY, GL_RGB8,
            {"assets/rock.jpg",
             "assets/rock.jpg",
             "assets/grass_top.jpg",
             "assets/grass_side.jpg",
             "assets/sand.jpg",
             "assets/water.jpg"},
            SOIL_LOAD_RGB, GL_RGB, GL_UNSIGNED_BYTE, texture_props, 1);

        main_window().on_key([this] (int key, int, int action, int) {
            bindings.dispatch_kb(key, action);
        });

        main_window().on_resize([this] (int, int) {
            auto fbsize = main_window().get_fb_size();
            glViewport(0, 0, fbsize.first, fbsize.second);

            this->camera.set_viewport(fbsize.first, fbsize.second);
            this->overlays->set_viewport(fbsize.first, fbsize.second);
        });

        main_window().on_mouse([this] (double dx, double dy) {
            if (!this->paused) {
                this->camera.yaw(dx);
                this->camera.pitch(-dy);
            }
        });

        main_window().on_mouse_btn([this] (int button, int action, int) {
            bindings.dispatch_mbtn(button, action);
        });

        main_window().on_focus([this] (int focused) {
            if (!focused) this->do_pause(true);
        });

        //terrain.set_worldgen_func(terrain_gen::perlin_hills);
        using namespace std::placeholders;
        terrain.set_worldgen_func(
            std::bind(&terrain_gen::PerlinGenerator::generate_chunk,
                      &perlin_gen, _1, _2, _3, _4));
        terrain.set_chunk_allocator(
            [this] (int p, int q, int r) {
                ++(this->loaded_chunks_count);
                return new ChunkMesh(p, q, r);
            },
            [this] (Chunk* chunk) {
                delete chunk;
                --(this->loaded_chunks_count);
            });
        terrain.set_world_height(0, WORLD_HEIGHT);
        terrain.set_evict_timeout(std::chrono::seconds(1));

        hud_font = Font::create("assets/DejaVuSansMono.ttf", 16);

        overlays.reset(new OverlayRenderer);
        overlays->set_viewport(window_width, window_height);

        hud_fps.reset(new TextOverlay({
                10, 10, 0, 0,
                Overlay::screen_rect_t::ALIGN_LEFT,
                Overlay::screen_rect_t::ALIGN_TOP,
                0
            }, glm::vec4(1.0f, 1.0f, 1.0f, 1.0f), "FPS", hud_font));
        hud_chunks.reset(new TextOverlay({
                10, 30, 0, 0,
                Overlay::screen_rect_t::ALIGN_LEFT,
                Overlay::screen_rect_t::ALIGN_TOP,
                0
            }, glm::vec4(1.0f, 1.0f, 1.0f, 1.0f), "Chunks", hud_font));
        hud_sel.reset(new TextOverlay({
                10, 50, 0, 0,
                Overlay::screen_rect_t::ALIGN_LEFT,
                Overlay::screen_rect_t::ALIGN_TOP,
                0
            }, glm::vec4(1.0f, 1.0f, 1.0f, 1.0f), "Selected", hud_font));

        const GLfloat crosshair_verts[] = {
            10.0f, 20.0f, 0.0f, 0.0f,
            10.0f, 0.0f, 0.0f, 0.0f,

            0.0f, 10.0f, 0.0f, 0.0f,
            20.0f, 10.0f, 0.0f, 0.0f
        };
        Buffer crosshair_buf = Buffer::create(GL_ARRAY_BUFFER,
            crosshair_verts, sizeof(crosshair_verts) * sizeof(GLfloat),
            GL_STATIC_DRAW);
        hud_crosshair.reset(new GeomOverlay({
                0, 0, 20, 20,
                Overlay::screen_rect_t::ALIGN_CENTER_LEFT,
                Overlay::screen_rect_t::ALIGN_CENTER_TOP,
                0
            }, glm::vec4(1.0f, 1.0f, 1.0f, 1.0f),
            crosshair_buf, GL_LINES, 4));

        overlays->add(hud_fps);
        overlays->add(hud_chunks);
        overlays->add(hud_sel);
        overlays->add(hud_crosshair);

        unsigned int text_off = 10;
        for (auto info : bindings.get_bindings()) {
            std::shared_ptr<TextOverlay> text(new TextOverlay({
                10, text_off, 0, 0,
                Overlay::screen_rect_t::ALIGN_LEFT,
                Overlay::screen_rect_t::ALIGN_BOTTOM,
                0
            }, glm::vec4(1.0f, 1.0f, 1.0f, 1.0f),
            fmt("%s - %s", info.button_str, info.name), hud_font));
            text->hide();
            hud_paused_bindings.push_back(text);
            overlays->add(text);

            text_off += 20;
        }

        hud_paused_back.reset(new Overlay({
            0, 0, window_width / 2, text_off,
            Overlay::screen_rect_t::ALIGN_LEFT,
            Overlay::screen_rect_t::ALIGN_BOTTOM,
            1
        }, glm::vec4(0.0f, 0.0f, 0.0f, 0.8f)));
        hud_paused_back->hide();
        overlays->add(hud_paused_back);

        const GLfloat selection_box_verts[] = {
            // front
            0, 0, 0,
            0, Chunk::CUBE_SIZE, 0,

            0, Chunk::CUBE_SIZE, 0,
            Chunk::CUBE_SIZE, Chunk::CUBE_SIZE, 0,

            Chunk::CUBE_SIZE, Chunk::CUBE_SIZE, 0,
            Chunk::CUBE_SIZE, 0, 0,

            Chunk::CUBE_SIZE, 0, 0,
            0, 0, 0,

            // back
            0, 0, Chunk::CUBE_SIZE,
            0, Chunk::CUBE_SIZE, Chunk::CUBE_SIZE,

            0, Chunk::CUBE_SIZE, Chunk::CUBE_SIZE,
            Chunk::CUBE_SIZE, Chunk::CUBE_SIZE, Chunk::CUBE_SIZE,

            Chunk::CUBE_SIZE, Chunk::CUBE_SIZE, Chunk::CUBE_SIZE,
            Chunk::CUBE_SIZE, 0, Chunk::CUBE_SIZE,

            Chunk::CUBE_SIZE, 0, Chunk::CUBE_SIZE,
            0, 0, Chunk::CUBE_SIZE,

            // left
            0, 0, 0,
            0, 0, Chunk::CUBE_SIZE,

            0, Chunk::CUBE_SIZE, 0,
            0, Chunk::CUBE_SIZE, Chunk::CUBE_SIZE,

            // right
            Chunk::CUBE_SIZE, 0, 0,
            Chunk::CUBE_SIZE, 0, Chunk::CUBE_SIZE,

            Chunk::CUBE_SIZE, Chunk::CUBE_SIZE, 0,
            Chunk::CUBE_SIZE, Chunk::CUBE_SIZE, Chunk::CUBE_SIZE
        };
        selection_box_buf = Buffer::create(GL_ARRAY_BUFFER,
            selection_box_verts, sizeof(selection_box_verts) * sizeof(GLfloat),
            GL_STATIC_DRAW);
        selection_box_mesh = Mesh::create([this] (Mesh the_mesh) {
            the_mesh.attach(this->selection_box_buf);
            the_mesh.set_attr(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), 0);
        });

        glViewport(0, 0, window_width, window_height);
        camera.set_viewport(window_width, window_height);
        overlays->set_viewport(window_width, window_height);

        camera.set_zoom_speed(50.0f);
        camera.set_move_speed(20.0f);
        camera.set_pos(glm::vec3(32, 128, 32));

        glEnable(GL_DEPTH_TEST);

        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);

        glFrontFace(GL_CCW);
        glEnable(GL_CULL_FACE);
    }

    virtual void cleanup() {
        save_config();
    }

    std::vector<ChunkMesh*> nearby_chunks;
    std::vector<ChunkMesh*> dirty_chunks;
    void calc_nearby_chunks() {
        nearby_chunks.clear();
        dirty_chunks.clear();
        terrain.traverse<ChunkMesh>(
            player_chunk.p, player_chunk.q, player_chunk.r, VIEW_RADIUS,
            [this] (ChunkMesh& chunk, int, int, int) {
                nearby_chunks.push_back(&chunk);
                chunk.touch();
                if (chunk.dirty()) dirty_chunks.push_back(&chunk);
            });
    }

    std::vector<ChunkMesh*> visible_chunks;
    void calc_visible_chunks() {
        visible_chunks.clear();
    }

    virtual void main_loop() {
        float last_time = glfwGetTime();

        size_t frame_count = 0;
        float last_fps_mark = last_time;
        float fps = 0;

        int total_tesselations = 0;
        int total_tesselation_time = 0;

        while (!main_window().should_close()) {
            float cur_time = glfwGetTime();
            float dtime = cur_time - last_time;
            last_time = cur_time;

            // sample FPS every 30 frames
            if (++frame_count >= 30) {
                fps = static_cast<float>(frame_count) /
                    (cur_time - last_fps_mark);
                frame_count = 0;
                last_fps_mark = cur_time;
            }

            main_window().poll_events();

            if (!paused && move_fwd_on)    camera.move_z(dtime);
            if (!paused && move_back_on)   camera.move_z(-dtime);
            if (!paused && move_left_on)   camera.move_x(-dtime);
            if (!paused && move_right_on)  camera.move_x(dtime);
            if (!paused) {
                if (zoom_on) {
                    camera.zoom(dtime);
                } else {
                    camera.zoom(-dtime);
                }
            }

            auto player_pos = camera.get_pos();
            player_chunk = world_to_block_coord(player_pos).first;

            const float BLOCK_SELECTION_OFFSET =
                Chunk::CUBE_SIZE * 2;
            selected_block = world_to_block_coord(
                player_pos + (camera.get_front() * BLOCK_SELECTION_OFFSET));
            glm::vec3 sel_world = block_to_world_coord(
                selected_block.first, selected_block.second);
            block_selection_transform = glm::translate(glm::mat4(1.f), sel_world);

            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

            if (wireframe) glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

            // first, touch all visible chunks.
            // this will mark any chunks dirty that need to be
            // re-tesselated
            calc_nearby_chunks();

            // next, re-tesselate any chunks that require it.
            // tesselation can be slow so we only do so much of it per frame.
            auto res = do_iterable_work(
                dirty_chunks.size(), 0,
                std::chrono::milliseconds(10),
                [this] (size_t cur) {
                    dirty_chunks[cur]->tesselate(tesselate_chunk);
                    return 1;
                });
            if (dirty_chunks.size() > 0) {
                total_tesselations += dirty_chunks.size() - std::get<0>(res);
                total_tesselation_time +=
                    std::chrono::duration_cast<std::chrono::milliseconds>(
                        std::get<2>(res)).count();
            }

            calc_visible_chunks();

            if (show_heightmap) {
                heightmap_shader.bind();
                heightmap_shader.set_uniform("view", camera.get_view());
                heightmap_shader.set_uniform("projection",
                                             camera.get_projection());
                heightmap_shader.set_uniform("world_height",
                    static_cast<float>(Chunk::CHUNK_SIZE*Chunk::CUBE_SIZE*
                        (WORLD_HEIGHT+1)));
                heightmap_shader.set_uniform<unsigned int>("enable_lighting",
                    lighting ? 1 : 0);

                for (auto cur : nearby_chunks) {
                    cur->render(this->heightmap_shader);
                }

            } else {
                chunk_shader.bind();
                chunk_shader.set_uniform("view", camera.get_view());
                chunk_shader.set_uniform("projection", camera.get_projection());
                heightmap_shader.set_uniform<unsigned int>("enable_lighting",
                    lighting ? 1 : 0);

                for (auto cur : nearby_chunks) {
                    cur->render(this->chunk_shader);
                }
            }

            if (show_normals) {
                normal_shader.bind();
                normal_shader.set_uniform("view", camera.get_view());
                normal_shader.set_uniform("projection", camera.get_projection());

                for (auto cur : nearby_chunks) {
                    cur->render(this->chunk_shader);
                }
            }

            glClear(GL_DEPTH_BUFFER_BIT);

            if (!paused && terrain.is_valid_coord(
                    selected_block.first.p,
                    selected_block.first.q,
                    selected_block.first.r)) {
                mesh_shader.bind();
                mesh_shader.set_uniform("view", camera.get_view());
                mesh_shader.set_uniform("projection", camera.get_projection());
                mesh_shader.set_uniform("model", block_selection_transform);
                mesh_shader.set_uniform("in_color",
                    glm::vec4(1.0f, 1.0f, 1.0f, 1.0f));
                selection_box_mesh.bind();
                glDrawArrays(GL_LINES, 0, 24);
            }

            if (wireframe) glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

            hud_fps->set_text(fmt("FPS %s (tesselation: %sms)", fps,
                static_cast<float>(total_tesselation_time) /
                    static_cast<float>(total_tesselations)));
            hud_chunks->set_text(fmt("Chunk %s,%s,%s (%s loaded, %sMiB used)",
                player_chunk.p, player_chunk.q, player_chunk.r,
                loaded_chunks_count,
                static_cast<float>(loaded_chunks_count * sizeof(ChunkMesh)) /
                    1024.f / 1024.f));
            hud_sel->set_text(fmt("Selected %s,%s,%s:%s,%s,%s (%s,%s,%s)",
                selected_block.first.p, selected_block.first.q,
                    selected_block.first.r,
                selected_block.second.p, selected_block.second.q,
                    selected_block.second.r,
                sel_world.x, sel_world.y, sel_world.z));

            overlays->render(dtime);

            main_window().swap();

            terrain.do_frame();
        }
    }
private:
    Shader chunk_shader, heightmap_shader, normal_shader, mesh_shader;
    Texture textures;

    Camera camera;
    Chunk::coord_t player_chunk;

    std::pair<Chunk::coord_t, Chunk::coord_t>
        selected_block;
    glm::mat4 block_selection_transform;

    Buffer selection_box_buf;
    Mesh selection_box_mesh;

    ChunkTerrain terrain;
    terrain_gen::PerlinGenerator perlin_gen;

    size_t loaded_chunks_count = 0;

    std::unique_ptr<OverlayRenderer> overlays;
    Font hud_font;
    std::shared_ptr<TextOverlay> hud_fps, hud_chunks, hud_sel;
    std::shared_ptr<GeomOverlay> hud_crosshair;

    std::shared_ptr<Overlay> hud_paused_back;
    std::vector<std::shared_ptr<TextOverlay>> hud_paused_bindings;

    bool paused = false;
};
Platform_interface& Platform_interface::create_platform() {
    return *(new ChunksDemo);
}
void Platform_interface::destroy_platform(Platform_interface& interface) {
    delete &interface;
}
