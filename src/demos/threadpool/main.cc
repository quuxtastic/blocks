#include <chrono>
#include <iostream>

#include "util/thread-pool.hh"

int main(int, char**) {

    AsyncWorkPool pool;

    for (size_t i = 0; i < 10; ++i) {
        int id = pool.add_work([i] (int) {
            std::this_thread::sleep_for(std::chrono::seconds(i+1));
        });
        std::cout << "Added work " << id << std::endl;
    }

    std::cout << "Total work items: " << pool.queue_count() << std::endl;

    while (pool.queue_count() > 0) {
        pool.poll([&] (int id) {
            std::cout << "Work finished for " << id << std::endl;
            std::cout << "Items left in queue: " << pool.queue_count() << std::endl;
        });
    }

    for (size_t i = 0; i < 10; ++i) {
        int id = pool.add_work([i] (int) {
            std::this_thread::sleep_for(std::chrono::seconds(i+1));
        });
        std::cout << "Added work " << id << std::endl;
    }

    pool.flush();

    std::cout << "Work items left: " << pool.queue_count() << std::endl;

    return 0;
}
