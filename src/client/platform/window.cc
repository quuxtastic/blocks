#include "window.hh"

#include <cstring>

#define GLEW_STATIC
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "util/error.hh"

Window* Window::main_window = nullptr;

Window::Window(int w, int h, const std::string& title) {
    handle = glfwCreateWindow(w, h, title.c_str(), nullptr, nullptr);
    if (!handle) {
        THROW("window", "Failed to create window %s", title);
    }
    glfwSetWindowUserPointer(handle, this);

    std::memset(keys, 0, sizeof(keys));

    // by default, the first window created is the main window
    if (!main_window) set_as_main_window();

    init_events();
}

Window::~Window() {
    if (main_window == this) main_window = nullptr;

    glfwDestroyWindow(handle);
}

std::pair<int, int> Window::get_fb_size() {
    int w, h;
    glfwGetFramebufferSize(handle, &w, &h);
    return std::make_pair(w, h);
}
std::pair<unsigned int, unsigned int> Window::get_position() {
    int x, y;
    glfwGetWindowPos(handle, &x, &y);
    return std::make_pair(x, y);
}
std::pair<unsigned int, unsigned int> Window::get_size() {
    int w, h;
    glfwGetWindowSize(handle, &w, &h);
    return std::make_pair(w, h);
}

bool Window::is_focus() {
    return glfwGetWindowAttrib(handle, GLFW_FOCUSED);
}

void Window::set_title(const std::string& title) {
    glfwSetWindowTitle(handle, title.c_str());
}

void Window::set_position(int x, int y) {
    glfwSetWindowPos(handle, x, y);
}

void Window::set_context() {
    glfwMakeContextCurrent(handle);

    glewExperimental = GL_TRUE;
    GLenum glewStatus = glewInit();
    if (glewStatus != GLEW_OK) {
        THROW("window", "glewInit failed: %s",
            glewGetErrorString(glewStatus));
    }
    glGetError(); // clear spurious error from glew
}

void Window::swap() {
    glfwSwapBuffers(handle);
}

bool Window::should_close() {
    return glfwWindowShouldClose(handle) == GL_TRUE;
}

void Window::close() {
    glfwSetWindowShouldClose(handle, GL_TRUE);
}

void Window::grab_mouse() {
    glfwSetInputMode(handle, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
}
void Window::free_mouse() {
    glfwSetInputMode(handle, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
}

std::pair<double, double> Window::get_cursor_pos() {
    std::pair<double, double> out;
    glfwGetCursorPos(handle, &(out.first), &(out.second));
    return out;
}
