#include "keybinding.hh"

#include <GLFW/glfw3.h>

#include "util/log.hh"
#include "util/error.hh"

void KeybindManager::attach(int id, const std::string& name,
                            const std::string& desc,
                            KeybindManager::binding_cb_t fun) {
    ASSERT(id > 0);

    fun_t info;
    info.name = name; info.desc = desc;
    info.fun = fun;
    functions.insert({id, info});
}

void KeybindManager::bind_kb(int func, int button, bool lock) {
    if (clear(func)) bind(func, button, lock, keyboard_bindings);
}
void KeybindManager::bind_mouse(int func, int button, bool lock) {
    if(clear(func)) bind(func, button, lock, mouse_bindings);
}

bool KeybindManager::clear(int func) {
    bool ret = clear(func, keyboard_bindings);
    bool ret2 = clear(func, mouse_bindings);
    return ret && ret2;
}

std::vector<KeybindManager::binding_info_t> KeybindManager::get_bindings() {
    std::vector<binding_info_t> out;
    binding_info_t info;

    for (auto fun_cur : functions) {
        info.device = binding_info_t::NONE;
        info.name = fun_cur.second.name; info.desc = fun_cur.second.desc;
        info.button = GLFW_KEY_UNKNOWN;
        info.locked = false;

        for (auto kb_cur : keyboard_bindings) {
            if (kb_cur.second.fun_id == fun_cur.first) {
                info.device = binding_info_t::KEYBOARD;
                info.button = kb_cur.first;
                info.locked = kb_cur.second.locked;
                info.button_str = get_kb_name(info.button);
                break;
            }
        }
        for (auto mb_cur : mouse_bindings) {
            if (mb_cur.second.fun_id == fun_cur.first) {
                info.device = binding_info_t::MOUSE;
                info.button = mb_cur.first;
                info.locked = mb_cur.second.locked;
                info.button_str = get_btn_name(info.button);
                break;
            }
        }

        out.push_back(info);
    }

    return out;
}

int KeybindManager::dispatch_kb(int key, int action) {
    return dispatch(key, action, binding_info_t::KEYBOARD, keyboard_bindings);
}

int KeybindManager::dispatch_mbtn(int button, int action) {
    return dispatch(button, action, binding_info_t::MOUSE, mouse_bindings);
}

#define KB(k) case GLFW_KEY_ ## k: return #k

const char* KeybindManager::get_kb_name(int key) {
    switch (key) {
        KB(SPACE);
        KB(APOSTROPHE);
        KB(COMMA);
        KB(MINUS);
        KB(PERIOD);
        KB(SLASH);
        KB(0); KB(1); KB(2); KB(3); KB(4); KB(5); KB(6); KB(7); KB(8); KB(9);
        KB(SEMICOLON);
        KB(EQUAL);
        KB(A); KB(B); KB(C); KB(D); KB(E); KB(F); KB(G); KB(H); KB(I); KB(J);
        KB(K); KB(L); KB(M); KB(N); KB(O); KB(P); KB(Q); KB(R); KB(S); KB(T);
        KB(U); KB(V); KB(W); KB(X); KB(Y); KB(Z);
        KB(LEFT_BRACKET);
        KB(BACKSLASH);
        KB(RIGHT_BRACKET);
        KB(GRAVE_ACCENT);
        KB(WORLD_1);
        KB(WORLD_2);
        KB(ESCAPE);
        KB(ENTER);
        KB(TAB);
        KB(BACKSPACE);
        KB(INSERT);
        KB(DELETE);
        KB(RIGHT);
        KB(LEFT);
        KB(DOWN);
        KB(UP);
        KB(PAGE_UP);
        KB(PAGE_DOWN);
        KB(HOME);
        KB(END);
        KB(CAPS_LOCK);
        KB(SCROLL_LOCK);
        KB(NUM_LOCK);
        KB(PRINT_SCREEN);
        KB(PAUSE);
        KB(F1); KB(F2); KB(F3); KB(F4); KB(F5); KB(F6); KB(F7); KB(F8); KB(F9);
        KB(F10); KB(F11); KB(F12); KB(F13); KB(F14); KB(F15); KB(F16); KB(F17);
        KB(F18); KB(F19); KB(F20); KB(F21); KB(F22); KB(F23); KB(F24); KB(F25);
        KB(KP_0); KB(KP_1); KB(KP_2); KB(KP_3); KB(KP_4); KB(KP_5); KB(KP_6);
        KB(KP_7); KB(KP_8); KB(KP_9);
        KB(KP_DECIMAL);
        KB(KP_DIVIDE);
        KB(KP_MULTIPLY);
        KB(KP_SUBTRACT);
        KB(KP_ADD);
        KB(KP_ENTER);
        KB(KP_EQUAL);
        KB(LEFT_SHIFT);
        KB(LEFT_CONTROL);
        KB(LEFT_ALT);
        KB(LEFT_SUPER);
        KB(RIGHT_SHIFT);
        KB(RIGHT_CONTROL);
        KB(RIGHT_ALT);
        KB(RIGHT_SUPER);
        KB(MENU);
    default: return "unknown";
    }
}

#undef KB

#define BTN(b) case GLFW_MOUSE_BUTTON_ ## b: return "MOUSE" #b

const char* KeybindManager::get_btn_name(int button) {
    switch (button) {
        BTN(1); BTN(2); BTN(3); BTN(4); BTN(5); BTN(6); BTN(7); BTN(8);
    default: return "unknown";
    }
}

#undef BTN

void KeybindManager::dump() {
    auto info = get_bindings();
    for (auto cur : info) {
        const char* btn = cur.device == binding_info_t::KEYBOARD ?
            get_kb_name(cur.button) : get_btn_name(cur.button);
        INFO("platform", "%s (%s) - %s (%s)",
            cur.name, cur.desc, btn, cur.button);
    }
}

void KeybindManager::bind(int func, int button, bool lock,
                          KeybindManager::binding_map_t& map) {
    auto fun_cur = functions.find(func);
    if (fun_cur == functions.end())
        THROW("platform", "Unknown binding %s", func);

    binding_t binding;
    binding.fun_id = fun_cur->first;
    binding.fun = fun_cur->second.fun;
    binding.locked = lock;
    map.insert({button, binding});
}

bool KeybindManager::clear(int func, KeybindManager::binding_map_t& map) {
    bool cleared = true;
    for (auto cur = map.begin(); cur != map.end(); ++cur) {
        if (cur->second.fun_id == func) {
            if (cur->second.locked) {
                cleared = false;
            } else {
                cur = map.erase(cur);
            }
        }
    }

    return cleared;
}

int KeybindManager::dispatch(int button, int action, size_t device,
                              binding_map_t& map) {
    auto cur = map.find(button);
    if (cur != map.end()) {
        cur->second.fun(device, button, action);
        return cur->second.fun_id;
    }

    return 0;
}
