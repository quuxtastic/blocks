#include "window.hh"

#include <algorithm>

#include <GLFW/glfw3.h>

#include "util/error.hh"

#define IMPL_EVENT_START(ev_name, ...) \
void Window::on_ ## ev_name (Window:: ev_name ## _cb_t cb) { \
    on_ ## ev_name ## _cbs_to_add.push_back(cb); \
} \
void Window::on_ ## ev_name ## _thunk(GLFWwindow* window, __VA_ARGS__) { \
    Window& wnd = *reinterpret_cast<Window*>(glfwGetWindowUserPointer(window));

#define IMPL_EVENT_DISPATCH(ev_name, ...) \
    wnd.dispatch_event(wnd.on_ ## ev_name ## _cbs, __VA_ARGS__)

IMPL_EVENT_START(pos, int x, int y)
    IMPL_EVENT_DISPATCH(pos, x, y);
}
IMPL_EVENT_START(resize, int w, int h)
    IMPL_EVENT_DISPATCH(resize, w, h);
}
IMPL_EVENT_START(focus, int b)
    wnd.mouse_focus_start = true;
    IMPL_EVENT_DISPATCH(focus, b == GL_TRUE);
}
IMPL_EVENT_START(key, int key, int code, int action, int mods)
    if (!wnd.is_focus()) return; // ignore input events when not focus
    if (key > 0 && key < KEY_MAP_SIZE) {
        wnd.keys[key] = action != GLFW_RELEASE;
    }
    IMPL_EVENT_DISPATCH(key, key, code, action, mods);
}
IMPL_EVENT_START(mouse, double x, double y)
    if (!wnd.is_focus()) return; // ignore input events when not focus
    if (wnd.mouse_focus_start) {
        wnd.last_mouse_x = x; wnd.last_mouse_y = y;
        wnd.mouse_focus_start = false;
    }
    IMPL_EVENT_DISPATCH(mouse, x - wnd.last_mouse_x, y - wnd.last_mouse_y);
    wnd.last_mouse_x = x; wnd.last_mouse_y = y;
}
IMPL_EVENT_START(mouse_btn, int button, int action, int mods)
    if (!wnd.is_focus()) return; // ignore input events when not focus
    IMPL_EVENT_DISPATCH(mouse_btn, button, action, mods);
}

#undef IMPL_EVENT_START
#undef IMPL_EVENT_DISPATCH

#define IMPL_EVENT_CLEAN(ev_name) \
    for (auto cur : on_ ## ev_name ## _cbs_to_add) { \
        on_ ## ev_name ## _cbs.push_back(cur); \
    } \
    on_ ## ev_name ## _cbs_to_add.clear()

void Window::poll_events() {
    glfwPollEvents();

    if (saved_exception) throw *saved_exception;

    IMPL_EVENT_CLEAN(pos);
    IMPL_EVENT_CLEAN(resize);
    IMPL_EVENT_CLEAN(focus);
    IMPL_EVENT_CLEAN(key);
    IMPL_EVENT_CLEAN(mouse);
    IMPL_EVENT_CLEAN(mouse_btn);
}

#undef IMPL_EVENT_CLEAN

void Window::handle_exceptions(std::function<void ()> f) {
    try {
        f();
    } catch (Error& e) {
        INFO("window", "Caught exception in handler: %s", e.what());
        saved_exception.reset(e.clone());
    } catch (std::exception& e) {
        INFO("window", "Caught std::exception in handler: %s", e.what());
        saved_exception.reset(new Error(e.what()));
    } catch (...) {
        INFO("window", "Caught unknown exception in handler");
        saved_exception.reset(new Error("Unknown exception"));
    }
}

#define INIT_EVENT(ev_name, glfw) glfw(handle, on_ ## ev_name ## _thunk)

void Window::init_events() {
    INIT_EVENT(pos, glfwSetWindowPosCallback);
    INIT_EVENT(resize, glfwSetWindowSizeCallback);
    INIT_EVENT(focus, glfwSetWindowFocusCallback);
    INIT_EVENT(key, glfwSetKeyCallback);
    INIT_EVENT(mouse, glfwSetCursorPosCallback);
    INIT_EVENT(mouse_btn, glfwSetMouseButtonCallback);
}

#undef INIT_EVENT
