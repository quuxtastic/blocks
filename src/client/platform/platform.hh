#ifndef PLATFORM_PLATFORM_HH_
#define PLATFORM_PLATFORM_HH_ 1

struct Platform_interface {
    virtual ~Platform_interface() {}

    virtual void setup() = 0;
    virtual void cleanup() = 0;

    virtual void main_loop() = 0;

    static Platform_interface& create_platform();
    static void destroy_platform(Platform_interface&);
};

#endif
