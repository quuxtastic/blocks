#ifndef PLATFORM_KEYBINDING_HH_
#define PLATFORM_KEYBINDING_HH_ 1

#include <functional>
#include <string>
#include <vector>
#include <unordered_map>

class KeybindManager {
public:
    typedef std::function<void (size_t, int, int)> binding_cb_t;

    void attach(int id, const std::string& name, const std::string& desc,
        binding_cb_t fun);

    void bind_kb(int func, int button, bool lock = false);
    void bind_mouse(int func, int button, bool lock = false);

    bool clear(int func);

    struct binding_info_t {
        std::string name, desc;

        enum {
            NONE = 0,
            KEYBOARD = 1,
            MOUSE = 2,
        };
        size_t device;
        int button;

        std::string button_str;

        bool locked;
    };
    std::vector<binding_info_t> get_bindings();

    int dispatch_kb(int key, int action);
    int dispatch_mbtn(int button, int action);

    const char* get_kb_name(int key);
    const char* get_btn_name(int button);

    void dump();

private:
    struct fun_t {
        std::string name, desc;
        binding_cb_t fun;
    };
    std::unordered_map<int, fun_t> functions;

    struct binding_t {
        int fun_id;
        binding_cb_t fun;
        bool locked;
    };
    typedef std::unordered_map<int, binding_t> binding_map_t;
    binding_map_t keyboard_bindings, mouse_bindings;

    void bind(int func, int button, bool lock, binding_map_t& map);
    bool clear(int func, binding_map_t& map);

    int dispatch(int button, int action, size_t device, binding_map_t& map);
};

#endif
