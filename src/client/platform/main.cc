#include <GLFW/glfw3.h>

#include "util/log.hh"
#include "util/error.hh"

#include "platform.hh"

static void glfw_error_callback(int error, const char* desc) {
    ERR("platform", "GLFW error (%s): %s", error, desc);
}

int main(int, char**) {
    try {
        glfwSetErrorCallback(glfw_error_callback);

        if (!glfwInit()) {
            THROW("platform", "glfwInit failed");
        }

        Platform_interface& platform = Platform_interface::create_platform();

        platform.setup();

        platform.main_loop();

        platform.cleanup();

        Platform_interface::destroy_platform(platform);

        glfwTerminate();

    } catch (std::exception& e) {
        INFO("platform", "Caught error: %s", e.what());
    }

    return 0;
}
