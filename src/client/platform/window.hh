#ifndef PLATFORM_WINDOW_HH_
#define PLATFORM_WINDOW_HH_ 1

#include <utility>
#include <string>
#include <functional>
#include <memory>
#include <vector>

#include "util/error.hh"

struct GLFWwindow;

class Window {
private:
    GLFWwindow* handle;
public:
    Window(int w, int h, const std::string& title);
    virtual ~Window();

    Window(const Window&) = delete;
    Window& operator=(const Window&) = delete;

    std::pair<int, int> get_fb_size();
    std::pair<unsigned int, unsigned int> get_position();
    std::pair<unsigned int, unsigned int> get_size();

    bool is_focus();

    void set_title(const std::string& title);
    void set_position(int x, int y);

    void set_context();

    void swap();

    bool should_close();
    void close();

    void grab_mouse();
    void free_mouse();

    void poll_events();

    bool key_state(int key) {
        if (key > 0 && key < KEY_MAP_SIZE) {
            return keys[key];
        }

        return false;
    }

    std::pair<double, double> get_cursor_pos();

    static Window& get_main_window() { return *main_window; }
    void set_as_main_window() { main_window = this; }

#define DEF_EVENT(ev_name, ...) \
public: \
    typedef std::function<void (__VA_ARGS__)> ev_name ## _cb_t; \
    void on_ ## ev_name(ev_name ## _cb_t); \
private: \
    std::vector<ev_name ## _cb_t> on_ ## ev_name ## _cbs; \
    std::vector<ev_name ## _cb_t> on_ ## ev_name ## _cbs_to_add; \
    static void on_ ## ev_name ## _thunk(GLFWwindow*, __VA_ARGS__);

    DEF_EVENT(pos, int, int)
    DEF_EVENT(resize, int, int)
    DEF_EVENT(focus, int)

    DEF_EVENT(key, int, int, int, int)
    DEF_EVENT(mouse, double, double)
    DEF_EVENT(mouse_btn, int, int, int)

#undef DEF_EVENT

private:
    static constexpr const int KEY_MAP_SIZE = 1024;
    bool keys[KEY_MAP_SIZE];

    double last_mouse_x, last_mouse_y;
    bool mouse_focus_start = true;

    std::shared_ptr<Error> saved_exception;
    void handle_exceptions(std::function<void ()> f);

    template<typename CBType, typename... Args>
    void dispatch_event(const CBType& cbs, Args&&... args) {
        handle_exceptions([&] {
            for (auto cur : cbs) {
                if (cur) cur(args...);
            }
        });
    }

    void init_events();

    static Window* main_window;
};

inline Window& main_window() { return Window::get_main_window(); }

#endif
