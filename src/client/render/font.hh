#ifndef RENDER_FONT_HH_
#define RENDER_FONT_HH_ 1

#include <string>
#include <unordered_map>
#include <vector>

#include "texture.hh"

class Font {
public:
    static const char MIN_PRINTABLE_CHAR = ' ';
    static const char MAX_PRINTABLE_CHAR = '~';

    static Font create(const std::string& filename, size_t pt_size);

    void bind();

    struct glyph_t {
        float x, y;
        float w, h;
        float adv_x, adv_y;

        float tx, ty, tw, th;
    };

    const glyph_t& get_glyph(char c);

    Font() : metrics_index(0) {}

    bool valid() { return atlas.valid(); }

private:
    Font(size_t in_metrics, Texture in_atlas) : metrics_index(in_metrics),
        atlas(in_atlas) {}

    size_t metrics_index;
    Texture atlas;

    static std::vector<std::unordered_map<char, glyph_t>> font_glyphs;
};

#endif
