#ifndef RENDER_GLWRAP_FRAMEBUFFER_HH_
#define RENDER_GLWRAP_FRAMEBUFFER_HH_ 1

#include "gl.hh"

namespace gl {

class Texture;

class Framebuffer : public Object<Framebuffer, GL_FRAMEBUFFER> {
public:
    Framebuffer() {}

    static Framebuffer create();

    void attach(const Texture& texture, GLenum attachment, GLint level = 0);
    void attach_layer(const Texture& texture, GLenum attachment, GLint layer,
                      GLint level = 0);

    GLenum get_status(GLenum target);
    bool is_complete(GLenum target);

    void bind_draw(GLenum mode = GL_BACK);

    void bind(GLenum target);
private:
    explicit Framebuffer(GLuint id);
    friend handle_type;
    void destroy();
};

}

#endif
