#ifndef RENDER_GLWRAP_TEXTURE_HH_
#define RENDER_GLWRAP_TEXTURE_HH_ 1

#include <glm/glm.hpp>

#include "gl.hh"

namespace gl {

class Buffer;

class Sampler : public Object<Sampler, GL_SAMPLER> {
public:
    Sampler() {}

    static Sampler create();

    void set(GLenum param, GLfloat in);
    void set(GLenum param, GLint in);
    void set(GLenum param, glm::vec4 in);

    void bind(GLuint unit = 0);
private:
    explicit Sampler(GLuint id);
    friend handle_type;
    void destroy();
};

class Texture : public Object<Texture, GL_TEXTURE> {
public:
    Texture() {}

    static Texture create_2d(GLenum format, GLsizei width, GLsizei height,
                             GLsizei levels);
    static Texture create_2d_array(GLenum format, GLsizei width, GLsizei height,
                                   GLsizei size, GLsizei levels);

    GLsizei get_width() const { return tex_width; }
    GLsizei get_height() const { return tex_height; }
    GLsizei get_depth() const { return tex_depth; }
    GLsizei get_levels() const { return tex_levels; }
    GLenum get_internal_format() const { return tex_format; }

    size_t get_memory_size() const;

    void set(GLenum param, GLfloat in);
    void set(GLenum param, GLint in);
    void set(GLenum param, glm::vec4 in);

    void write(GLint x_off, GLint y_off, GLsizei width, GLsizei height,
               GLenum in_fmt, GLenum in_comp, const void* data,
               GLint level = 0);
    void write(GLint x_off, GLint y_off, GLint z_off, GLsizei width,
               GLsizei height, GLsizei depth, GLenum in_fmt, GLenum in_comp,
               const void* data, GLint level = 0);

    void generate_mipmaps();

    void bind(GLenum target);

    void bind_unit(GLuint unit);

    void bind_image(GLuint unit, GLint layer, GLenum access, GLenum format,
                    GLint level = 0);
    void bind_image_layers(GLuint unit, GLenum access, GLenum format,
                           GLint level = 0);

private:
    explicit Texture(GLuint id, GLsizei width, GLsizei height, GLsizei depth,
                     GLenum format, GLsizei levels);
    friend handle_type;
    void destroy();

    GLsizei tex_width = 0,
            tex_height = 0,
            tex_depth = 0,
            tex_levels = 0;
    GLenum tex_format = GL_INVALID_ENUM;

    static GLuint create_texture();
};

}

#endif
