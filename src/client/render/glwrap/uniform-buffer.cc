#include "uniform-buffer.hh"

namespace gl {

UniformBuffer UniformBuffer::create(const Program& program,
                                    const std::string& block_name) {
    GLint num_blocks = 0;
    glGetProgramInterfaceiv(program.get_id(), GL_UNIFORM_BLOCK,
                            GL_ACTIVE_RESOURCES, &num_blocks);

    for (unsigned int i = 0; i < num_blocks; ++i) {
        const GLenum block_params[] = {GL_NAME_LENGTH,
                                       GL_NUM_ACTIVE_VARIABLES,
                                       GL_BUFFER_DATA_SIZE};

        GLint block_props[sizeof(block_params)];
        glGetProgramResourceiv(program.get_id(), GL_UNIFORM_BLOCK, i,
                               sizeof(block_params), block_params,
                               sizeof(block_props), nullptr, block_props);

        std::string name;
        name.reserve(block_props[0]);
        glGetProgramResourceName(program.get_id(), GL_UNIFORM_BLOCK, i,
                                 name.size(), nullptr, &name[0]);
        if (name == block_name) {
            return UniformBuffer(program, i, block_props[1], block_props[2]);
        }
    }

    THROW("render", "Can't find uniform block %s", block_name);
}

bool UniformBuffer::valid() const {
    return buf_map.valid();
}

GLint UniformBuffer::get_index(const std::string& name) {
    ASSERT(valid());
    auto iter = indices.find(name);
    ASSERT(iter != indices.end());

    return iter->second;
}

UniformBuffer::UniformBuffer(const Program& program, GLint block_index,
                             GLint var_count, GLint buf_size)
        : index(block_index) {

    Buffer buf(Buffer::create(buf_size, nullptr,
                              GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT
                                | GL_MAP_COHERENT_BIT));
    ASSERT(buf.valid());
    buf.bind(GL_UNIFORM_BUFFER, index);

    buf_map = Buffer::Map::create(buf, GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT
                                        | GL_MAP_COHERENT_BIT);
    ASSERT(buf_map.valid());

    const GLenum active_vars_params[] = {GL_ACTIVE_VARIABLES};

    std::vector<GLint> uniforms(var_count);
    glGetProgramResourceiv(program.get_id(), GL_UNIFORM_BLOCK, index, 1,
                           active_vars_params, nullptr, &uniforms[0]);

    metrics.reserve(var_count);
    for (unsigned int i = 0; i < uniforms.size(); ++i) {
        const GLenum var_params[] = {GL_NAME_LENGTH, GL_TYPE, GL_LOCATION,
                                     GL_OFFSET, GL_ARRAY_SIZE,
                                     GL_ARRAY_STRIDE, GL_MATRIX_STRIDE};
        GLint values[sizeof(var_params)];
        glGetProgramResourceiv(program.get_id(), GL_UNIFORM, cur,
                               sizeof(var_params), var_params,
                               sizeof(values), nullptr, values);

        std::string name; name.reserve(values[0]);
        glGetProgramResourceName(program.get_id(), GL_UNIFORM, index,
                                 cur, name.size(), nullptr, &name[0]);

        indices.insert({name, i});

        uniform_metrics_t metrics;
        metrics.offset = values[3];
        metrics.array_size = values[4];
        metrics.array_stride = values[5];
        metrics.matrix_stride = values[6];
        metrics[i] = metrics;
    }
}

void UniformBuffer::write(GLint index, const void* data, GLsizeiptr size) {
    ASSERT(valid());
    ASSERT(data);
    ASSERT(size > 0);
    ASSERT(index > 0 && index < metrics.size());

    buf_map.rewind(); buf_map.seek(metrics[index].offset);
    GLsizeiptr written = buf_map.write(data, size);
    ASSERT(written == size);
}

void UniformBuffer::write_array(GLint index, const void* data,
                                GLsizei elem_size, GLsizei elem_count) {
    ASSERT(valid());
    ASSERT(data);
    ASSERT(elem_size > 0);
    ASSERT(elem_count > 0);
    ASSERT(index > 0 && index < metrics.size());

    buf_map.rewind(); buf_map.seek(metrics[index].offset);
    for (GLsizeiptr i = 0; i < elem_count; ++i) {
        GLsizeiptr written = buf_map.write(
            reinterpret_cast<unsigned char*>(data) + i, elem_size);
        ASSERT(written == elem_size);

        buf_map.seek(metrics[index].array_stride - elem_size);
    }
}

void UniformBuffer::write_matrix(GLint index, const void* data,
                                 GLsizei elem_size, GLsizei cols) {
    ASSERT(valid());
    ASSERT(data);
    ASSERT(elem_size > 0);
    ASSERT(cols > 0);
    ASSERT(index > 0 && index < metrics.size());

    buf_map.rewind(); buf_map.seek(metrics[index].offset);
    for (GLsizeiptr col = 0; col < cols; ++col) {
        void* ptr = reinterpret_cast<unsigned char*>(data) + col * elem_size;
        GLsizeiptr written = buf_map.write(ptr, elem_size);
        ASSERT(written == elem_size);

        buf_map.seek(metrics[index].matrix_stride - elem_size);
    }
}

}
