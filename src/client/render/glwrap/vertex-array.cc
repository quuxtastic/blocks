#include "vertex-array.hh"

namespace gl {

VertexArray VertexArray::create() {
    GLuint id = GL_INVALID_ID;
    glCreateVertexArrays(1, &id);
    ASSERT(id > 0);

    return VertexArray(id);
}

void VertexArray::attach(GLuint index, Buffer buf, GLintptr offset,
                         GLintptr stride, GLuint divisor = 0) {
    ASSERT(valid());
    ASSERT(buf.valid());
    ASSERT(offset < buf.get_size());
    glBindVertexArrayVertexBuffer(get_id(), index, buf.get_id(), offset,
                                  stride);
    if (divisor > 0) glVertexArrayBindingDivisor(get_id(), index, divisor);
}

void VertexArray::set_attr(GLuint layout_index, GLint size, GLenum type,
                           GLuint offset, GLboolean normalized) {
    ASSERT(valid());
    glVertexArrayAttribFormat(layout_index, size, type, normalized, offset);
}

void VertexArray::bind(GLuint buf_index, GLuint attr_index) {
    ASSERT(valid());
    glVertexArrayAttribBinding(attr_index, buf_index);
}

void VertexArray::destroy() {
    ASSERT(valid());
    glDeleteVertexArrays(1, &obj_id);
    obj_id = GL_INVALID_ID;
}

VertexArray::VertexArray(GLuint id) : VertexArray::handle_type(id) {}

}
