namespace gl {

template<typename T> bool Buffer::Map::write(const T& in) {
    ASSERT(valid());

    return write(&in, sizeof(in)) == sizeof(in);
}

}
