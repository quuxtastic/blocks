#ifndef RENDER_GLWRAP_BUFFER_HH_
#define RENDER_GLWRAP_BUFFER_HH_ 1

#include "gl.hh"

namespace gl {

class Buffer : public Object<Buffer, GL_BUFFER> {
public:
    Buffer() {}

    static Buffer create(GLsizei size, const void* data, GLbitfield flags);

    GLsizei get_size() const { return buf_size; }

    void reset(GLsizei size, const void* data, GLbitfield flags);

    void write(GLsizei offset, GLsizei size, const void* data);

    class Map {
    public:
        Map();
        virtual ~Map();

        bool valid() const;

        GLsizeiptr write(const void* data, GLsizei size);
        template<typename T> bool write(const T& in);

        GLintptr tell() const;
        void rewind();

        void seek(int off);

        GLsizei get_size() const;

        void unmap();

        Buffer& get_buffer() { return buf; }

    private:
        Map(const Buffer& src_buf, const void* in_ptr, GLsizei size);
        friend Buffer;

        Buffer buf;

        void* map_ptr;

        GLsizei mapped_size;
        GLintptr mem_cur;
    };

    Map map(GLintptr offset, GLsizeiptr size, GLbitfield mode);
    Map map(GLbitfield mode);

    void copy_from(const Buffer& src, GLintptr read_off, GLintptr write_off,
                   GLsizeiptr size);

    void invalidate(GLintptr offset, GLsizeiptr size);
    void invalidate();

    void flush(GLintptr offset, GLsizeiptr size);
    void flush();

    void bind(GLuint target);
    void bind(GLuint target, GLuint index);
    void bind(GLuint target, GLuint index, GLintptr offset, GLsizeiptr size);

    static void unbind(GLuint target);
    static void unbind(GLuint target, GLuint index);

    static size_t get_count();
    static size_t get_memory_usage();

private:
    Buffer(GLuint id, GLsizei size);
    friend handle_type;
    void destroy();

    GLsizei buf_size = 0;

    static size_t count;
    static size_t bytes_count;
};

}

#include "buffer.inl"

#endif
