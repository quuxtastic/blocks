#ifndef RENDER_GLWRAP_SHADER_HH_
#define RENDER_GLWRAP_SHADER_HH_ 1

#include <string>
#include <glm/glm.hpp>

#include "gl.hh"

namespace gl {

class Shader : public Object<Shader, GL_SHADER> {
public:
    Shader() {}

    static Shader create(GLenum type, const std::string& source);

    bool compile_status();

    std::string get_log();

private:
    Shader(GLuint id, GLenum type);
    friend handle_type;
    void destroy();

    GLenum shader_type = GL_INVALID_ENUM;
};

class Program : public Object<Program, GL_PROGRAM> {
public:
    Program() {}

    static Program create();

    void attach(const Shader& shader);

    void link();
    bool link_status();

    bool validate();

    std::string get_log();

    GLint get_uniform_location(const std::string& name);

    void set_uniform(GLint loc, GLfloat in);
    void set_uniform(GLint loc, GLint in);
    void set_uniform(GLint loc, GLuint in);
    void set_uniform(GLint loc, glm::vec2 in);
    void set_uniform(GLint loc, glm::vec3 in);
    void set_uniform(GLint loc, glm::vec4 in);
    void set_uniform(GLint loc, const glm::mat4& in);

private:
    explicit Program(GLuint id);
    friend handle_type;
    void destroy();
};

}

#endif
