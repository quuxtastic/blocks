#ifndef RENDER_GLWRAP_UNIFORM_BUFFER_HH_
#define RENDER_GLWRAP_UNIFORM_BUFFER_HH_ 1

#include <string>
#include <map>
#include <unordered_map>

#include "buffer.hh"

namespace gl {

class UniformBuffer {
public:
    UniformBuffer() {}
    virtual ~UniformBuffer();

    static UniformBuffer create(const Program& program,
                                const std::string& block_name);

    bool valid() const;
    void release();

    GLint get_index(const std::string& name);

    template<typename T>
    void set(GLint index, const T& in);
    template<typename T>
    void set(const std::string& name, const T& in);

private:
    UniformBuffer(const Program& program, GLint block_index, GLint var_count,
                  GLint buf_size);

    void write(GLint index, const void* data, GLsizeiptr size);
    void write_array(GLint index, const void* data, GLsizei elem_size,
                     GLsizei elem_count);
    void write_matrix(GLint index, const void* data, GLsizei elem_size,
                      GLsizei cols);

    Buffer::Map buf_map;

    GLint index;

    std::map<std::string, GLint> indices;

    struct uniform_metrics_t {
        GLuint offset, array_size, array_stride, matrix_stride;
    };
    std::vector<uniform_metrics_t> metrics;
};

}

#include "uniform-buffer.inl"

#endif
