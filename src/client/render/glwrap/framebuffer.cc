#include "framebuffer.hh"
#include "texture.hh"

namespace gl {

Framebuffer Framebuffer::create() {
    GLuint id = GL_INVALID_ID;
    glCreateFramebuffer(1, &id);
    ASSERT(id > 0);

    return Framebuffer(id);
}

void Framebuffer::attach(const Texture& texture, GLenum attachment,
                         GLint level) {
    ASSERT(valid());
    ASSERT(level < texture.get_levels());
    glNamedFramebufferTexture(get_id(), attachment, texture.get_id(), level);
}

void Framebuffer::attach_layer(const Texture& texture, GLenum attachment,
                               GLint layer, GLint level) {
    ASSERT(valid());
    ASSERT(level < texture.get_levels());
    glNamedFramebufferTextureLayer(get_id(), attachment, texture.get_id(),
                                   level, layer);
}

GLenum Framebuffer::get_status(GLenum target) {
    ASSERT(valid());
    return glCheckNamedFramebufferStatus(get_id(), target);
}

bool Framebuffer::is_complete(GLenum target) {
    return get_status() == GL_FRAMEBUFFER_COMPLETE;
}

void Framebuffer::bind_draw(GLenum mode) {
    ASSERT(valid());
    glNamedFramebufferDrawBuffer(get_id(), mode);
}

void Framebuffer::bind(GLenum target) {
    ASSERT(valid());
    glBindFramebuffer(target, get_id());
}

Framebuffer::Framebuffer(GLuint id) : Framebuffer::handle_type(id) {}

void Framebuffer::destroy() {
    ASSERT(valid());
    glDeleteFramebuffers(1, &obj_id);
    obj_id = GL_INVALID_ID;
}

}
