#ifndef RENDER_GLWRAP_VERTEX_ARRAY_HH_
#define RENDER_GLWRAP_VERTEX_ARRAY_HH_ 1

namespace gl {

class VertexArray : public Object<VertexArray, GL_VERTEX_ARRAY> {
public:
    VertexArray() {}

    static VertexArray create();

    void attach(GLuint index, Buffer buf, GLintptr offset, GLintptr stride,
                GLuint divisor = 0);

    void set_attr(GLuint layout_index, GLint size, GLenum type, GLuint offset,
                  GLboolean normalized = GL_FALSE);

    void bind(GLuint buf_index, GLuint attr_index);

private:
    explicit VertexArray(GLuint id);
    friend handle_type;
    void destroy();
};

}

#endif
