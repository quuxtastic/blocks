#ifndef RENDER_GLWRAP_GL_HH_
#define RENDER_GLWRAP_GL_HH_ 1

#define GLEW_STATIC
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <string>

namespace gl {

template<typename T, GLuint TypeID>
class Object {
public:
    typedef Object<T, TypeID> handle_type;

    Object();
    Object(const Object& in);

    virtual ~Object();

    Object& operator=(const Object& in);

    int release();

    bool valid() const;
    int get_ref_count() const;

    GLuint get_id() const;
    constexpr GLuint get_gl_type() const;

    std::string get_label() const;

    void set_label(const std::string& name);

protected:
    explicit Object(GLuint id);

    void inc_ref() const;

    GLuint obj_id;

private:
    int* ref_count;
};

void enable_debug();
void disable_debug();

void ignore_debug_message(GLuint src, GLuint type, GLuint id);

}

#include "gl.inl"

#endif
