#include "util/error.hh"

namespace gl {

template<typename T, GLuint TypeID>
Object<T, TypeID>::Object() : obj_id(GL_INVALID_ID), ref_count(nullptr) {}

template<typename T, GLuint TypeID>
Object<T, TypeID>::Object(const Object& in)
        : obj_id(in.obj_id), ref_count(in.ref_count) {
    inc_ref();
}

template<typename T, GLuint TypeID>
Object<T, TypeID>::~Object() {
    release();
}

template<typename T, GLuint TypeID>
Object<T, TypeID>& Object<T, TypeID>::operator=(const Object& in) {
    in.inc_ref();

    release();

    ref_count = in.ref_count;
    obj_id = in.obj_id;

    return *this;
}

template<typename T, GLuint TypeID>
int Object<T, TypeID>::release() {
    if (valid()) {
        int count = --(*ref_count);
        if (count <= 0) {
            static_cast<T*>(this)->destroy();
            delete ref_count;
        }

        obj_id = GL_INVALID_ID;
        ref_count = nullptr;

        return count;
    }

    return 0;
}

template<typename T, GLuint TypeID>
bool Object<T, TypeID>::valid() const { return ref_count; }

template<typename T, GLuint TypeID>
int Object<T, TypeID>::get_ref_count() const {
    return valid() ? *ref_count : 0;
}

template<typename T, GLuint TypeID>
GLuint Object<T, TypeID>::get_id() const { return obj_id; }

template<typename T, GLuint TypeID>
constexpr GLuint Object<T, TypeID>::get_gl_type() const { return TypeID; }

template<typename T, GLuint TypeID>
std::string Object<T, TypeID>::get_label() const {
    ASSERT(valid());

    char buf[GL_MAX_LABEL_LENGTH];
    GLsizei out_len = 0;
    glGetObjectLabel(get_gl_type(), get_id(), sizeof(buf), &out_len, buf);
    return std::string(buf, out_len);
}

template<typename T, GLuint TypeID>
void Object<T, TypeID>::set_label(const std::string& name) {
    ASSERT(valid());
    glObjectLabel(get_gl_type(), get_id(), name.size(), name.c_str());
}

template<typename T, GLuint TypeID>
Object<T, TypeID>::Object(GLuint id) : obj_id(id), ref_count(new int(1)) {}

template<typename T, GLuint TypeID>
void Object<T, TypeID>::inc_ref() const {
    if (ref_count) ++(*ref_count);
}

}
