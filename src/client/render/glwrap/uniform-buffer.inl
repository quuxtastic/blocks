namespace gl {

template<typename T>
void UniformBuffer::set(GLint index, const T& in) {
    write(index, &in, sizeof(in));
}

template<> void UniformBuffer::set<glm::mat4>(GLint index, const mat4& in) {
    write_matrix(index, glm::value_ptr(in), sizeof(GLfloat) * 4, 4);
}

template<typename T>
void UniformBuffer::set(const std::string& name, const T& in) {
    set(get_index(name), in);
}

}
