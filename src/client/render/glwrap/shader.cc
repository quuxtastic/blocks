#include "shader.hh"

namespace gl {

Shader Shader::create(GLuint type, const std::string& source) {
    GLuint id = glCreateShader(type);
    ASSERT(id > 0);

    glShaderSource(id, 1, &source[0], nullptr);
    glCompileShader(id);

    return Shader(id, type);
}

bool Shader::compile_status() {
    ASSERT(valid());

    GLint status = 0;
    glGetShaderiv(get_id(), GL_COMPILE_STATUS, &status);

    return status == GL_TRUE;
}

std::string Shader::get_log() {
    ASSERT(valid());

    std::string out;

    GLint log_length = 0;
    glGetShaderiv(get_id(), GL_INFO_LOG_LENGTH, &log_length);
    if (log_length > 0) {
        out.reserve(log_length);
        glGetShaderInfoLog(get_id(), log_length, nullptr, &out[0]);
    }

    return out;
}

void Shader::destroy() {
    ASSERT(valid());
    glDeleteShader(get_id());
    obj_id = 0;
}

Shader::Shader(GLuint id, GLenum type)
    : Shader::handle_type(id), shader_type(type) {}

Program Program::create() {
    GLuint id = glCreateProgram();
    ASSERT(id > 0);
    return Program(id);
}

void Program::attach(const Shader& shader) {
    ASSERT(valid());
    ASSERT(shader.valid());
    ASSERT(shader.compile_status());

    glAttachShader(get_id(), shader.get_id());
}

void Program::link() {
    ASSERT(valid());

    glLinkProgram(get_id());
}

bool Program::link_status() {
    ASSERT(valid());

    GLint status = 0;
    glGetProgramiv(get_id(), GL_LINK_STATUS, &status);

    return status == GL_TRUE;
}

bool Program::validate() {
    ASSERT(valid());

    glValidateProgram(get_id());

    GLint status = 0;
    glGetProgramiv(get_id(), GL_VALIDATE_STATUS, &status);

    return status == GL_TRUE;
}

std::string Program::get_log() {
    ASSERT(valid());

    std::string out;

    GLint log_length = 0;
    glGetProgramiv(get_id(), GL_INFO_LOG_LENGTH, &log_length);
    if (log_length > 0) {
        out.reserve(log_length);
        glGetProgramInfoLog(get_id(), log_length, nullptr, &out[0]);
    }

    return out;
}

GLint Program::get_uniform_location(const std::string& name) {
    ASSERT(valid());

    GLint index = glGetProgramResourceLocation(get_id(), GL_UNIFORM,
                                               name.c_str());
    ASSERT(index != -1);

    return index;
}

void Program::set_uniform(GLint loc, GLfloat in) {
    ASSERT(valid());
    glProgramUniform1f(get_id(), loc, in);
}

void Program::set_uniform(GLint loc, GLint in) {
    ASSERT(valid());
    glProgramUniform1i(get_id(), loc, in);
}

void Program::set_uniform(GLint loc, GLuint in) {
    ASSERT(valid());
    glProgramUniform1ui(get_id(), loc, in);
}

void Program::set_uniform(GLint loc, glm::vec2 in) {
    ASSERT(valid());
    glProgramUniform2fv(get_id(), loc, 1, &in[0]);
}

void Program::set_uniform(GLint loc, glm::vec3 in) {
    ASSERT(valid());
    glProgramUniform3fv(get_id(), loc, 1, &in[0]);
}

void Program::set_uniform(GLint loc, glm::vec4 in) {
    ASSERT(valid());
    glProgramUniform4fv(get_id(), loc, 1, &in[0]);
}

void Program::set_uniform(GLint loc, const glm::mat4& in) {
    ASSERT(valid());
    glProgramUniformMatrix4fv(get_id(), loc, 1, GL_FALSE, glm::value_ptr(in));
}

void Program::destroy() {
    ASSERT(valid());
    glDeleteProgram(get_id());
    obj_id = 0;
}

Program::Program(GLuint id) : Program::handle_type(id) {}

}
