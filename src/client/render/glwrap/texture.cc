#include "texture.hh"

namespace gl {

Sampler Sampler::create() {
    GLuint id = GL_INVALID_ID;
    glCreateSamplers(1, &id);
    ASSERT(id > 0);

    return Sampler(id);
}

void Sampler::set(GLenum param, GLfloat in) {
    ASSERT(valid());
    glSamplerParameterf(get_id(), param, in);
}

void Sampler::set(GLenum param, GLint in) {
    ASSERT(valid());
    glSamplerParameteri(get_id(), param, in);
}

void Sampler::set(GLenum param, glm::vec4 in) {
    ASSERT(valid());
    glSamplerParemeterfv(get_id(), param, &in[0]);
}

void Sampler::bind(GLuint unit) {
    ASSERT(valid());
    glBindSampler(get_id(), unit);
}

void Sampler::destroy() {
    ASSERT(valid());
    glDeleteSamplers(1, &obj_id);
    obj_id = 0;
}

Sampler::Sampler(GLuint id) : Sampler::handle_type(id) {}

Texture Texture::create_2d(GLenum format, GLsizei width, GLsizei height,
                           GLsizei levels) {
    GLuint id = create_texture();
    glTextureStorage2D(id, levels, format, width, height);
    return Texture(id, width, height, 0, format, levels);
}

Texture Texture::create_2d_array(GLenum format, GLsizei width, GLsizei height,
                                 GLsizei size, GLsizei levels) {
    GLuint id = create_texture();
    glTextureStorage3D(id, levels, format, width, height, size);
    return Texture(id, width, height, size, format, levels);
}

void Texture::set(GLenum param, GLfloat in) {
    ASSERT(valid());
    glTextureParameterf(get_id(), param, in);
}

void Texture::set(GLenum param, GLint in) {
    ASSERT(valid());
    glTextureParameteri(param, in);
}

void Texture::set(GLenum param, glm::vec4 in) {
    ASSERT(valid());
    glTextureParameterfv(param, &in[0]);
}

void Texture::write(GLint x_off, GLint y_off, GLsizei width, GLsizei height,
                    GLenum in_fmt, GLenum in_comp, const void* data,
                    GLint level) {
    ASSERT(valid());
    ASSERT(data);
    ASSERT(x_off + width <= tex_width);
    ASSERT(y_off + height <= tex_height);
    ASSERT(level < tex_levels);
    glTextureSubImage2D(get_id(), level, x_off, y_off, width, height, in_fmt,
                        in_comp, data);
}

void Texture::write(GLint x_off, GLint y_off, GLint z_off, GLsizei width,
                    GLsizei height, GLsizei depth, GLenum in_fmt,
                    GLenum in_comp,
                    const void* data, GLint level) {
    ASSERT(valid());
    ASSERT(data);
    ASSERT(x_off + width <= tex_width);
    ASSERT(y_off + height <= tex_height);
    ASSERT(z_off + depth <= tex_depth);
    ASSERT(level < tex_levels);
    glTextureSubImage3D(get_id(), level, x_off, y_off, z_off, width, height,
                        depth, in_fmt, in_comp, data);
}

void Texture::generate_mipmaps() {
    ASSERT(valid());
    glGenerateTextureMipmap(get_id());
}

void Texture::bind(GLenum target) {
    ASSERT(valid());
    glBindTexture(target, get_id());
}

void Texture::bind_unit(GLint unit) {
    ASSERT(valid());
    glBindTextureUnit(unit, get_id());
}

void Texture::bind_image(GLuint unit, GLint layer, GLenum access, GLenum format,
                         GLsizei level) {
    ASSERT(valid());
    ASSERT(level < tex_levels);
    glBindImageTexture(unit, get_id(), level, GL_FALSE, layer, access, format);
}

void Texture::bind_image_layers(GLuint unit, GLenum access, GLenum format,
                                GLsizei level) {
    ASSERT(valid());
    ASSERT(level < tex_levels);
    glBindImageTexture(unit, get_id(), level, GL_TRUE, 0, access, format);
}

Texture::Texture(GLuint id, GLsizei width, GLsizei height, GLsizei depth,
                 GLenum format, GLsizei levels)
        : Texture::handle_type(id), tex_width(width), tex_height(height),
          tex_depth(depth), tex_format(format), tex_levels(levels) {}

void Texture::destroy() {
    ASSERT(valid());
    glDeleteTextures(1, &obj_id);
    obj_id = GL_INVALID_ID;
}

Gluint Texture::create_texture() {
    GLuint id = GL_INVALID_ID;
    glCreateTextures(1, &id);
    ASSERT(id > 0);

    return id;
}

}
