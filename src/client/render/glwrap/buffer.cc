#include "buffer.hh"
#include "util/error.hh"
#include "util/math.hh"

namespace gl {

Buffer Buffer::create(GLsizei size, const void* data, GLbitfield flags) {
    GLuint id = GL_INVALID_ID;
    glCreateBuffers(1, &id);
    ASSERT(id != GL_INVALID_ID);

    glNamedBufferStorage(id, size, data, flags);

    return Buffer(id, size);
}

void Buffer::reset(GLsizei size, const void* data, GLbitfield flags) {
    release();
    *this = create(size, data, flags);
}

void Buffer::write(GLsizei offset, GLsizei size, const void* data) {
    ASSERT(valid());
    ASSERT(size + offset <= buf_size);
    ASSERT(data);

    glNamedBufferSubData(get_id(), offset, size, data);
}

Buffer::Map Buffer::map(GLintptr offset, GLsizeiptr size, GLbitfield mode) {
    ASSERT(valid());
    ASSERT(size + offset <= buf_size);

    void* out = glMapNamedBufferRange(get_id(), offset, size, mode);
    ASSERT(out);

    return Map(*this, out, size);
}

Buffer::Map Buffer::map(GLbitfield mode) {
    return map(0, buf_size, mode);
}

void Buffer::copy_from(const Buffer& src, GLintptr read_off, GLintptr write_off,
                       GLsizeiptr size) {
    ASSERT(valid()); ASSERT(src.valid());
    ASSERT(read_off + size <= src.buf_size);
    ASSERT(write_off + size <= buf_size);

    glCopyNamedBufferSubData(src.get_id(), get_id(), read_off, write_off, size);
}

void Buffer::invalidate() {
    ASSERT(valid());
    glInvalidateBufferData(get_id());
}

void Buffer::invalidate(GLintptr offset, GLsizeiptr size) {
    ASSERT(valid());
    ASSERT(offset + size <= buf_size);
    glInvalidateBufferSubData(get_id(), offset, size);
}

void Buffer::flush(GLintptr offset, GLsizeiptr size) {
    ASSERT(valid());
    ASSERT(offset + size <= buf_size);
    glFlushMappedNamedBufferRange(get_id(), offset, size);
}

void Buffer::flush() {
    flush(0, buf_size);
}

void Buffer::bind(GLuint target) {
    ASSERT(valid());
    glBindBuffer(target, get_id());
}

void Buffer::bind(GLuint target, GLuint index) {
    ASSERT(valid());
    glBindBufferBase(target, index, get_id());
}

void Buffer::bind(GLuint target, GLuint index, GLintptr offset,
                  GLsizeiptr size) {
    ASSERT(valid());
    ASSERT(offset + size <= buf_size);
    glBindBufferRange(target, index, get_id(), offset, size);
}

void Buffer::unbind(GLuint target) {
    glBindBuffer(target, 0);
}

void Buffer::destroy() {
    ASSERT(valid());
    glDeleteBuffers(1, &obj_id);
    obj_id = GL_INVALID_ID;

    bytes_count -= buf_size;
    --count;
}

Buffer::Buffer(GLuint id, GLsizei size)
        : Buffer::handle_type(id), buf_size(size) {
    ++count;
    bytes_count += size;
}

size_t Buffer::count = 0;
size_t Buffer::bytes_count = 0;

size_t Buffer::get_count() { return count; }
size_t Buffer::get_memory_usage() { return bytes_count; }

Buffer::Map::Map() : map_ptr(nullptr), mapped_size(0), mem_cur(0) {}

Buffer::Map::~Map() {
    unmap();
}

bool Buffer::Map::valid() const { return map_ptr; }

GLsizeiptr Buffer::Map::write(const void* data, GLsizeiptr size) {
    ASSERT(valid());
    ASSERT(data);

    GLsizeiptr write_count = std::min(mapped_size - mem_cur, size);
    if (write_count > 0) {
        std::copy(mem_ptr + mem_cur, mem_ptr + mem_cur + size, data);
        mem_cur += write_count;
    }

    return write_count;
}

GLintptr Buffer::Map::tell() const {
    ASSERT(valid());
    return mem_cur;
}

void Buffer::Map::rewind() const {
    mem_cur = 0;
}

void Buffer::Map::seek(int off) {
    ASSERT(valid());
    mem_cur = clamp(mem_cur + off, 0, mapped_size - 1);
}

GLsizei Buffer::Map::get_size() const {
    ASSERT(valid())
    return mapped_size;
}

void Buffer::Map::unmap() {
    if (valid()) {
        glUnmapNamedBuffer(buf.get_id());
        map_ptr = nullptr;
        mapped_size = 0;
        mem_cur = 0;

        buf.release();
    }
}

Buffer::Map::Map(Buffer& src_buf, const void* in_ptr, GLsizei size)
    : buf(src_buf), map_ptr(in_ptr), mapped_size(size), mem_cur(0) {}
