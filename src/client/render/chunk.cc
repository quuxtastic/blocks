#include "chunk.hh"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "tesselate.hh"

ChunkMesh::ChunkMesh(int p, int q, int r) : Chunk(p, q, r) {
    model = glm::translate(glm::mat4(1.f),
        glm::vec3(
            p * Chunk::CHUNK_SIZE * Chunk::CUBE_SIZE,
            q * Chunk::CHUNK_SIZE * Chunk::CUBE_SIZE,
            r * Chunk::CHUNK_SIZE * Chunk::CUBE_SIZE));

    chunk_buf = Buffer::create(GL_ARRAY_BUFFER, 0, GL_DYNAMIC_DRAW);
    chunk_mesh = Mesh::create([&] (Mesh mesh) {
        mesh.attach(chunk_buf);
        mesh.set_attr(0, 4, GL_UNSIGNED_BYTE, GL_FALSE, 8, 0);
        mesh.set_attr(1, 4, GL_UNSIGNED_BYTE, GL_FALSE, 8, 4);
    });

    dirty(true);
}

void ChunkMesh::render(Shader shader) {
    if (terrain_ready() && !dirty()) {

        shader.set_uniform("model", model);
        chunk_mesh.bind();

        glDrawArrays(GL_TRIANGLES, 0, vertices);
    }
}

void ChunkMesh::post_load() {
    dirty_neighbors();
}

void ChunkMesh::dirty_neighbors() {
    if (front()) cast(front())->dirty(true);
    if (back()) cast(back())->dirty(true);
    if (left()) cast(left())->dirty(true);
    if (right()) cast(right())->dirty(true);
    if (top()) cast(top())->dirty(true);
    if (bottom()) cast(bottom())->dirty(true);
}

void ChunkMesh::tesselate(ChunkMesh::tesselate_fun_t fun) {
    if (!dirty() || !terrain_ready()) return;

    std::vector<GLubyte> verts;
    vertices = fun(
        *this,
        [] (Block block, size_t face, size_t, size_t, size_t) {
            switch (block) {
            case Block::GRASS:
                if (face == TOP_FACE) return 2;
                else return 3;
            case Block::STONE:
                return 1;
            case Block::SAND:
                return 4;
            case Block::WATER:
                return 5;
            default: break;
            }
            return 0;
        },
        verts);
    chunk_buf.reset(&verts[0], verts.size(), GL_DYNAMIC_DRAW);

    dirty(false);
}

void ChunkMesh::on_set(size_t x, size_t y, size_t z, Block) {
    if (z == 0 && front()) cast(front())->dirty(true);
    if (z == Chunk::CHUNK_SIZE-1 && back()) cast(back())->dirty(true);
    if (x == 0 && left()) cast(left())->dirty(true);
    if (x == Chunk::CHUNK_SIZE-1 && right()) cast(right())->dirty(true);
    if (y == 0 && bottom()) cast(bottom())->dirty(true);
    if (y == Chunk::CHUNK_SIZE-1 && top()) cast(top())->dirty(true);

    dirty(true);
}
