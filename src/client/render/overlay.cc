#include "overlay.hh"

#include <glm/gtc/matrix_transform.hpp>

#include "util/error.hh"

OverlayRenderer::OverlayRenderer() :
    overlay_shader(
        Shader::create_from_file("assets/shaders/overlay.program")) {}

void OverlayRenderer::set_viewport(unsigned int w, unsigned int h) {
    viewport_w = w;
    viewport_h = h;

    projection = glm::ortho(
        0.0f, static_cast<float>(w),
        0.0f, static_cast<float>(h));

    for (auto layer : layers) {
        for (auto child : layer) {
            child->update_rect(w, h);
        }
    }
}

void OverlayRenderer::render(float dtime) {
    overlay_shader.bind();
    overlay_shader.set_uniform("projection", projection);

    glDisable(GL_DEPTH_TEST);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    for (auto cur = layers.rbegin(); cur != layers.rend(); ++cur) {
        for (auto child : *cur) {
            child->render(overlay_shader, dtime);
        }
    }

    glDisable(GL_BLEND);
    glEnable(GL_DEPTH_TEST);
}

void OverlayRenderer::add(std::shared_ptr<Overlay> overlay) {
    layers.resize(std::max(
        layers.size(),
        static_cast<size_t>(overlay->rect.z + 1)));

    layers[overlay->rect.z].push_back(overlay);
    overlay->update_rect(viewport_w, viewport_h);
}

void Overlay::set_pos(unsigned int x, unsigned int y) {
    rect.x = x; rect.y = y;
    update_rect(viewport_w, viewport_h);
}

void Overlay::set_size(unsigned int w, unsigned int h) {
    rect.w = w; rect.h = h;
    update_rect(viewport_w, viewport_h);
    dirty = true;
}

void Overlay::set_color(glm::vec4 new_color) {
    color = new_color;
}

void Overlay::render(Shader shader, float) {
    if (is_visible) {
        build_geometry();

        mesh.bind();
        shader.set_uniform("model", model);
        shader.set_uniform("blend_color", color);
        shader.set_uniform<unsigned int>("overlay_mode", 0);

        glDrawArrays(GL_TRIANGLES, 0, 6);
    }
}

unsigned int Overlay::calc_rect_dim(unsigned int n, unsigned int align) {
    switch (align) {
    case screen_rect_t::ALIGN_TOP:
        return viewport_h - n - rect.h;
    case screen_rect_t::ALIGN_BOTTOM:
        return n;
    case screen_rect_t::ALIGN_LEFT:
        return n;
    case screen_rect_t::ALIGN_RIGHT:
        return viewport_w - n - rect.w;
    case screen_rect_t::ALIGN_CENTER_LEFT:
        return viewport_w / 2 - n - rect.w / 2;
    case screen_rect_t::ALIGN_CENTER_RIGHT:
        return viewport_w / 2 + n + rect.w / 2;
    case screen_rect_t::ALIGN_CENTER_TOP:
        return viewport_h / 2 - n - rect.h / 2;
    case screen_rect_t::ALIGN_CENTER_BOTTOM:
        return viewport_h / 2 + n + rect.h / 2;
    default:
        break;
    }

    THROW("render", "Unknown alignment %s", align);
    return 0;
}

void Overlay::update_rect(unsigned int in_w, unsigned int in_h) {
    viewport_w = in_w; viewport_h = in_h;

    unsigned int abs_x = calc_rect_dim(rect.x, rect.align_x);
    unsigned int abs_y = calc_rect_dim(rect.y, rect.align_y);

    model = glm::translate(glm::mat4(1.f), glm::vec3(abs_x, abs_y, 0.0f));
}

static void gen_rect(GLfloat left, GLfloat top, GLfloat right, GLfloat bottom,
        GLfloat tleft, GLfloat ttop, GLfloat tright, GLfloat tbottom,
        std::array<GLfloat, 24>& out) {
    size_t ind = 0;

    out[ind++] = left;      out[ind++] = top;
    out[ind++] = tleft;     out[ind++] = ttop;
    out[ind++] = left;      out[ind++] = bottom;
    out[ind++] = tleft;     out[ind++] = tbottom;
    out[ind++] = right;     out[ind++] = bottom;
    out[ind++] = tright;    out[ind++] = tbottom;

    out[ind++] = left;      out[ind++] = top;
    out[ind++] = tleft;     out[ind++] = ttop;
    out[ind++] = right;     out[ind++] = bottom;
    out[ind++] = tright;    out[ind++] = tbottom;
    out[ind++] = right;     out[ind++] = top;
    out[ind++] = tright;    out[ind++] = ttop;
}

void Overlay::build_geometry() {
    if (!dirty) return;

    if (!mesh.valid()) {
        buf = Buffer::create(GL_ARRAY_BUFFER, 0, GL_DYNAMIC_DRAW);
        mesh = Mesh::create([this] (Mesh the_mesh) {
            the_mesh.attach(this->buf);
            the_mesh.set_attr(0, 4, GL_FLOAT, GL_FALSE,
                4 * sizeof(GLfloat), 0);
        });
    }

    std::array<GLfloat, 24> vertices;
    gen_rect(0, rect.h, rect.w, 0, 0, 0, 1, 1, vertices);

    buf.reset(&vertices[0], vertices.size() * sizeof(GLfloat),
        GL_DYNAMIC_DRAW);

    dirty = false;
}

void TextOverlay::render(Shader shader, float) {
    if (is_visible) {
        build_geometry();

        font.bind();
        mesh.bind();
        shader.set_uniform("model", model);
        shader.set_uniform("blend_color", color);
        shader.set_uniform<unsigned int>("overlay_mode", 2);

        glDrawArrays(GL_TRIANGLES, 0, text.size() * 6);
    }
}

void TextOverlay::build_geometry() {
    if (!dirty) return;

    if (!mesh.valid()) {
        buf = Buffer::create(GL_ARRAY_BUFFER, 0, GL_DYNAMIC_DRAW);
        mesh = Mesh::create([this] (Mesh the_mesh) {
            the_mesh.attach(this->buf);
            the_mesh.set_attr(0, 4, GL_FLOAT, GL_FALSE,
                4 * sizeof(GLfloat), 0);
        });
    }

    std::vector<std::array<GLfloat, 24>> vertices;

    GLfloat x_cur = 0, y_cur = 0;
    for (auto cur : text) {
        const Font::glyph_t& glyph = font.get_glyph(cur);

        GLfloat xpos = x_cur + glyph.x;
        GLfloat ypos = y_cur - (glyph.h - glyph.y);

        x_cur += glyph.adv_x; y_cur += glyph.adv_y;

        // skip empty characters
        if (glyph.w <= 0 || glyph.h <= 0) continue;

        std::array<GLfloat, 24> rect_data;
        gen_rect(
            xpos, ypos + glyph.h, xpos + glyph.w, ypos,
            glyph.tx, glyph.ty, glyph.tx + glyph.tw, glyph.ty + glyph.th,
            rect_data);
        vertices.push_back(rect_data);

        rect.w = xpos + glyph.w;
        rect.h = std::max(glyph.h, ypos + glyph.h);
    }

    buf.reset(&vertices[0], vertices.size() * sizeof(std::array<GLfloat, 24>),
        GL_DYNAMIC_DRAW);

    dirty = false;
}

void GeomOverlay::render(Shader shader, float) {
    if (is_visible) {
        build_geometry();

        mesh.bind();
        shader.set_uniform("model", model);
        shader.set_uniform("blend_color", color);
        shader.set_uniform<unsigned int>("overlay_mode", 0);

        glDrawArrays(vertex_type, 0, vertex_count);
    }
}

void GeomOverlay::build_geometry() {
    if (!dirty) return;

    if (!mesh.valid()) {
        mesh = Mesh::create([this] (Mesh the_mesh) {
            the_mesh.attach(this->buf);
            the_mesh.set_attr(0, 4, GL_FLOAT, GL_FALSE,
                4 * sizeof(GLfloat), 0);
        });
    }

    dirty = false;
}

void ImageOverlay::render(Shader shader, float) {
    if (is_visible) {
        build_geometry();

        image.bind();
        mesh.bind();
        shader.set_uniform("model", model);
        shader.set_uniform("blend_color", color);
        shader.set_uniform<unsigned int>("overlay_mode", 1);

        glDrawArrays(GL_TRIANGLES, 0, 6);
    }
}
