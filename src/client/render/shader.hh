#ifndef RENDER_SHADER_HH_
#define RENDER_SHADER_HH_ 1

#include <string>
#include <vector>

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "gl.hh"

class Shader : public GlObjectHandle<Shader, GL_PROGRAM> {
public:
    typedef GlObjectHandle<Shader, GL_PROGRAM> handle_type;

    Shader() {}
    virtual ~Shader() {}

    template<typename... Args>
    static Shader create(Args&&... args) {
        Shader shader(glCreateProgram());

        std::vector<GLuint> parts;

        shader.attach_each(parts, args...);
        shader.compile();

        for (GLuint id : parts) {
            glDeleteShader(id);
        }

        return shader;
    }

    template<typename... Args>
    static Shader create_from_files(Args&&... args) {
        Shader shader(glCreateProgram());

        std::vector<GLuint> parts;

        shader.attach_file_each(parts, args...);
        shader.compile();

        for (GLuint id : parts) {
            glDeleteShader(id);
        }

        return shader;
    }

    static Shader create_from_file(const std::string& filename) {
        Shader shader(glCreateProgram());

        shader.attach_all(filename);
        shader.compile();

        return shader;
    }

    void bind();

    GLint get_uniform(const std::string& name) {
        return glGetUniformLocation(handle_type::get_id(), name.c_str());
    }

    template<typename T>
    void set_uniform(const std::string& name, T&& val) {
        set_uniform(get_uniform(name), val);
    }

    void set_uniform(GLint loc, glm::vec4 v) {
        glUniform4fv(loc, 1, &v[0]);
    }
    void set_uniform(GLint loc, glm::vec3 v) {
        glUniform3fv(loc, 1, &v[0]);
    }
    void set_uniform(GLint loc, glm::vec2 v) {
        glUniform2fv(loc, 1, &v[0]);
    }

    void set_uniform(GLint loc, int n) {
        glUniform1i(loc, n);
    }
    void set_uniform(GLint loc, unsigned int n) {
        glUniform1ui(loc, n);
    }

    void set_uniform(GLint loc, float n) {
        glUniform1f(loc, n);
    }

    void set_uniform(GLint loc, const glm::mat4& m) {
        glUniformMatrix4fv(loc, 1, GL_FALSE, glm::value_ptr(m));
    }

protected:
    explicit Shader(GLuint id) :
        handle_type(id) {}

    friend handle_type;
    void destroy();

    template<typename... Args>
    void attach_each(std::vector<GLuint>& parts, GLuint type,
            const std::string& program, Args&&... args) {

        parts.push_back(attach(type, program));
        attach_each(parts, args...);
    }
    void attach_each(std::vector<GLuint>&) {}
    template<typename... Args>
    void attach_file_each(std::vector<GLuint>& parts, GLuint type,
            const std::string& program, Args&&... args) {

        parts.push_back(attach_file(type, program));
        attach_file_each(parts, args...);
    }
    void attach_file_each(std::vector<GLuint>&) {}

    void attach_all(const std::string& filename);

    GLuint attach(GLuint type, const std::string& program,
        const char* name = nullptr);
    GLuint attach_file(GLuint type, const std::string& filename);
    void compile();
};

#endif
