#ifndef RENDER_GL_HH_
#define RENDER_GL_HH_ 1

// must be included in this order!
#define GLEW_STATIC
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "util/log.hh"

void enable_gl_debug();
void disable_gl_debug();

void ignore_gl_debug_message(GLuint src, GLuint type, GLuint id);
template<typename... Args>
void ignore_gl_debug_message(GLuint src, GLuint type, GLuint id,
        Args&&... args) {

    ignore_gl_debug_message(src, type, id);
    ignore_gl_debug_message(args...);
}

const char* translate_gl_error(GLenum error);

#include <string>

template<typename T, GLuint TypeID>
class GlObjectHandle {
public:

    static const GLuint INVALID_ID = -1;

    GlObjectHandle() :
            obj_id(INVALID_ID),
            ref_count(nullptr) {}

    GlObjectHandle(const GlObjectHandle& in) :
            obj_id(in.obj_id),
            ref_count(in.ref_count) {

        inc_ref();
    }
    virtual ~GlObjectHandle() {
        release();
    }

    GlObjectHandle& operator=(const GlObjectHandle& in) {
        if (ref_count != in.ref_count) release();

        ref_count = in.ref_count;
        inc_ref();

        obj_id = in.obj_id;

        return *this;
    }

    int release() {
        if (ref_count) {
            int count = --(*ref_count);

            if (count <= 0) {
                static_cast<T*>(this)->destroy();
                delete ref_count;
            }
            ref_count = nullptr;

            obj_id = INVALID_ID;

            return count;
        }

        return 0;
    }

    GLuint get_id() const { return obj_id; }
    GLuint constexpr get_gl_type() const { return TypeID; }

    std::string get_label() const {
        char buf[GL_MAX_LABEL_LENGTH];
        glGetObjectLabel(get_gl_type(), get_id(), sizeof(buf), nullptr, buf);
        return std::string(buf);
    }

    void set_label(const std::string& name) {
        glObjectLabel(get_gl_type(), get_id(), name.size(), name.c_str());
    }

    bool valid() const { return obj_id != INVALID_ID; }

    int get_ref_count() const { return ref_count ? *ref_count : 0; }

protected:
    explicit GlObjectHandle(GLuint id) :
            obj_id(id),
            ref_count(new int(1)) {}
    GlObjectHandle(GLuint id, const std::string& name) :
            GlObjectHandle(id) {

        glObjectLabel(get_gl_type(), get_id(), name.length(), name.c_str());
    }

    void inc_ref() {
        if(ref_count) ++(*ref_count);
    }

    GLuint obj_id;

private:
    int* ref_count;
};

#endif
