#ifndef RENDER_OVERLAY_HH_
#define RENDER_OVERLAY_HH_ 1

#include <memory>
#include <string>
#include <glm/glm.hpp>

#include "shader.hh"
#include "mesh.hh"
#include "buffer.hh"
#include "font.hh"
#include "texture.hh"

class Overlay;

class OverlayRenderer {
public:
    OverlayRenderer();

    void set_viewport(unsigned int w, unsigned int h);
    void render(float dtime);

    void add(std::shared_ptr<Overlay> overlay);

private:
    Shader overlay_shader;
    glm::mat4 projection;

    Mesh overlay_mesh;
    std::vector<std::vector<std::shared_ptr<Overlay>>> layers;

    unsigned int viewport_w = 0, viewport_h = 0;
};

class Overlay {
public:
    struct screen_rect_t {
        enum {
            ALIGN_TOP = 0,
            ALIGN_BOTTOM,
            ALIGN_LEFT,
            ALIGN_RIGHT,

            ALIGN_CENTER_LEFT,
            ALIGN_CENTER_RIGHT,
            ALIGN_CENTER_TOP,
            ALIGN_CENTER_BOTTOM
        };

        unsigned int x, y, w, h;
        unsigned int align_x, align_y;

        unsigned int z;
    };

    Overlay(screen_rect_t in_rect, glm::vec4 in_color) :
            rect(in_rect), color(in_color) {}
    virtual ~Overlay() {}

    virtual void set_pos(unsigned int x, unsigned int y);
    virtual void set_size(unsigned int w, unsigned int h);
    virtual void set_color(glm::vec4 new_color);

    void hide() { is_visible = false; }
    void show() { is_visible = true; }

protected:
    friend class OverlayRenderer;

    virtual void render(Shader shader, float dtime);

    virtual unsigned int calc_rect_dim(unsigned int n, unsigned int mod);
    virtual void update_rect(unsigned int in_w, unsigned int in_h);

    virtual void build_geometry();

    screen_rect_t rect;
    unsigned int viewport_w, viewport_h;
    bool dirty = true;

    glm::vec4 color;

    bool is_visible = true;

    Buffer buf;
    Mesh mesh;
    glm::mat4 model;
};

class TextOverlay : public Overlay {
public:
    TextOverlay(screen_rect_t rect, glm::vec4 color,
            const std::string& in_text, Font in_font, size_t in_max_len = 0) :
            Overlay(rect, color),
            font(in_font), max_len(in_max_len) {

        set_text(in_text);
        build_geometry();
    }

    virtual void set_size(unsigned int, unsigned int) {}

    void set_text(const std::string& in_text) {
        text = in_text;
        dirty = true;
    }

protected:
    virtual void render(Shader shader, float dtime);
    virtual void build_geometry();

    Font font;
    size_t max_len;
    std::string text;
};

class GeomOverlay : public Overlay {
public:
    GeomOverlay(screen_rect_t in_rect, glm::vec4 in_color,
            Buffer in_buf, GLuint in_vertex_type, size_t in_vertex_count) :
            Overlay(in_rect, in_color),
            vertex_type(in_vertex_type), vertex_count(in_vertex_count) {
        buf = in_buf;
    }

    virtual void set_size(unsigned int, unsigned int) {}

protected:
    virtual void render(Shader shader, float dtime);
    virtual void build_geometry();

    GLuint vertex_type;
    size_t vertex_count;
};

class ImageOverlay : public Overlay {
public:
    ImageOverlay(screen_rect_t in_rect, glm::vec4 in_color,
        Texture in_img) :
        Overlay(in_rect, in_color),
        image(in_img) {}

protected:
    virtual void render(Shader shader, float dtime);

    Texture image;
};

#endif
