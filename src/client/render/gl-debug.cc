#include "gl.hh"

#include "util/log.hh"
#include "util/error.hh"

const char* translate_gl_error(GLenum error) {
    switch(error) {
    case GL_INVALID_ENUM:
        return "invalid enum";
    case GL_INVALID_VALUE:
        return "invalid value";
    case GL_INVALID_OPERATION:
        return "invalid operation";
    case GL_STACK_OVERFLOW:
        return "stack overflow";
    case GL_STACK_UNDERFLOW:
        return "stack underflow";
    case GL_OUT_OF_MEMORY:
        return "out of memory";
    case GL_TABLE_TOO_LARGE:
        return "table too large";
    default:
        return "unknown";
    }
}

static msglog::MessageType translate_gl_debug_severity(GLenum severity) {
    switch (severity) {
    case GL_DEBUG_SEVERITY_HIGH:
        return msglog::MSG_ERROR;
    case GL_DEBUG_SEVERITY_MEDIUM:
        return msglog::MSG_WARN;
    case GL_DEBUG_SEVERITY_LOW:
        return msglog::MSG_WARN;
    case GL_DEBUG_SEVERITY_NOTIFICATION:
        return msglog::MSG_INFO;
    default:
        return msglog::MSG_OTHER;
    }
}

static const char* translate_gl_debug_source(GLenum source) {
    switch(source) {
    case GL_DEBUG_SOURCE_API:
        return "gl-dbg-opengl";
    case GL_DEBUG_SOURCE_WINDOW_SYSTEM:
        return "gl-dbg-windowsystem";
    case GL_DEBUG_SOURCE_SHADER_COMPILER:
        return "gl-dbg-shader";
    case GL_DEBUG_SOURCE_THIRD_PARTY:
        return "gl-dbg-thirdparty";
    case GL_DEBUG_SOURCE_APPLICATION:
        return "gl-dbg-user";
    default:
        return "gl-dbg-other";
    }
}

static const char* translate_gl_debug_type(GLenum type) {
    switch(type) {
    case GL_DEBUG_TYPE_ERROR:
        return "error";
    case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
        return "deprecated";
    case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
        return "undefined";
    case GL_DEBUG_TYPE_PORTABILITY:
        return "unportable";
    case GL_DEBUG_TYPE_PERFORMANCE:
        return "performance";
    case GL_DEBUG_TYPE_MARKER:
        return "marker";
    case GL_DEBUG_TYPE_PUSH_GROUP:
        return "push group";
    case GL_DEBUG_TYPE_POP_GROUP:
        return "pop group";
    default:
        return "other";
    }
}

static void APIENTRY gl_debug_cb(GLenum source, GLenum type, GLuint id,
        GLenum severity, GLsizei, const GLchar* message, const void*) {

    msglog::write(translate_gl_debug_severity(severity),
        "?", "?", 0, translate_gl_debug_source(source),
        "%s: #%s %s",
        translate_gl_debug_type(type), id, message);
}

void enable_gl_debug() {
    glEnable(GL_DEBUG_OUTPUT);
    glDebugMessageCallback(gl_debug_cb, nullptr);
    glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE,
        GL_DEBUG_SEVERITY_NOTIFICATION,
        0, nullptr, GL_TRUE);

    // double-check that debug log works
    glDebugMessageInsert(GL_DEBUG_SOURCE_APPLICATION, GL_DEBUG_TYPE_MARKER,
        0, GL_DEBUG_SEVERITY_NOTIFICATION, -1, "Debug log start");
}

void disable_gl_debug() {
    glDisable(GL_DEBUG_OUTPUT);
}

void ignore_gl_debug_message(GLuint src, GLuint type, GLuint id) {
    glDebugMessageControl(src, type, GL_DONT_CARE, 1, &id, GL_FALSE);
}
