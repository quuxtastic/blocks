#include "buffer.hh"

#include <cstring>

Buffer Buffer::create(GLuint target, GLsizeiptr size, GLuint usage) {
    GLuint id;
    glGenBuffers(1, &id);
    glBindBuffer(target, id);

    glBufferData(target, size, nullptr, usage);

    glBindBuffer(target, 0);

    return Buffer(id, target);
}

Buffer Buffer::create(GLuint target, const void* data, GLsizeiptr size,
        GLenum usage) {

    GLuint id;
    glGenBuffers(1, &id);
    glBindBuffer(target, id);

    glBufferData(target, size, data, usage);

    glBindBuffer(target, 0);

    return Buffer(id, target);
}

void Buffer::copy(Buffer src, GLintptr read_off, GLintptr write_off,
        GLsizeiptr size) {

    glBindBuffer(GL_COPY_READ_BUFFER, src.obj_id);
    glBindBuffer(GL_COPY_WRITE_BUFFER, obj_id);

    glCopyBufferSubData(GL_COPY_READ_BUFFER, GL_COPY_WRITE_BUFFER,
        read_off, write_off, size);

    glBindBuffer(GL_COPY_READ_BUFFER, 0);
    glBindBuffer(GL_COPY_WRITE_BUFFER, 0);
}

void Buffer::dirty(GLintptr offset, GLsizeiptr size) {
    glInvalidateBufferSubData(obj_id, offset, size);
}

void Buffer::destroy() {
    glDeleteBuffers(1, &obj_id);
}

void Buffer::write(GLsizei offset, GLsizei size, const void* data) {
    glBindBuffer(target, obj_id);
    glBufferSubData(target, offset, size, data);
    glBindBuffer(target, 0);
}

void Buffer::reset(const void* data, GLsizeiptr size, GLenum usage) {
    glBindBuffer(target, obj_id);
    glBufferData(target, size, data, usage);
    glBindBuffer(target, 0);
}
