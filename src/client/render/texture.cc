#include "texture.hh"

#include <limits>

#include <SOIL.h>

#include "util/error.hh"

const Texture::texture_setup_cb_t Texture::TextureSetupNoop =
    [](GLuint, GLuint) {};

Texture Texture::d2(GLuint type, GLuint format,
        const void* image_data, int w, int h,
        GLuint src_format, GLuint src_data_type,
        Texture::texture_setup_cb_t setup_fun, int gen_mipmaps) {

    GLuint id = 0;
    glGenTextures(1, &id);

    glBindTexture(type, id);

    setup_fun(id, type);

    glTexImage2D(type, gen_mipmaps, format, w, h, 0, src_format, src_data_type,
        image_data);
    if (gen_mipmaps == 0) {
        glGenerateMipmap(type);
    }

    glBindTexture(type, 0);

    return Texture(id, type);
}
Texture Texture::d2(GLuint type, GLuint format,
        const std::string& filename, int channels,
        GLuint src_format, GLuint src_data_type,
        Texture::texture_setup_cb_t setup_fun, int gen_mipmaps) {

    int w, h;
    unsigned char* image_data = SOIL_load_image(filename.c_str(), &w, &h,
        nullptr, channels);
    if (!image_data) {
        THROW("render", "SOIL failed to load '%s'", filename);
    }

    Texture tex = d2(type, format, image_data, w, h, src_format, src_data_type,
        setup_fun, gen_mipmaps);

    SOIL_free_image_data(image_data);

    return tex;
}

Texture Texture::array(GLuint type, GLuint format,
        const void* image_data, int w, int h, int d,
        GLuint src_fmt, GLuint src_data_type,
        Texture::texture_setup_cb_t setup_fun, int gen_mipmaps) {

    GLuint id = 0;
    glGenTextures(1, &id);
    glBindTexture(type, id);

    setup_fun(id, type);

    glTexStorage3D(type, gen_mipmaps, format, w, h, d);
    glTexSubImage3D(type, 0, 0, 0, 0, w, h, d, src_fmt, src_data_type,
        image_data);

    return Texture(id, type);
}

Texture Texture::array(GLuint type, GLuint format,
        const std::vector<std::string>& filenames, int channels,
        GLuint src_fmt, GLuint src_data_type,
        Texture::texture_setup_cb_t setup_fun, int gen_mipmaps) {

    int w, h;
    int min_w = std::numeric_limits<int>::max(),
        min_h = std::numeric_limits<int>::max();
    std::vector<unsigned char*> images;
    for (auto cur : filenames) {
        unsigned char* img = SOIL_load_image(cur.c_str(), &w, &h, nullptr,
            channels);
        if (!img) THROW("render", "SOIL failed to load '%s'", cur);
        images.push_back(img);

        min_w = std::min(min_w, w); min_h = std::min(min_h, h);
    }

    GLuint id = 0;
    glGenTextures(1, &id);
    glBindTexture(type, id);

    setup_fun(id, type);

    glTexStorage3D(type, gen_mipmaps, format, min_w, min_h,
        images.size());
    for (size_t i = 0; i < images.size(); ++i) {
        glTexSubImage3D(type, 0, 0, 0, i, min_w, min_h, 1,
            src_fmt, src_data_type, images[i]);
        SOIL_free_image_data(images[i]);
    }

    return Texture(id, type);
}

Texture Texture::array(GLuint, GLuint,
        const std::string&, int,
        size_t, size_t, size_t, size_t,
        GLuint, GLuint,
        Texture::texture_setup_cb_t, int) {

    THROW("render", "Not implemented");

    return Texture(0, 0);
}

void Texture::bind(int unit) {
    glActiveTexture(GL_TEXTURE0 + unit);
    glBindTexture(type, obj_id);
}

void Texture::destroy() {
    glDeleteTextures(1, &obj_id);
}
