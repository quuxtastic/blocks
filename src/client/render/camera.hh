#ifndef RENDER_CAMERA_HH_
#define RENDER_CAMERA_HH_ 1

#include <glm/glm.hpp>

#include "gl.hh"
#include "util/math.hh"

class Camera {
public:
    Camera();

    glm::mat4 get_view() { return view; }
    glm::mat4 get_projection() { return projection; }

    glm::vec3 get_pos() { return position; }
    glm::vec3 get_front() { return front; }

    void set_viewport(int w, int h);

    void set_pos(glm::vec3 in_pos);
    void set_front(glm::vec3 in_dir);
    void set_up(glm::vec3 in_up);

    void set_fov(float in_fov);

    void set_move_speed(float in_spd);
    void set_pan_speed(float in_spd);
    void set_zoom_speed(float in_spd);

    void move_z(float delta);
    void move_x(float delta);
    void move_y(float delta);
    void yaw(float angle);
    void pitch(float angle);
    void zoom(float delta);

private:
    static const glm::vec3 world_up;

    glm::vec3 position, front, right, up;
    float yaw_angle = -90.0f, pitch_angle = 0.0f;

    float move_factor = 1.0f;
    float pan_factor = 0.25f;

    float fov = 45.0f;
    float zoom_min = 1.0f, zoom_max = 45.0f, zoom_factor = 1.0f;

    float viewport_w = 640, viewport_h = 480;

    glm::mat4 view, projection;

    Frustum frustum, normalized_frustum;

    struct {
        glm::vec3 far_top_left, far_top_right,
            far_bottom_left, far_bottom_right;
        glm::vec3 near_top_left, near_top_right,
            near_bottom_left, near_bottom_right;
    } frustum_corners;

    void set_vectors();

    void set_matrices();

    void set_frustum();
};

#endif
