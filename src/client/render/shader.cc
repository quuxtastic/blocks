#include "shader.hh"

#include "util/error.hh"
#include "util/log.hh"
#include "util/file.hh"

GLuint Shader::attach(GLuint type, const std::string& program,
        const char* name) {

    GLuint id = glCreateShader(type);

    const char* psrc = program.c_str();
    glShaderSource(id, 1, &psrc, nullptr);
    glCompileShader(id);

    GLint status;
    glGetShaderiv(id, GL_COMPILE_STATUS, &status);

    if (!status) {
        GLchar log_buf[512];
        glGetShaderInfoLog(id, sizeof(log_buf), nullptr, log_buf);
        THROW("render", "Shader %s:%s compilation failed: %s",
            id, name ? name : "", log_buf);
    }

    glAttachShader(obj_id, id);

    return id;
}

GLuint Shader::attach_file(GLuint type, const std::string& filename) {
    return attach(type, load_file_to_string(filename), filename.c_str());
}

static std::pair<bool, GLuint> match_shader_type(const std::string& line) {
    if (line == "vertex:") {
        return {true, GL_VERTEX_SHADER};
    } else if (line == "fragment:") {
        return {true, GL_FRAGMENT_SHADER};
    } else if (line == "geometry:") {
        return {true, GL_GEOMETRY_SHADER};
    } else if (line == "tesselation control:") {
        return {true, GL_TESS_CONTROL_SHADER};
    } else if (line == "tesselation eval:") {
        return {true, GL_TESS_EVALUATION_SHADER};
    }

    return {false, 0};
}

static const char* translate_shader_type(GLenum type) {
    switch (type) {
    case GL_VERTEX_SHADER:
        return "vertex";
    case GL_FRAGMENT_SHADER:
        return "fragment";
    case GL_GEOMETRY_SHADER:
        return "geometry";
    case GL_TESS_CONTROL_SHADER:
        return "tess-control";
    case GL_TESS_EVALUATION_SHADER:
        return "tess-eval";
    }

    return "unknown";
}

void Shader::attach_all(const std::string& filename) {
    std::ifstream file(filename, std::ios::in | std::ios::binary);
    if (!file) {
        THROW("render", "Can't open shader source file '%s'", filename);
    }

    std::vector<GLuint> parts;

    std::stringstream ss;
    std::pair<bool, GLuint> cur(false, 0);
    load_file_by_line(filename, [&](const std::string& line, unsigned int) {
        auto next = match_shader_type(line);
        if (next.first) {
            if (cur.first) {
                parts.push_back(attach(cur.second, ss.str(),
                    fmt("%s|%s", filename,
                        translate_shader_type(cur.second)).c_str()));
                ss.str("");
            }

            cur = next;
        } else {
            if (!cur.first) {
                THROW("render", "Combined shader file %s has invalid format",
                    filename);
            }

            ss << line << "\n";
        }
    });
    parts.push_back(attach(cur.second, ss.str(),
        fmt("%s|%s", filename,
            translate_shader_type(cur.second)).c_str()));

    compile();

    for (GLuint cur : parts) {
        glDeleteShader(cur);
    }
}

void Shader::compile() {
    glLinkProgram(obj_id);

    GLint status;
    glGetProgramiv(obj_id, GL_LINK_STATUS, &status);

    if (!status) {
        GLchar log_buf[512];
        glGetProgramInfoLog(obj_id, sizeof(log_buf), nullptr, log_buf);
        THROW("render", "Program %s compilation failed: %s", obj_id, log_buf);
    }
}

void Shader::bind() {
    glUseProgram(obj_id);
}

void Shader::destroy() {
    glDeleteProgram(obj_id);
}
