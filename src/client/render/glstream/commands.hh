#ifndef RENDER_GLSTREAM_COMMANDS_HH_
#define RENDER_GLSTREAM_COMMANDS_HH_ 1

#include "comand-stream.hh"

namespace glstream {

class EnableDisableCommand : public BaseCommand {
public:
    static void execute_enable(const EnableDisableCommand& cmd);
    static void execute_disable(const EnableDisableCommand& cmd);
private:
    unsigned int state;
};

class CreateStaticBufferCommand : public BaseCommand {
public:
    static void execute(const CreateStaticBufferCommand& cmd);
private:
    void* buffer_data;
    size_t buffer_size;
};

}

#endif

