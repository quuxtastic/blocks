#ifndef RENDER_GLSTREAM_COMMAND_STREAM_HH_
#define RENDER_GLSTREAM_COMMAND_STREAM_HH_ 1

namespace glstream {

class BaseCommand {
public:
    typedef void (execute_fun_t)(BaseCommand&);
private:
    execute_fun_t execute_fun;
};

class CommandStream {
public:
    CommandStream(size_t max_command_count);
    virtual ~CommandStream();

    void push(BaseCommand& cmd);

    void _execute();
private:

    union largest_command_t {
        BaseCommand base;
    };

    std::vector<largest_command_t> commands;
    size_t command_count = 0;
};

}

#endif
