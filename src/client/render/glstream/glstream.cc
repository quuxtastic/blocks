#include "util/ring-buffer.hh"

void render_thread() {
    float last_time = glfwGetTime();

    BaseCommand command;

    bool keep_running = true;
    while (keep_running) {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        while (command_stream.try_pop(command)) {
            command.execute();
        }

        for (auto cur : draw_streams) {
            if (cur.enabled) {
                cur.execute();
            }
        }

        main_window().swap();
    }
}
