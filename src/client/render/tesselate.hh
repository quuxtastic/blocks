#ifndef RENDER_TESSELATE_HH_
#define RENDER_TESSELATE_HH_ 1

#include <vector>
#include <functional>

#include "gl.hh"

#include "terrain/chunk.hh"

struct cube_tesselator_t {
    static size_t front(GLubyte x, GLubyte y, GLubyte z, GLubyte cube_size,
        GLubyte texIndex,
        std::vector<GLubyte>& out);
    static size_t back(GLubyte x, GLubyte y, GLubyte z, GLubyte cube_size,
        GLubyte texIndex,
        std::vector<GLubyte>& out);
    static size_t left(GLubyte x, GLubyte y, GLubyte z, GLubyte cube_size,
        GLubyte texIndex,
        std::vector<GLubyte>& out);
    static size_t right(GLubyte x, GLubyte y, GLubyte z, GLubyte cube_size,
        GLubyte texIndex,
        std::vector<GLubyte>& out);
    static size_t top(GLubyte x, GLubyte y, GLubyte z, GLubyte cube_size,
        GLubyte texIndex,
        std::vector<GLubyte>& out);
    static size_t bottom(GLubyte x, GLubyte y, GLubyte z, GLubyte cube_size,
        GLubyte texIndex,
        std::vector<GLubyte>& out);
};

enum face_t {
    FRONT_FACE = 0,
    BACK_FACE,
    LEFT_FACE,
    RIGHT_FACE,
    TOP_FACE,
    BOTTOM_FACE
};

class ChunkMesh;

size_t tesselate_chunk(const ChunkMesh& chunk,
        std::function<GLubyte (Block, size_t, size_t, size_t, size_t)>
            map_texture_fun,
        std::vector<GLubyte>& out);

size_t tesselate_chunk_greedy(const ChunkMesh& chunk,
    std::function<GLubyte (Block, size_t, size_t, size_t, size_t)>
        map_texture_fun,
    std::vector<GLubyte>& out);

#endif
