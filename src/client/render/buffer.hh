#ifndef RENDER_BUFFER_HH_
#define RENDER_BUFFER_HH_ 1

//#include <iterator>

#include "gl.hh"

class Buffer : public GlObjectHandle<Buffer, GL_BUFFER> {
public:
    typedef GlObjectHandle<Buffer, GL_BUFFER> handle_type;

    Buffer() : target(INVALID_ID) {}
    virtual ~Buffer() {}

    static Buffer create(GLuint target, GLsizeiptr size, GLenum usage);
    static Buffer create(GLuint target, const void* data, GLsizeiptr size,
        GLenum usage);
    /*typedef std::const_iterator<
        std::input_iterator_tag,
        std::pair<const void*, GLsizeiptr>>
        buffer_set_iter_t;
    static Buffer create(GLuint target, GLenum usage,
        buffer_set_iter_t begin, buffer_set_iter_t end);*/

    void reset(const void* data, GLsizeiptr size, GLenum usage);
    void write(GLsizei offset, GLsizei size, const void* data);

    template<typename Callable>
    void map(GLintptr offset, GLsizeiptr length, GLbitfield mode, Callable f) {
        bind();
        void* p = glMapBufferRange(target, offset, length, mode);
        f(p);
        glUnmapBuffer(target);
    }

    void copy(Buffer src, GLintptr read_off, GLintptr write_off,
        GLsizeiptr size);

    void dirty(GLintptr offset, GLsizeiptr size);

    void bind() {
        glBindBuffer(target, obj_id);
    }
    void bind(GLuint new_target) {
        glBindBuffer(new_target, obj_id);
    }

    void unbind() {
        glBindBuffer(target, 0);
    }

protected:
    Buffer(GLuint id, GLuint in_target) : handle_type(id), target(in_target) {}

    friend handle_type;
    void destroy();
private:
    GLuint target;
};

#endif
