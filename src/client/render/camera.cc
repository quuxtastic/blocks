#include "camera.hh"

#include <cmath>
#include <glm/gtc/matrix_transform.hpp>

#include "util/math.hh"
#include "util/log.hh"

const glm::vec3 Camera::world_up = glm::vec3(0.0f, 1.0f, 0.0f);

void Camera::set_vectors() {
    front.x =
        std::cos(glm::radians(yaw_angle)) * std::cos(glm::radians(pitch_angle));
    front.y = std::sin(glm::radians(pitch_angle));
    front.z =
        std::sin(glm::radians(yaw_angle)) * std::cos(glm::radians(pitch_angle));
    front = glm::normalize(front);

    right = glm::normalize(glm::cross(front, world_up));
    up = glm::normalize(glm::cross(right, front));
}

void Camera::set_matrices() {
    view = glm::lookAt(position, position + front, up);
    projection = glm::perspective(glm::radians(fov), viewport_w / viewport_h,
        0.1f, 1000.0f);
}

void Camera::set_frustum() {
    using glm::vec3;

    frustum = frustum_from_mvp_matrix(view * projection);
    normalized_frustum = {
        normalize(frustum.left),
        normalize(frustum.right),
        normalize(frustum.top),
        normalize(frustum.bottom),
        normalize(frustum.far),
        normalize(frustum.near)
    };

    vec3 near_center = position - front * 0.1f;
    vec3 far_center = position - front * 1000.0f;

    float near_height = 2 * std::tan(glm::radians(fov) / 2) * 0.1f;
    float far_height = 2 * std::tan(glm::radians(fov) / 2) * 1000.0f;
    float near_width = near_height * (viewport_w / viewport_h);
    float far_width = far_height * (viewport_w / viewport_h);

    frustum_corners.far_top_left =
        far_center + up * (far_height * 0.5f) -
        right * (far_width * 0.5f);
    frustum_corners.far_top_right =
        far_center + up * (far_height * 0.5f) +
        right * (far_width * 0.5f);
    frustum_corners.far_bottom_left =
        far_center - up * (far_height * 0.5f) -
        right * (far_width * 0.5f);
    frustum_corners.far_bottom_right =
        far_center - up * (far_height * 0.5f) +
        right * (far_width * 0.5f);

    frustum_corners.near_top_left =
        near_center + up * (near_height * 0.5f) -
        right * (near_width * 0.5f);
    frustum_corners.near_top_right =
        near_center + up * (near_height * 0.5f) +
        right * (near_width * 0.5f);
    frustum_corners.near_bottom_left =
        near_center - up * (near_height * 0.5f) -
        right * (near_width * 0.5f);
    frustum_corners.near_bottom_right =
        near_center - up * (near_height * 0.5f) +
        right * (near_width * 0.5f);
}

Camera::Camera() {
    set_vectors();
    set_matrices();
    set_frustum();
}

void Camera::set_viewport(int w, int h) {
    viewport_w = w; viewport_h = h;
    set_matrices();
    set_frustum();
}

void Camera::set_pos(glm::vec3 in_pos) {
    position = in_pos;
    set_vectors();
    set_matrices();
    set_frustum();
}
void Camera::set_front(glm::vec3 in_dir) {
    front = in_dir;
    set_vectors();
    set_matrices();
    set_frustum();
}
void Camera::set_up(glm::vec3 in_up) {
    up = in_up;
    set_vectors();
    set_matrices();
    set_frustum();
}

void Camera::set_fov(float in_fov)  {
    fov = in_fov;
    set_matrices();
    set_frustum();
}

void Camera::set_move_speed(float in_spd)   { move_factor = in_spd; }
void Camera::set_pan_speed(float in_spd)    { pan_factor = in_spd; }
void Camera::set_zoom_speed(float in_spd)   { zoom_factor = in_spd; }

void Camera::move_z(float delta) {
    position += front * move_factor * delta;
    set_vectors();
    set_matrices();
    set_frustum();
}
void Camera::move_x(float delta) {
    position += right * move_factor * delta;
    set_vectors();
    set_matrices();
    set_frustum();
}
void Camera::move_y(float delta) {
    position += up * move_factor * delta;
    set_matrices();
    set_frustum();
    set_vectors();
}
void Camera::yaw(float angle) {
    yaw_angle += pan_factor * angle;
    set_vectors();
    set_matrices();
    set_frustum();
}
void Camera::pitch(float angle) {
    pitch_angle = clamp(pitch_angle + (pan_factor * angle), -89.0f, 89.0f);
    set_vectors();
    set_matrices();
    set_frustum();
}
void Camera::zoom(float delta) {
    fov -= (zoom_factor * delta);
    fov = clamp(fov, zoom_min, zoom_max);
    set_matrices();
    set_frustum();
}
