#include "mesh.hh"

#include <cassert>

#include "util/log.hh"

Mesh Mesh::create(Mesh::specification_cb_t spec_fun) {
    GLuint obj_id;
    glGenVertexArrays(1, &obj_id);

    Mesh mesh(obj_id);

    glBindVertexArray(obj_id);
    spec_fun(mesh);
    glBindVertexArray(0);

    return mesh;
}

void Mesh::bind() {
    glBindVertexArray(obj_id);
}

void Mesh::destroy() {
    glDeleteVertexArrays(1, &obj_id);
}

void Mesh::attach(Buffer buf) {
    assert(valid());
    buf.bind();
}

void Mesh::set_attr(int layout, int components, GLuint data_type,
        bool normalize, int stride, ptrdiff_t offset) {

    glVertexAttribPointer(layout, components, data_type, normalize,
        stride, reinterpret_cast<GLvoid*>(offset));
    glEnableVertexAttribArray(layout);
}
