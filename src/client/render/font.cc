#include "font.hh"

#include <utility>
#include <ft2build.h>
#include FT_FREETYPE_H

#include "util/error.hh"

static FT_Library ft_library() {
    static bool initialized = false;
    static FT_Library lib;

    if (!initialized) {
        if (FT_Init_FreeType(&lib)) {
            THROW("render-text", "Failed to initialize freetype library");
        }
        initialized = true;
    }

    return lib;
}

std::vector<std::unordered_map<char, Font::glyph_t>> Font::font_glyphs;

Font Font::create(const std::string& filename, size_t pt_size) {
    FT_Face face;
    if (FT_New_Face(ft_library(), filename.c_str(), 0, &face)) {
        THROW("render", "Failed to load font %s", filename);
    }
    FT_Set_Pixel_Sizes(face, 0, pt_size);

    size_t cur_font_index = font_glyphs.size();
    font_glyphs.push_back(std::unordered_map<char, glyph_t>());
    std::unordered_map<char, glyph_t>& metrics = font_glyphs[cur_font_index];

    const size_t MAX_ATLAS_WIDTH = 1024;

    // calculate total texture size and save glyph metrics
    size_t tex_width = 0, tex_height = 0;
    size_t tex_cur_x = 0, tex_cur_y = 0;
    for (char cur = MIN_PRINTABLE_CHAR; cur <= MAX_PRINTABLE_CHAR; ++cur) {
        if (FT_Load_Char(face, cur, FT_LOAD_RENDER)) {
            THROW("render", "Can't load glyph '%s' for font %s",
                cur, filename);
        }
        FT_GlyphSlot glyph = face->glyph;

        if (tex_cur_x + glyph->bitmap.width + 1 > MAX_ATLAS_WIDTH) {
            tex_cur_x = 0; tex_cur_y += tex_height;
        }

        metrics.insert(std::pair<char, glyph_t>(cur, {
            static_cast<float>(glyph->bitmap_left),
            static_cast<float>(glyph->bitmap_top),

            static_cast<float>(glyph->bitmap.width),
            static_cast<float>(glyph->bitmap.rows),

            static_cast<float>(glyph->advance.x >> 6),
            static_cast<float>(glyph->advance.y >> 6),

            static_cast<float>(tex_cur_x),
            static_cast<float>(tex_cur_y),
            static_cast<float>(glyph->bitmap.width),
            static_cast<float>(glyph->bitmap.rows)
        }));

        tex_width = std::max(tex_cur_x + glyph->bitmap.width, tex_width);
        tex_height = std::max(tex_cur_y + glyph->bitmap.rows, tex_height);

        tex_cur_x += glyph->bitmap.width;
    }

    // create texture
    Texture texture = Texture::d2(GL_TEXTURE_2D, GL_RED,
        nullptr, tex_width, tex_height,
        GL_RED, GL_UNSIGNED_BYTE,
        [] (GLuint, GLuint) {
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        });

    // copy glyphs into texture and scale texels to correct values
    texture.bind();
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    for (char cur = MIN_PRINTABLE_CHAR; cur <= MAX_PRINTABLE_CHAR; ++cur) {
        if (FT_Load_Char(face, cur, FT_LOAD_RENDER)) {
            THROW("render", "Can't load glyph '%s' for font %s",
                cur, filename);
        }
        FT_GlyphSlot glyph = face->glyph;

        glTexSubImage2D(GL_TEXTURE_2D, 0,
            metrics[cur].tx, metrics[cur].ty,
            glyph->bitmap.width, glyph->bitmap.rows,
            GL_RED, GL_UNSIGNED_BYTE,
            glyph->bitmap.buffer);

        metrics[cur].tx /= tex_width;
        metrics[cur].ty /= tex_height;
        metrics[cur].tw /= tex_width;
        metrics[cur].th /= tex_height;
    }

    FT_Done_Face(face);

    return Font(cur_font_index, texture);
}

void Font::bind() {
    atlas.bind();
}

const Font::glyph_t& Font::get_glyph(char c) {
    auto cur = font_glyphs[metrics_index].find(c);
    if (cur == font_glyphs[metrics_index].end()) {
        return get_glyph('?');
    }

    return font_glyphs[metrics_index][c];
}
