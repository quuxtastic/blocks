#ifndef RENDER_CHUNK_HH_
#define RENDER_CHUNK_HH_ 1

#include "terrain/chunk.hh"
#include "terrain/terrain.hh"
#include "util/math.hh"

#include "shader.hh"
#include "mesh.hh"
#include "buffer.hh"

class ChunkMesh : public Chunk {
public:
    ChunkMesh(int p, int q, int r);

    void render(Shader shader);

    bool dirty() const { return get_bits(flags, DIRTY_BIT); }
    void dirty(bool in) { set_bits(flags, DIRTY_BIT, in); }

    bool empty() const { return get_bits(flags, EMPTY_BIT); }
    void empty(bool in) { return set_bits(flags, EMPTY_BIT, in); }

    typedef std::function<size_t (ChunkMesh&,
        std::function<GLubyte (Block, size_t, size_t, size_t, size_t)>,
        std::vector<GLubyte>&)> tesselate_fun_t;
    void tesselate(tesselate_fun_t fun);

protected:

    virtual void post_load();
    virtual void on_set(size_t x, size_t y, size_t z, Block value);

    enum {
        DIRTY_BIT   = 0b00100000,
        EMPTY_BIT   = 0b00010000
    };

private:

    ChunkMesh* cast(Chunk* ptr) { return reinterpret_cast<ChunkMesh*>(ptr); }

    void dirty_neighbors();

    Mesh chunk_mesh;
    Buffer chunk_buf;
    size_t vertices = 0;

    glm::mat4 model;
};

#endif
