#ifndef RENDER_TEXTURE_HH_
#define RENDER_TEXTURE_HH_ 1

#include <functional>
#include <string>
#include <vector>

#include "gl.hh"

class Texture : public GlObjectHandle<Texture, GL_TEXTURE> {
public:
    typedef GlObjectHandle<Texture, GL_TEXTURE> handle_type;

    typedef std::function<void (GLuint, GLuint)> texture_setup_cb_t;
    static const texture_setup_cb_t TextureSetupNoop;

    Texture() : type(INVALID_ID) {}
    virtual ~Texture() {}

    static Texture d2(GLuint type, GLuint format,
        const void* image_data, int w, int h,
        GLuint src_fmt, GLuint src_data_type,
        texture_setup_cb_t setup = TextureSetupNoop,
        int gen_mipmaps = 0);
    static Texture d2(GLuint type, GLuint format,
        const std::string& filename, int channels,
        GLuint src_fmt, GLuint src_data_type,
        texture_setup_cb_t setup = TextureSetupNoop,
        int gen_mipmaps = 0);

    static Texture array(GLuint type, GLuint format,
        const void* image_data, int w, int h, int d,
        GLuint src_fmt, GLuint src_data_type,
        texture_setup_cb_t setup = TextureSetupNoop,
        int gen_mipmaps = 0);
    static Texture array(GLuint type, GLuint format,
        const std::vector<std::string>& filenames, int channels,
        GLuint src_fmt, GLuint src_data_type,
        texture_setup_cb_t setup = TextureSetupNoop,
        int gen_mipmaps = 0);
    static Texture array(GLuint type, GLuint format,
        const std::string& atlas_filename, int channels,
        size_t w, size_t h, size_t d, size_t cols,
        GLuint src_fmt, GLuint src_data_type,
        texture_setup_cb_t setup = TextureSetupNoop,
        int gen_mipmaps = 0);

    void bind(int unit = 0);

protected:
    explicit Texture(GLuint id, GLuint in_type) :
        handle_type(id),
        type(in_type) {}

    friend handle_type;
    void destroy();

private:
    GLuint type;
};

#endif
