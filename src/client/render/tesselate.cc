#include "tesselate.hh"

#include "chunk.hh"

/*
 * ====== Vertex Format ======
 *
 * We know that each chunk is 32 cubes on a side (128 units), each cube is 4
 * units on a side and is always aligned with the x,y,z axes. Using this we can
 * compact the vertex data we store considerably:
 *
 * 1 byte each for x, y, and z coordinates of each vertex
 * 1 byte for normal coords:
 *     top bit is sign bit, last 3 bits are x,y, and z.
 *     (thus we actually only 4 bits)
 * 1 byte each for texel u and v
 * 1 byte for texture index
 * 1 byte of padding (to align to 4-byte boundaries)
 *
 * This gives 8 bytes per vertex, or 32 bytes per cube face.
 */

namespace {

const GLubyte NORMAL_FRONT  = 0b00001001;
const GLubyte NORMAL_BACK   = 0b00000001;
const GLubyte NORMAL_LEFT   = 0b00001100;
const GLubyte NORMAL_RIGHT  = 0b00000100;
const GLubyte NORMAL_TOP    = 0b00000010;
const GLubyte NORMAL_BOTTOM = 0b00001010;

}

/*
 * ====== Cube Face Tesselation ======
 *
 *              2 +--+ 4
 *                |\ |
 *                | \|
 *              1 +--+ 3
 */

size_t cube_tesselator_t::front(GLubyte x, GLubyte y, GLubyte z,
        GLubyte cube_size, GLubyte texIndex, std::vector<GLubyte>& out) {

    out.reserve(out.size() + 32);

    // 1 - bottom-left-front
    out.push_back(x*cube_size);
    out.push_back(y*cube_size);
    out.push_back(z*cube_size);

    out.push_back(NORMAL_FRONT);

    out.push_back(0); out.push_back(255);
    out.push_back(texIndex);

    out.push_back(0);

    // 2- top-left-front
    out.push_back(x*cube_size);
    out.push_back(y*cube_size + cube_size);
    out.push_back(z*cube_size);

    out.push_back(NORMAL_FRONT);

    out.push_back(0); out.push_back(0);
    out.push_back(texIndex);

    out.push_back(0);

    // 3 - bottom-right-front
    out.push_back(x*cube_size + cube_size);
    out.push_back(y*cube_size);
    out.push_back(z*cube_size);

    out.push_back(NORMAL_FRONT);

    out.push_back(255); out.push_back(255);
    out.push_back(texIndex);

    out.push_back(0);

    // 2- top-left-front
    out.push_back(x*cube_size);
    out.push_back(y*cube_size + cube_size);
    out.push_back(z*cube_size);

    out.push_back(NORMAL_FRONT);

    out.push_back(0); out.push_back(0);
    out.push_back(texIndex);

    out.push_back(0);

    // 4 - top-right-front
    out.push_back(x*cube_size + cube_size);
    out.push_back(y*cube_size + cube_size);
    out.push_back(z*cube_size);

    out.push_back(NORMAL_FRONT);

    out.push_back(255); out.push_back(0);
    out.push_back(texIndex);

    out.push_back(0);

    // 3 - bottom-right-front
    out.push_back(x*cube_size + cube_size);
    out.push_back(y*cube_size);
    out.push_back(z*cube_size);

    out.push_back(NORMAL_FRONT);

    out.push_back(255); out.push_back(255);
    out.push_back(texIndex);

    out.push_back(0);

    return 6;
}

size_t cube_tesselator_t::back(GLubyte x, GLubyte y, GLubyte z,
        GLubyte cube_size, GLubyte texIndex, std::vector<GLubyte>& out) {

    out.reserve(out.size() + 32);

    // 1 - bottom-right-back
    out.push_back(x*cube_size + cube_size);
    out.push_back(y*cube_size);
    out.push_back(z*cube_size + cube_size);

    out.push_back(NORMAL_BACK);

    out.push_back(0); out.push_back(255);
    out.push_back(texIndex);

    out.push_back(0);

    // 2 - top-right-back
    out.push_back(x*cube_size + cube_size);
    out.push_back(y*cube_size + cube_size);
    out.push_back(z*cube_size + cube_size);

    out.push_back(NORMAL_BACK);

    out.push_back(0); out.push_back(0);
    out.push_back(texIndex);

    out.push_back(0);

    // 3 - bottom-left-back
    out.push_back(x*cube_size);
    out.push_back(y*cube_size);
    out.push_back(z*cube_size + cube_size);

    out.push_back(NORMAL_BACK);

    out.push_back(255); out.push_back(255);
    out.push_back(texIndex);

    out.push_back(0);

    // 2 - top-right-back
    out.push_back(x*cube_size + cube_size);
    out.push_back(y*cube_size + cube_size);
    out.push_back(z*cube_size + cube_size);

    out.push_back(NORMAL_BACK);

    out.push_back(0); out.push_back(0);
    out.push_back(texIndex);

    out.push_back(0);

    // 4 - top-left-back
    out.push_back(x*cube_size);
    out.push_back(y*cube_size + cube_size);
    out.push_back(z*cube_size + cube_size);

    out.push_back(NORMAL_BACK);

    out.push_back(255); out.push_back(0);
    out.push_back(texIndex);

    out.push_back(0);

    // 3 - bottom-left-back
    out.push_back(x*cube_size);
    out.push_back(y*cube_size);
    out.push_back(z*cube_size + cube_size);

    out.push_back(NORMAL_BACK);

    out.push_back(255); out.push_back(255);
    out.push_back(texIndex);

    out.push_back(0);

    return 6;
}

size_t cube_tesselator_t::left(GLubyte x, GLubyte y, GLubyte z,
        GLubyte cube_size, GLubyte texIndex, std::vector<GLubyte>& out) {

    out.reserve(out.size() + 32);

    // 1 - bottom-left-back
    out.push_back(x*cube_size);
    out.push_back(y*cube_size);
    out.push_back(z*cube_size + cube_size);

    out.push_back(NORMAL_LEFT);

    out.push_back(0); out.push_back(255);
    out.push_back(texIndex);

    out.push_back(0);

    // 2 - top-left-back
    out.push_back(x*cube_size);
    out.push_back(y*cube_size + cube_size);
    out.push_back(z*cube_size + cube_size);

    out.push_back(NORMAL_LEFT);

    out.push_back(0); out.push_back(0);
    out.push_back(texIndex);

    out.push_back(0);

    // 3 - bottom-left-front
    out.push_back(x*cube_size);
    out.push_back(y*cube_size);
    out.push_back(z*cube_size);

    out.push_back(NORMAL_LEFT);

    out.push_back(255); out.push_back(255);
    out.push_back(texIndex);

    out.push_back(0);

    // 2 - top-left-back
    out.push_back(x*cube_size);
    out.push_back(y*cube_size + cube_size);
    out.push_back(z*cube_size + cube_size);

    out.push_back(NORMAL_LEFT);

    out.push_back(0); out.push_back(0);
    out.push_back(texIndex);

    out.push_back(0);

    // 4 - top-left-front
    out.push_back(x*cube_size);
    out.push_back(y*cube_size + cube_size);
    out.push_back(z*cube_size);

    out.push_back(NORMAL_LEFT);

    out.push_back(255); out.push_back(0);
    out.push_back(texIndex);

    out.push_back(0);

    // 3 - bottom-left-front
    out.push_back(x*cube_size);
    out.push_back(y*cube_size);
    out.push_back(z*cube_size);

    out.push_back(NORMAL_LEFT);

    out.push_back(255); out.push_back(255);
    out.push_back(texIndex);

    out.push_back(0);

    return 6;
}

size_t cube_tesselator_t::right(GLubyte x, GLubyte y, GLubyte z,
        GLubyte cube_size, GLubyte texIndex, std::vector<GLubyte>& out) {

    out.reserve(out.size() + 32);

    // 1 - bottom-right-front
    out.push_back(x*cube_size + cube_size);
    out.push_back(y*cube_size);
    out.push_back(z*cube_size);

    out.push_back(NORMAL_RIGHT);

    out.push_back(0); out.push_back(255);
    out.push_back(texIndex);

    out.push_back(0);

    // 2 - top-right-front
    out.push_back(x*cube_size + cube_size);
    out.push_back(y*cube_size + cube_size);
    out.push_back(z*cube_size);

    out.push_back(NORMAL_RIGHT);

    out.push_back(0); out.push_back(0);
    out.push_back(texIndex);

    out.push_back(0);

    // 3 - bottom-right-back
    out.push_back(x*cube_size + cube_size);
    out.push_back(y*cube_size);
    out.push_back(z*cube_size + cube_size);

    out.push_back(NORMAL_RIGHT);

    out.push_back(255); out.push_back(255);
    out.push_back(texIndex);

    out.push_back(0);

    // 2 - top-right-front
    out.push_back(x*cube_size + cube_size);
    out.push_back(y*cube_size + cube_size);
    out.push_back(z*cube_size);

    out.push_back(NORMAL_RIGHT);

    out.push_back(0); out.push_back(0);
    out.push_back(texIndex);

    out.push_back(0);

    // 4 - top-right-back
    out.push_back(x*cube_size + cube_size);
    out.push_back(y*cube_size + cube_size);
    out.push_back(z*cube_size + cube_size);

    out.push_back(NORMAL_RIGHT);

    out.push_back(255); out.push_back(0);
    out.push_back(texIndex);

    out.push_back(0);

    // 3 - bottom-right-back
    out.push_back(x*cube_size + cube_size);
    out.push_back(y*cube_size);
    out.push_back(z*cube_size + cube_size);

    out.push_back(NORMAL_RIGHT);

    out.push_back(255); out.push_back(255);
    out.push_back(texIndex);

    out.push_back(0);

    return 6;
}

size_t cube_tesselator_t::bottom(GLubyte x, GLubyte y, GLubyte z,
        GLubyte cube_size, GLubyte texIndex, std::vector<GLubyte>& out) {

    out.reserve(out.size() + 32);

    // 1 - bottom-left-back
    out.push_back(x*cube_size);
    out.push_back(y*cube_size);
    out.push_back(z*cube_size + cube_size);

    out.push_back(NORMAL_BOTTOM);

    out.push_back(0); out.push_back(255);
    out.push_back(texIndex);

    out.push_back(0);

    // 2 - bottom-left-front
    out.push_back(x*cube_size);
    out.push_back(y*cube_size);
    out.push_back(z*cube_size);

    out.push_back(NORMAL_BOTTOM);

    out.push_back(0); out.push_back(0);
    out.push_back(texIndex);

    out.push_back(0);

    // 3 - bottom-right-back
    out.push_back(x*cube_size + cube_size);
    out.push_back(y*cube_size);
    out.push_back(z*cube_size + cube_size);

    out.push_back(NORMAL_BOTTOM);

    out.push_back(255); out.push_back(255);
    out.push_back(texIndex);

    out.push_back(0);

    // 2 - bottom-left-front
    out.push_back(x*cube_size);
    out.push_back(y*cube_size);
    out.push_back(z*cube_size);

    out.push_back(NORMAL_BOTTOM);

    out.push_back(0); out.push_back(0);
    out.push_back(texIndex);

    out.push_back(0);

    // 4 - bottom-right-front
    out.push_back(x*cube_size + cube_size);
    out.push_back(y*cube_size);
    out.push_back(z*cube_size);

    out.push_back(NORMAL_BOTTOM);

    out.push_back(255); out.push_back(0);
    out.push_back(texIndex);

    out.push_back(0);

    // 3 - bottom-right-back
    out.push_back(x*cube_size + cube_size);
    out.push_back(y*cube_size);
    out.push_back(z*cube_size + cube_size);

    out.push_back(NORMAL_BOTTOM);

    out.push_back(255); out.push_back(255);
    out.push_back(texIndex);

    out.push_back(0);

    return 6;
}

size_t cube_tesselator_t::top(GLubyte x, GLubyte y, GLubyte z,
        GLubyte cube_size, GLubyte texIndex, std::vector<GLubyte>& out) {

    out.reserve(out.size() + 32);

    // 1 - top-left-front
    out.push_back(x*cube_size);
    out.push_back(y*cube_size + cube_size);
    out.push_back(z*cube_size);

    out.push_back(NORMAL_TOP);

    out.push_back(0); out.push_back(255);
    out.push_back(texIndex);

    out.push_back(0);

    // 2 - top-left-back
    out.push_back(x*cube_size);
    out.push_back(y*cube_size + cube_size);
    out.push_back(z*cube_size + cube_size);

    out.push_back(NORMAL_TOP);

    out.push_back(0); out.push_back(0);
    out.push_back(texIndex);

    out.push_back(0);

    // 3 - top-right-front
    out.push_back(x*cube_size + cube_size);
    out.push_back(y*cube_size + cube_size);
    out.push_back(z*cube_size);

    out.push_back(NORMAL_TOP);

    out.push_back(255); out.push_back(255);
    out.push_back(texIndex);

    out.push_back(0);

    // 2 - top-left-back
    out.push_back(x*cube_size);
    out.push_back(y*cube_size + cube_size);
    out.push_back(z*cube_size + cube_size);

    out.push_back(NORMAL_TOP);

    out.push_back(0); out.push_back(0);
    out.push_back(texIndex);

    out.push_back(0);

    // 4 - top-right-back
    out.push_back(x*cube_size + cube_size);
    out.push_back(y*cube_size + cube_size);
    out.push_back(z*cube_size + cube_size);

    out.push_back(NORMAL_TOP);

    out.push_back(255); out.push_back(0);
    out.push_back(texIndex);

    out.push_back(0);

    // 3 - top-right-front
    out.push_back(x*cube_size + cube_size);
    out.push_back(y*cube_size + cube_size);
    out.push_back(z*cube_size);

    out.push_back(NORMAL_TOP);

    out.push_back(255); out.push_back(255);
    out.push_back(texIndex);

    out.push_back(0);

    return 6;
}

size_t tesselate_chunk(const ChunkMesh& chunk,
        std::function<GLubyte (Block, size_t, size_t, size_t, size_t)>
            map_texture_fun,
        std::vector<GLubyte>& out) {

    size_t element_count = 0;
    chunk.traverse([&] (Block cur, size_t x, size_t y, size_t z) {
        if (cur.occludes()) {
            if (!chunk.front(x, y, z).occludes()) {
                element_count += cube_tesselator_t::front(x, y, z,
                    ChunkMesh::CUBE_SIZE,
                    map_texture_fun(cur, FRONT_FACE, x, y, z), out);
            }
            if (!chunk.back(x, y, z).occludes()) {
                element_count += cube_tesselator_t::back(x, y, z,
                    ChunkMesh::CUBE_SIZE,
                    map_texture_fun(cur, BACK_FACE, x, y, z), out);
            }
            if (!chunk.left(x, y, z).occludes()) {
                element_count += cube_tesselator_t::left(x, y, z,
                    ChunkMesh::CUBE_SIZE,
                    map_texture_fun(cur, LEFT_FACE, x, y, z), out);
            }
            if (!chunk.right(x, y, z).occludes()) {
                element_count += cube_tesselator_t::right(x, y, z,
                    ChunkMesh::CUBE_SIZE,
                    map_texture_fun(cur, RIGHT_FACE, x, y, z), out);
            }
            if (!chunk.top(x, y, z).occludes()) {
                element_count += cube_tesselator_t::top(x, y, z,
                    ChunkMesh::CUBE_SIZE,
                    map_texture_fun(cur, TOP_FACE, x, y, z), out);
            }
            if (!chunk.bottom(x, y, z).occludes()) {
                element_count += cube_tesselator_t::bottom(x, y, z,
                    ChunkMesh::CUBE_SIZE,
                    map_texture_fun(cur, BOTTOM_FACE, x, y, z), out);
            }
        } else if (cur.liquid() && !chunk.top(x, y, z).liquid()) {
            element_count += cube_tesselator_t::top(x, y, z,
                ChunkMesh::CUBE_SIZE,
                map_texture_fun(cur, TOP_FACE, x, y, z), out);
        }
    });

    return element_count;
}

/*namespace {

int calc_quad_width(
        std::array<Block, Chunk::CHUNK_SIZE * Chunk::CHUNK_SIZE>& mask,
        int count) {
    int w = 1;
    for (; count + w < Chunk::CHUNK_SIZE && mask[count + w].occludes() &&
        mask[count + w] == mask[count]; ++w);
    return w;
}

int calc_quad_height(
        std::array<Block, Chunk::CHUNK_SIZE * Chunk::CHUNK_SIZE>& mask,
        int count, int w) {
    int h = 1;
    for (; count + h < Chunk::CHUNK_SIZE; ++h) {
        for (int i = 0; i < w; ++i) {
            if (!mask[count + i + h * Chunk::CHUNK_SIZE].occludes() ||
                    mask[count + i + h * Chunk::CHUNK_SIZE] != mask[count]) {
                return h;
            }
        }
    }
}

size_t tesselate_quad(
        GLubyte bottom_left[3], GLubyte top_left[3],
        GLubyte top_right[3], GLubyte bottom_right[3],
        Block b, GLubyte face,
        std::vector<GLubyte>& out) {
    out.reserve(out.size() + 32);
    out.push_back(bottom_left);
    out.push_back(face);
    out.push_back(bottom_left);
}

size_t make_quads_for_face(
        std::array<Block, Chunk::CHUNK_SIZE * Chunk::CHUNK_SIZE>& mask,
        int axis_x, int axis_y, int cur[3], GLubyte face, bool back,
        std::vector<GLubyte>& out) {
    int element_count = 0;

    int cur_mask = 0;
    for (int j = 0; j < Chunk::CHUNK_SIZE; ++j) {
        for (int i = 0; i < Chunk::CHUNK_SIZE;) {
            if (mask[cur_mask].occludes()) {
                int w = calc_quad_width(mask, cur_mask);
                int h = calc_quad_height(mask, cur_mask, w);

                cur[axis_x] = i;
                cur[axis_y] = j;

                int quad0[3] = {0, 0, 0};
                quad0[axis_x] = w * Chunk::CUBE_SIZE;
                int quad1[3] = {0, 0, 0};
                quad1[axis_y] = h * Chunk::CUBE_SIZE;

                element_count += tesselate_quad(quad0, quad1, mask[cur_mask],
                    face, out);

                i += w;
                cur_mask += w;
            } else {
                ++i;
                ++cur_mask;
            }
        }
    }

    return element_count;
}

size_t chunk_face_greedy(
        std::array<Block, Chunk::CHUNK_SIZE * Chunk::CHUNK_SIZE>& mask,
        int axis, int axis_x, int axis_y, int cur[3], int next[3],
        GLubyte face, bool back,
        std::vector<GLubyte>& out) {
    int element_count = 0;
    std::fill(mask.begin(), mask.end(), Block::AIR);
    int i = 0;
    for (cur[axis] = -1; cur[axis] < Chunk::CHUNK_SIZE) {
        for (cur[axis_y] = 0; cur[axis_y] < Chunk::CHUNK_SIZE) {
            for (cur[axis_z] = 0; cur[axis_x] < Chunk::CHUNK_SIZE) {
                Block cur_block = (cur[axis] >= 0) ?
                    chunk.get(cur[0], cur[1], cur[2]) :
                    Block::AIR;
                Block next_block = (next[axis] < CHUNK_SIZE -1) ?
                    chunk.get(cur[0] + next[0], cur[1] + next[1], cur[2] + next[2]) :
                    Block::AIR;

                mask[i++] = !cur_block.occludes() && !next_block.occludes()
                    && cur_block == next_block ?
                    Block::AIR :
                    back ? next_block : cur_block;
            }
        }

        cur[axis]++;
        i = 0;

        element_count +=
            make_quads_for_face(mask, axis_x, axis_y, cur, face, back, out);
    }

    return element_count;
}

}*/

size_t tesselate_chunk_greedy(const ChunkMesh& /*chunk*/,
        std::function<GLubyte (Block, size_t, size_t, size_t, size_t)>
            /*map_texture_fun*/,
        std::vector<GLubyte>& /*out*/) {
    size_t element_count = 0;

    /*std::array<Block, Chunk::CHUNK_SIZE * Chunk::CHUNK_SIZE> mask;

    int cur[3] = {0, 0, 0};
    int next[3] = {0, 0, 0};

    GLubyte face = 0;

    for (int i = 0, bool back = true; i < 6; ++i) {
        axis = i % 3;
        axis_x = (axis + 1) % 3;
        axis_y = (axis + 2) % 3;

        cur = {0, 0, 0};
        next = {0, 0, 0};
        next[axis % 3] = 1;

        if (axis == 0) face = back ? LEFT_FACE : RIGHT_FACE;
        else if (axis == 1) face = back ? BOTTOM_FACE : TOP_FACE;
        else face = back ? BACK_FACE : FRONT_FACE;

        chunk_face_greedy(mask, axis, axis_x, axis_y, cur, next, face, back,
            out);

        back = !back;
    }*/

    return element_count;
}
