#ifndef RENDER_MESH_HH_
#define RENDER_MESH_HH_ 1

#include <vector>
#include <functional>

#include "gl.hh"
#include "buffer.hh"

class Mesh : public GlObjectHandle<Mesh, GL_VERTEX_ARRAY> {
public:
    typedef GlObjectHandle<Mesh, GL_VERTEX_ARRAY> handle_type;

    Mesh() {}
    virtual ~Mesh() {}

    typedef std::function<void (Mesh)> specification_cb_t;
    static Mesh create(specification_cb_t cb);

    void bind();

protected:
    explicit Mesh(GLuint id) :
        handle_type(id) {}

    friend handle_type;
    void destroy();

public:

    void attach(Buffer b);

    void set_attr(int layout, int components, GLuint data_type, bool normalize,
            int stride, ptrdiff_t offset);
};

#endif
