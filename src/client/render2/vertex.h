#ifndef VERTEX_H_
#define VERTEX_H_ 1

/*
 * * * * Coordinate System * * *
 *
 * North: -z East: +x South: +z West: -x Up: +y Down: -y
 *
 * Each chunk is centered on 0,0,0
 *
 * * * * Vertex Format * * *
 *
 * We know that each chunk is 32 blocks on a side (128 units), each block is a
 * unit cube aligned with the x,y,z axes. Using this we can compact the vertex
 * data considerably:
 *
 * Vertex buffers (1 per chunk)
 * - 3 bytes for x,y,z coords
 * - 1 byte for normal index (there are 6 hardcoded normals)
 * - 2 bytes for texel u,v coords
 * - 1 byte for texture index
 * - 1 byte of padding
 *
 * This gives 8 bytes per vertex, or 32 bytes per cube face
 *
 * Only 8 vertices are needed for chunk meshes - these are hard-coded in the
 * vertex shader.
 *
 */

#endif
