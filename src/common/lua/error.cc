#include "error.hh"

namespace lua {

namespace detail {

void check_error(lua_State* state, int code) {
    if (code == LUA_OK) return;

    const char* error_str = lua_tostring(state, -1);
    lua_pop(state, 1);

    switch(code) {
    case LUA_ERRSYNTAX:
        throw SyntaxError(error_str);
    case LUA_ERRMEM:
    case LUA_GCMM:
        throw MemError(error_str);
    default:
        throw Error(error_str);
    }
}

}

}
