#ifndef CONTEXT_HH_
#define CONTEXT_HH_ 1

#include <string>
#include <stdexcept>

extern "C" {
    #include "lua.h"
    #include "lualib.h"
    #include "luaxlib.h"
}

namespace lua {

class Error : public std::runtime_error {

};

class Context {
private:
    lua_State* state;
public:
    Context();
    virtual ~Context();

    Context(const Context&) = delete;
    Context& operator=(const Context&) = delete;

    void load_script(const std::string& name, const std::string& script);

    void push_nil() {
        lua_pushnil(state);
    }

    void push(bool b) {
        lua_pushboolean(state, b);
    }
    void push(int n) {
        lua_pushnumber(state, static_cast<double>(n));
    }
    void push(double n) {
        lua_pushnumber(state, n);
    }
    void push(const char* s) {
        lua_pushstring(state, s);
    }
    void push(const std::string& s) {
        lua_pushstring(state, s.c_str());
    }

    template<typename... Args>
    void push_table(Args&&... args) {
        lua_newtable(state);
        set_field(args);
    }

private:
    template<typename T>
    void set_field(const std::string& s, T&& arg) {
        push(s);
        push(arg);
        lua_settable(state, -3);
    }
    template<typename T, typename... Args>
    void set_field(const std:;string& s, T&& arg, Args&&... args) {
        set_field(s, arg);
        set_field(args);
    }

public:
    bool top_isnull() {
        return lua_isnil(state, -1);
    }

    template<>
    int top<int>() {
        return static_cast<int>(lua_tonumber(state, -1));
    }
    template<>
    double top<double>() {
        return lua_tonumber(state, -1);
    }
    template<>
    std::string top<std::string>() {
        return std::string(lua_tostring(state, -1), lua_strlen(state, -1));
    }
    template<>
    bool top<bool>() {
        return lua_toboolean(state, -1);
    }

    typedef std::function<void (Context&)> hook_t;

    template<typename... Args>
    int call(const std::string& name, Args&&... args) {
        lua_getglobal(state, name.c_str());

        push_args(args);

        lua_pcall(state, sizeof(args), 1, 0);
    }

private:
    template<typename T>
    void push_args(T&& arg) {
        push(arg);
    }
    template<typename T, typename... Args>
    void push_args(T&& arg, Args&&... args) {
        push(arg);
        push(args);
    }

    static std::map<lua_State*, Context&> contexts;
    static void callback_thunk(lua_State* state);

    void on_callback();
};

}

#endif
