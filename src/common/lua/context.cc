#include "context.hh"

namespace lua {

const char* LUA_CALLBACK_THUNK = "ctx-thunk";

Context::Context() {
    state = lua_open();
    luaL_openlibs(state);

    contexts.insert(state, *this);

    lua_pushstring(state, LUA_CALLBACK_THUNK);
    lua_pushcfunction(state, Context::callback_thunk);
}

Context::~Context() {
    lua_close(state);
}

void Context::load_script(const std::string& name, const std::string& script) {
    luaL_loadbuffer(state, script.c_str(), script.length(), name.c_str());
}

void Context::callback_thunk(lua_State* state) {
    contexts.get(state).on_callback();
}

}
