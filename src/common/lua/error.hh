#ifndef ERROR_HH_
#define ERROR_HH_ 1

#include <stdexcept>

extern "C" {
    #include "lua.h"
}

namespace lua {

class Error : public std::runtime_error {
private:
    std::string error_str;
public:
    explicit Error(const std::string& s) noexcept : error_str(s) {}

    virtual const char* what() const noexcept {
        return error_str.c_str();
    }
};

class SyntaxError : public Error {
public:
    explicit SyntaxError(const std::string& s) noexcept :
        Error("Syntax error: " + s) {}
};

class MemError : public Error {
public:
    explicit MemError(const std::string& s) noexcept :
        Error("Memory allocation error: " + s) {}
};

namespace detail {

void check_error(lua_State* state, int code);

}

}

#endif
