#include "terrain.hh"

#include <cmath>

#include "util/math.hh"
#include "util/log.hh"

ChunkTerrain::~ChunkTerrain() {
    set_evict_timeout(std::chrono::seconds(0));
    garbage_collect();
}

Chunk* ChunkTerrain::get(int p, int q, int r) {
    if (!is_valid_coord(p, q, r)) return nullptr;

    Chunk* ret = find_chunk(p, q, r);
    if (!ret) ret = build_chunk(p, q, r);

    return ret;
}

void ChunkTerrain::garbage_collect() {
    auto now = std::chrono::steady_clock::now();
    for (auto cur = chunk_map.begin(); cur != chunk_map.end(); ) {
        // unload chunk only if:
        // - keep loaded bit not set
        // - terrain ready (if terrain is still generating, we don't want to
        //      delete memory that is being worked on by another thread)
        // - it has been at least evict_timeout seconds since we last touched
        //      this chunk
        if (!cur->second->keep_loaded() && cur->second->terrain_ready() &&
                (now - cur->second->last_touched() > evict_timeout)) {
            destruct_chunk(cur->first.p, cur->first.q, cur->first.r,
                cur->second);
            cur = chunk_map.erase(cur);
        } else {
            ++cur;
        }
    }
}

void ChunkTerrain::set_neighbors(int p, int q, int r, Chunk* chunk,
        Chunk* newval) {
    Chunk* other = nullptr;

    other = find_chunk(p, q, r-1);
    chunk->front(other);
    if (other) {
        other->back(newval);
    }

    other = find_chunk(p, q, r+1);
    chunk->back(other);
    if (other) {
        other->front(newval);
    }

    other = find_chunk(p-1, q, r);
    chunk->left(other);
    if (other) {
        other->right(newval);
    }

    other = find_chunk(p+1, q, r);
    chunk->right(other);
    if (other) {
        other->left(newval);
    }

    other = find_chunk(p, q+1, r);
    chunk->top(other);
    if (other) {
        other->bottom(newval);
    }

    other = find_chunk(p, q-1, r);
    chunk->bottom(other);
    if (other) {
        other->top(newval);
    }
}

Chunk* ChunkTerrain::build_chunk(int p, int q, int r) {
    Chunk* chunk = allocate_chunk_fun(p, q, r);

    chunk_map[Chunk::coord_t(p, q, r)] = chunk;
    set_neighbors(p, q, r, chunk, chunk);

    int id = terrain_gen_pool.add_work([this, p, q, r, chunk] (int) {
        gen_chunk_fun(p, q, r, *chunk);
    });
    worker_chunks.insert({id, chunk});

    return chunk;
}

void ChunkTerrain::destruct_chunk(int p, int q, int r, Chunk* chunk) {
    chunk->pre_unload();

    set_neighbors(p, q, r, chunk, nullptr);

    free_chunk_fun(chunk);
}

Chunk* ChunkTerrain::find_chunk(int p, int q, int r) {
    auto cur = chunk_map.find(Chunk::coord_t(p, q, r));
    if (cur == chunk_map.end()) return nullptr;

    return cur->second;
}

size_t ChunkTerrain::coord_hasher::operator()(
        const Chunk::coord_t& coord) const {
    return hash_xyz(coord.p, coord.q, coord.r);
}

bool ChunkTerrain::is_valid_coord(int, int q, int) {
    return (q >= min_q) && (q <= max_q);
}

void ChunkTerrain::do_frame() {
    terrain_gen_pool.poll([this] (int id) {
        Chunk* chunk = worker_chunks[id];

        chunk->terrain_ready(true);

        chunk->post_load();
    });

    garbage_collect();
}
