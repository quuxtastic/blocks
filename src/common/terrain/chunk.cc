#include "chunk.hh"

Block Chunk::front(size_t x, size_t y, size_t z) const {
    if (z > 0) {
        return get(x, y, z-1);
    } else if (front_p) {
        return front_p->get(x, y, CHUNK_SIZE-1);
    }

    return Block::AIR;
}

Block Chunk::back(size_t x, size_t y, size_t z) const {
    if (z < CHUNK_SIZE-1) {
        return get(x, y, z+1);
    } else if (back_p) {
        return back_p->get(z, y, 0);
    }

    return Block::AIR;
}

Block Chunk::left(size_t x, size_t y, size_t z) const {
    if (x > 0) {
        return get(x-1, y, z);
    } else if (left_p) {
        return left_p->get(CHUNK_SIZE-1, y, z);
    }

    return Block::AIR;
}

Block Chunk::right(size_t x, size_t y, size_t z) const {
    if (x < CHUNK_SIZE-1) {
        return get(x+1, y, z);
    } else if (right_p) {
        return right_p->get(0, y, z);
    }

    return Block::AIR;
}

Block Chunk::top(size_t x, size_t y, size_t z) const {
    if (y < CHUNK_SIZE-1) {
        return get(x, y+1, z);
    } else if (top_p) {
        return top_p->get(x, 0, z);
    }

    return Block::AIR;
}
Block Chunk::bottom(size_t x, size_t y, size_t z) const {
    if (y > 0) {
        return get(x, y-1, z);
    } else if (bottom_p) {
        return bottom_p->get(x, CHUNK_SIZE-1, z);
    }

    return Block::AIR;
}
