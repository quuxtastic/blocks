#include "generators.hh"

#include <random>

#include "util/log.hh"

namespace terrain_gen {

void fill_all_chunks(int, int, int, Chunk& chunk) {
    chunk.fill(Block::DIRT);
}

void checkerboard(int, int, int, Chunk& chunk) {
    size_t count = 0;
    for (int i = 0; i < Chunk::CHUNK_SIZE; i+=2) {
        for (int j = 0; j < Chunk::CHUNK_SIZE; j+=2) {
            for (int k = 0; k < Chunk::CHUNK_SIZE; k+=2) {
                chunk._set(k, j, i, Block((count % 3) + 1));
                ++count;
            }
        }
    }
}

void pyramid(int, int, int, Chunk& chunk) {
    for (size_t y = 0; y < Chunk::CHUNK_SIZE/2; ++y) {
        for (size_t x = y; x < Chunk::CHUNK_SIZE-y; ++x) {
            for(size_t z = y; z < Chunk::CHUNK_SIZE-y; ++z) {
                chunk._set(x, y, z, Block(y%3 + 1));
            }
        }
    }
}

void random_pillars(int, int, int, Chunk& chunk) {
    static std::uniform_int_distribution<> dist(0, Chunk::CHUNK_SIZE-1);
    static std::mt19937 generator;

    for (size_t x = 0; x < Chunk::CHUNK_SIZE; ++x) {
        for (size_t z = 0; z < Chunk::CHUNK_SIZE; ++z) {
            size_t h = dist(generator);
            for (size_t y = 0; y < h+1; ++y) {
                chunk._set(x, y, z, Block::DIRT);
            }
        }
    }
}

void mammoth_cave(int, int q, int, Chunk& chunk) {
    static thread_local std::uniform_int_distribution<> stalag_dist(0, 5), h_dist(1, 20);
    static thread_local std::mt19937 generator;

    for (size_t x = 0; x < Chunk::CHUNK_SIZE; ++x) {
        for (size_t z = 0; z < Chunk::CHUNK_SIZE; ++z) {
            // set floor and ceiling
            if (q == 0) chunk._set(x, 0, z, Block::DIRT);
            if (q == 1) chunk._set(x, Chunk::CHUNK_SIZE-1, z, Block::DIRT);

            if (stalag_dist(generator) == 5) {
                size_t h = h_dist(generator);
                size_t y_min = q == 0 ? 0 : h;
                size_t y_max = q == 0 ? h-1 : Chunk::CHUNK_SIZE-1;
                for (size_t y = y_min; y <= y_max; ++y) {
                    chunk._set(x, y, z, Block::DIRT);
                }
            }
        }
    }
}

}
