#ifndef TERRAIN_CHUNK_HH_
#define TERRAIN_CHUNK_HH_ 1

#include <array>
#include <chrono>

#include "util/math.hh"
#include "util/error.hh"

class Block {
public:
    enum {
        TYPE_MASK = 0x00FF,
        FLAGS_MASK = 0xFF00,

        OCCLUDE_BIT = 0b1000000000000000,
        SOLID_BIT   = 0b0100000000000000,
        GRAVITY_BIT = 0b0010000000000000,
        LIQUID_BIT  = 0b0001000000000000,

        EMPTY   = 0,
        AIR     = 1,
        DIRT    = 2 | OCCLUDE_BIT | SOLID_BIT,
        GRASS   = 3 | OCCLUDE_BIT | SOLID_BIT,
        STONE   = 4 | OCCLUDE_BIT | SOLID_BIT,
        SAND    = 5 | OCCLUDE_BIT | SOLID_BIT | GRAVITY_BIT,
        WATER   = 6 | LIQUID_BIT
    };

    Block() : data(0) {}
    Block(std::uint16_t in) : data(in) {}
    operator std::uint16_t() { return data; }

    std::uint8_t type() const { return get_bits(data, TYPE_MASK); }
    void type(std::uint8_t type) { data = get_bits(data, FLAGS_MASK) | type; }

    std::uint8_t flags() const { return get_bits(data, FLAGS_MASK); }

    bool occludes() const { return get_bits(data, OCCLUDE_BIT); }
    bool solid() const { return get_bits(data, SOLID_BIT); }
    bool liquid() const { return get_bits(data, LIQUID_BIT); }

private:
    std::uint16_t data;

    friend bool operator==(const Block&, const Block&);
    friend bool operator==(const Block&, const Block&);
};

inline bool operator==(const Block& a, const Block& b) {
    return a.data == b.data;
}
inline bool operator!=(const Block& a, const Block& b) {
    return !(a == b);
}

class ChunkTerrain;

class Chunk {
public:
    enum {

        CHUNK_SIZE = 32,
        CUBE_SIZE = 4
    };

    struct coord_t {
        coord_t() : p(0), q(0), r(0) {}
        coord_t(int in_p, int in_q, int in_r) :
            p(in_p), q(in_q), r(in_r) {}

        int p, q, r;
    };

    Chunk(int p, int q, int r) : coord(p, q, r) { data.fill(Block::AIR); }
    virtual ~Chunk() {}

    Block get(size_t x, size_t y, size_t z) const {
        ASSERT(x < CHUNK_SIZE);
        ASSERT(y < CHUNK_SIZE);
        ASSERT(z < CHUNK_SIZE);
        return data[get_index(x, y, z)];
    }

    void set(size_t x, size_t y, size_t z, Block value) {
        if (terrain_ready()) {
            _set(x, y, z, value);
            on_set(x, y, z, value);
        }
    }

    void _set(size_t x, size_t y, size_t z, Block value) {
        ASSERT(x < CHUNK_SIZE);
        ASSERT(y < CHUNK_SIZE);
        ASSERT(z < CHUNK_SIZE);
        data[get_index(x, y, z)] = value;
    }

    void fill(Block value) { data.fill(value); }

    template<typename Callable>
    void traverse(Callable f,
            size_t x_off = 0, size_t y_off = 0, size_t z_off = 0,
            size_t x_max = CHUNK_SIZE, size_t y_max = CHUNK_SIZE,
            size_t z_max = CHUNK_SIZE) const {

        for (size_t z = z_off; z < z_max; ++z) {
            for (size_t y = y_off; y < y_max; ++y) {
                for (size_t x = x_off; x < x_max; ++x) {
                    f(get(x, y, z), x, y, z);
                }
            }
        }
    }

    Block front(size_t x, size_t y, size_t z) const;
    Block back(size_t x, size_t y, size_t z) const;
    Block left(size_t x, size_t y, size_t z) const;
    Block right(size_t x, size_t y, size_t z) const;
    Block top(size_t x, size_t y, size_t z) const;
    Block bottom(size_t x, size_t y, size_t z) const;

    Chunk* front() const    { return front_p; }
    void   front(Chunk* p)  { front_p = p; }
    Chunk* back() const     { return back_p; }
    void   back(Chunk* p)   { back_p = p; }
    Chunk* left() const     { return left_p; }
    void   left(Chunk* p)   { left_p = p; }
    Chunk* right() const    { return right_p; }
    void   right(Chunk* p)  { right_p = p; }
    Chunk* top() const      { return top_p; }
    void   top(Chunk* p)    { top_p = p; }
    Chunk* bottom() const   { return bottom_p; }
    void   bottom(Chunk* p) { bottom_p = p; }

    void touch() { last_touch_time = std::chrono::steady_clock::now(); }
    std::chrono::time_point<std::chrono::steady_clock>
    last_touched() { return last_touch_time; }

    bool keep_loaded() const { return get_bits(flags, KEEP_LOADED_BIT); }
    void keep_loaded(bool keep) { set_bits(flags, KEEP_LOADED_BIT, keep); }

    bool terrain_ready() const { return get_bits(flags, GEN_FINISHED_BIT); }
    void terrain_ready(bool b) { set_bits(flags, GEN_FINISHED_BIT, b); }

protected:

    virtual void post_load() {}
    virtual void pre_unload() {}
    virtual void on_set(size_t, size_t, size_t, Block) {}
    friend class ChunkTerrain;

    enum {
        KEEP_LOADED_BIT     = 0b10000000,
        GEN_FINISHED_BIT    = 0b01000000
    };

    std::uint8_t flags = 0;

    coord_t coord;

private:
    std::array<Block, CHUNK_SIZE * CHUNK_SIZE * CHUNK_SIZE> data;
    Chunk *front_p  = nullptr,
          *back_p   = nullptr,
          *left_p   = nullptr,
          *right_p  = nullptr,
          *top_p    = nullptr,
          *bottom_p = nullptr;

    size_t get_index(size_t x, size_t y, size_t z) const {
        return x + y*CHUNK_SIZE + z*CHUNK_SIZE*CHUNK_SIZE;
    }

    std::chrono::time_point<std::chrono::steady_clock> last_touch_time;
};

#endif
