#include "generators.hh"

#include <algorithm>
#include <cmath>
#include <numeric>
#include <glm/glm.hpp>
#include <glm/gtc/noise.hpp>

#include "util/error.hh"
#include "util/config.hh"

namespace terrain_gen {

double perlin(double nx, double ny) {
    // map [-1, 1] to [0, 1]
    return glm::perlin(glm::vec2(nx, ny)) / 2.0 + 0.5;
}

unsigned int PerlinGenerator::make_height_noise(unsigned int x, unsigned int y,
                                                int p, int r) {
    double nx =
        static_cast<double>(p * Chunk::CHUNK_SIZE + static_cast<int>(x)) /
        Chunk::CHUNK_SIZE;
    double ny =
        static_cast<double>(r * Chunk::CHUNK_SIZE + static_cast<int>(y)) /
        Chunk::CHUNK_SIZE;

    double noise = octaves[0] * perlin( 1*nx,  1*ny)
                 + octaves[1] * perlin( 2*nx,  2*ny)
                 + octaves[2] * perlin( 4*nx,  4*ny)
                 + octaves[3] * perlin( 8*nx,  8*ny)
                 + octaves[4] * perlin(16*nx, 16*ny)
                 + octaves[5] * perlin(32*nx, 32*ny);
    noise /= std::accumulate(octaves, octaves + 6, 0.0);
    noise = std::pow(noise, exponent);

    return std::round(noise * max_height);
}

void fill_column(unsigned int x, unsigned int z, unsigned int h, Chunk& chunk) {
    h = clamp(h, 1u, static_cast<unsigned int>(Chunk::CHUNK_SIZE));

    chunk._set(x, h-1, z, Block::GRASS);
    for (unsigned int y = 0; y < h-1; ++y) {
        chunk._set(x, y, z, Block::DIRT);
    }
}

void PerlinGenerator::fill_water(Chunk& chunk) {
    for (unsigned int y = 0; y < Chunk::CHUNK_SIZE; ++y) {
        for (unsigned int x = 0; x < Chunk::CHUNK_SIZE; ++x) {
            for (unsigned int h = 0; h < water_height; ++h) {
                if (chunk.get(x, h, y) == Block(Block::AIR)) {
                    chunk._set(x, h, y, Block::WATER);
                }
            }
        }
    }
}

void PerlinGenerator::generate_chunk(int p, int q, int r, Chunk& chunk) {
    for (unsigned int y = 0; y < Chunk::CHUNK_SIZE; ++y) {
        for (unsigned int x = 0; x < Chunk::CHUNK_SIZE; ++x) {
            unsigned int h = make_height_noise(x, y, p, r);
            if (q == 1 && h > Chunk::CHUNK_SIZE)
                fill_column(x, y, h-Chunk::CHUNK_SIZE, chunk);
            if (q == 0) fill_column(x, y, h, chunk);
        }
    }

    if (q == 0) fill_water(chunk);
}

void PerlinGenerator::setup(ConfigRegistry& config) {
    max_height = config.get_section("terrain", true)
        .get<unsigned int>("world_height", 2, true) * Chunk::CHUNK_SIZE;
    water_height = config.get_section("terrain")
        .get<unsigned int>("water_height", 20, true);
    exponent = config.get_section("terrain")
        .get<double>("exponent", 1.0, true);

    for (unsigned int i = 0; i < 6; ++i) {
        octaves[i] = config.get_section("terrain")
            .get<double>(fmt("octave%s", i), 0.5, true);
    }
}

}
