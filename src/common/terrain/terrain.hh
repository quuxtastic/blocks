#ifndef TERRAIN_TERRAIN_HH_
#define TERRAIN_TERRAIN_HH_ 1

#include <unordered_map>
#include <memory>
#include <array>
#include <chrono>
#include <glm/glm.hpp>

#include "chunk.hh"
#include "util/log.hh"
#include "util/math.hh"
#include "util/thread-pool.hh"

class ChunkTerrain {
public:
    ChunkTerrain() {}
    virtual ~ChunkTerrain();

    ChunkTerrain(const ChunkTerrain&) = delete;
    ChunkTerrain& operator=(const ChunkTerrain&) = delete;

    typedef std::function<void (int, int, int, Chunk&)> worldgen_func_t;
    void set_worldgen_func(worldgen_func_t f) { gen_chunk_fun = f; }

    typedef std::function<Chunk* (int, int, int)> allocate_chunk_func_t;
    typedef std::function<void (Chunk*)> free_chunk_func_t;
    void set_chunk_allocator(
            allocate_chunk_func_t in_alloc, free_chunk_func_t in_free) {
        allocate_chunk_fun = in_alloc;
        free_chunk_fun = in_free;
    }

    Chunk* get(int p, int q, int r);

    template<typename T, typename Callable>
    void traverse(int p, int q, int r, size_t radius, Callable f) {
        int sr = static_cast<int>(radius);
        for (int i = p-sr; i < p+sr+1; ++i) {
            for (int j = q-sr; j < q+sr+1; ++j) {
                for (int k = r-sr; k < r+sr+1; ++k) {
                    T* ptr = reinterpret_cast<T*>(get(i, j, k));
                    if (ptr) f(*ptr, i, j, k);
                }
            }
        }
    }

    void set_evict_timeout(std::chrono::steady_clock::duration timeout) {
        evict_timeout = timeout;
    }

    void set_world_height(int in_min, int in_max) {
        min_q = in_min; max_q = in_max;
    }

    bool is_valid_coord(int p, int q, int r);

    void do_frame();

private:
    struct coord_hasher {
        size_t operator()(const Chunk::coord_t& coord) const;
    };
    std::unordered_map<
        Chunk::coord_t,
        Chunk*,
        coord_hasher> chunk_map;

    Chunk* build_chunk(int p, int q, int r);
    void destruct_chunk(int p, int q, int r, Chunk* chunk);

    void set_neighbors(int p, int q, int r, Chunk* chunk, Chunk* newval);

    Chunk* find_chunk(int p, int q, int r);

    int min_q = 0, max_q = 0;

    worldgen_func_t gen_chunk_fun;
    allocate_chunk_func_t allocate_chunk_fun;
    free_chunk_func_t free_chunk_fun;

    std::chrono::steady_clock::duration evict_timeout;

    AsyncWorkPool terrain_gen_pool;
    std::unordered_map<int, Chunk*> worker_chunks;

    void garbage_collect();
};

inline bool operator==(
        const Chunk::coord_t& a,
        const Chunk::coord_t& b) {
    return a.p == b.p && a.q == b.q && a.r == b.r;
}

inline Chunk::coord_t world_to_chunk_coord(glm::vec3 vec) {
    const float f = Chunk::CHUNK_SIZE * Chunk::CUBE_SIZE;
    return Chunk::coord_t(
        quantize(vec.x, f), quantize(vec.y, f), quantize(vec.z, f));
}

inline std::pair<Chunk::coord_t, Chunk::coord_t>
        world_to_block_coord(glm::vec3 vec) {
    const float f = Chunk::CUBE_SIZE;

    auto chunk_coord = world_to_chunk_coord(vec);
    Chunk::coord_t block_coord(
        quantize(vec.x, f), quantize(vec.y, f), quantize(vec.z, f));

    if (vec.x >= 0.0f) {
        block_coord.p = block_coord.p % Chunk::CHUNK_SIZE;
    } else {
        block_coord.p = Chunk::CHUNK_SIZE - 1 -
            (std::abs(block_coord.p + 1) % Chunk::CHUNK_SIZE);
    }

    if (vec.y >= 0.0f) {
        block_coord.q = block_coord.q % Chunk::CHUNK_SIZE;
    } else {
        block_coord.q = Chunk::CHUNK_SIZE - 1 -
            (std::abs(block_coord.q + 1) % Chunk::CHUNK_SIZE);
    }

    if (vec.z >= 0.0f) {
        block_coord.r = block_coord.r % Chunk::CHUNK_SIZE;
    } else {
        block_coord.r = Chunk::CHUNK_SIZE - 1 -
            (std::abs(block_coord.r + 1) % Chunk::CHUNK_SIZE);
    }

    return std::make_pair(chunk_coord, block_coord);
}

inline glm::vec3 chunk_to_world_coord(Chunk::coord_t chunk) {
    const float f = Chunk::CHUNK_SIZE * Chunk::CUBE_SIZE;
    return glm::vec3(chunk.p, chunk.q, chunk.r) * f;
}

inline glm::vec3 block_to_world_coord(Chunk::coord_t chunk,
        Chunk::coord_t block) {
    auto chunk_vec = chunk_to_world_coord(chunk);
    auto block_vec = glm::vec3(block.p, block.q, block.r) *
        static_cast<float>(Chunk::CUBE_SIZE);

    return chunk_vec + block_vec;
}

#endif
