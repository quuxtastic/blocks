#ifndef TERRAIN_GENERATORS_HH_
#define TERRAIN_GENERATORS_HH_ 1

#include "chunk.hh"

class ConfigRegistry;

namespace terrain_gen {

void fill_all_chunks(int p, int q, int r, Chunk& chunk);
void checkerboard(int p, int q, int r, Chunk& chunk);
void pyramid(int p, int q, int r, Chunk& chunk);
void random_pillars(int p, int q, int r, Chunk& chunk);
void mammoth_cave(int p, int q, int r, Chunk& chunk);

class PerlinGenerator {
public:
    PerlinGenerator() {}

    void setup(ConfigRegistry& config);
    void generate_chunk(int p, int q, int r, Chunk& chunk);
private:
    unsigned int make_height_noise(unsigned int x, unsigned int y,
                                   int p, int r);
    void fill_water(Chunk& chunk);

    unsigned int max_height, water_height;
    double octaves[6];
    double exponent;
};

void perlin_hills(int p, int q, int r, Chunk& chunk);

}

#endif
