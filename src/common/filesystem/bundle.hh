#ifndef FILESYSTEM_BUNDLE_HH_
#define FILESYSTEM_BUNDLE_HH_ 1

#include <string>
#include <vector>

class Bundle {
public:
    Bundle(const std::string& in_name);

    Bundle(const Bundle&) = delete;
    Bundle& operator=(const Bundle&) = delete;

    void join_dir(const std::string& path);
    void join_archive(const std::string& path);
    void join_http(const std::string& request_url);

    bool path_exists(const std::string& path);

    std::vector<std::string> list_files(const std::string& path);

    std::vector<unsigned char> read(const std::string& path,
                                    size_t max_size = 0);
    size_t read(const std::string& path, std::vector<unsigned char>& out,
                size_t max_size = 0);
    size_t read(const std::string& path, void* out, size_t max_size);

    std::string read_as_sring(const std::string& path, size_t max_size = 0);

    template<typename Callable> void read_by_line(const std::string& path,
                                                  Callable fun);

    template<typename Callable> void stream_read(const std::string& path,
                                                 Callable fun);

    size_t write(const std::string& path, const std::vector<unsigned char>& in);
    size_t write(const std::string& path, const std::string& in);
    size_t write(const std::string& path, const void* data, size_t size);

private:
    template<typename Callable> void for_each_dir(Callable fun);
    template<typename Callable> void for_each_archive(Callable fun);
    template<typename Callable> void for_each_uri(Callable fun);
    std::string name;

    std::vector<std::string> dirs, archives, urls;
};

#endif
