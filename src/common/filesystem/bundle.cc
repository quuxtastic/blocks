#include "bundle.hh"

Bundle::Bundle(const std::string& in_name) : name(in_name) {}

void Bundle::join_dir(const std::string& path) {
    ASSERT(file_exists(path));
    dirs.push_back(path);
}

void Bundle::join_archive(const std::string& path) {
    ASSERT(file_exists(path));
    dirs.push_back(path);
}

void Bundle::join_http(const std::string& request_url) {
    urls.push_back(request_url);
}

bool Bundle::path_exists(const std::string& path) {

}

std::vector<std::string> list_files(const std::string& path) {
    //
}
