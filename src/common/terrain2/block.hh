#ifndef TERRAIN_BLOCK_HH_
#define TERRAIN_BLOCK_HH_ 1

class Block {
public:
    typedef std::uint16_t data_t;

    Block();

    data_t get_value() const;

    bool empty() const;

    bool occludes() const;
    bool transparent() const;

    bool solid() const;
    bool liquid() const;
    bool gravity() const;

    static const Block EMPTY;
    static const Block AIR;
    static const Block DIRT;
    static const Block GRASS;
    static const Block STONE;
    static const Block SAND;
    static const Block WATER;

private:
    enum {
        TYPE_MASK  = 0x00ff,
        FLAGS_MASK = 0xff00,

        OCCLUDE_BIT     = 0b1000000000000000,
        TRANSPARENT_BIT = 0b0100000000000000,
        SOLID_BIT       = 0b0010000000000000,
        GRAVITY_BIT     = 0b0001000000000000,
        LIQUID_BIT      = 0b0000100000000000
    };

    data_t data;

    friend bool operator==(const Block&, const Block&);
};

bool operator==(const Block& a, const Block& b);
bool operator!=(const Block& a, const Block& b);

#include "block.inl"

#endif
