#include <fstream>
#include <sstream>

#ifdef PLATFORM_LINUX
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#endif

#include "error.hh"

inline bool file_exists(const std::string& path) {
#ifdef PLATFORM_LINUX
    struct stat stats;
    int ret = stat(path.c_str(), &stats);
    return ret != -1 && S_ISREG(stats.st_mode);
#endif
}

inline std::string load_file_to_string(const std::string& path) {
    std::ifstream file(path, std::ios::in | std::ios::binary);
    if (!file) THROW("common", "Can't load text file '%s'", path);

    std::stringstream ss;
    ss << file.rdbuf();

    return ss.str();
}

template<typename Callable>
void load_file_by_line(const std::string& path, Callable fun) {
    std::ifstream file(path, std::ios::in | std::ios::binary);
    if (!file) THROW("common", "Can't load text file '%s'", path);

    std::string line;
    unsigned int line_num = 0;
    while(std::getline(file, line)) {
        fun(line, line_num++);
    }
}
