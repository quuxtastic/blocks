#ifndef UTIL_PERF_HH_
#define UTIL_PERF_HH_ 1

#include <chrono>
#include <tuple>

class PerfClock {
public:
    typedef std::chrono::high_resolution_clock::duration duration_t;
    typedef std::chrono::high_resolution_clock::time_point time_point_t;

    PerfClock();

    static time_point_t now();

    void start();
    duration_t elapsed();
private:
    time_point_t start_time;
};

template<typename Callable>
std::tuple<size_t, size_t, PerfClock::duration_t>
do_iterable_work(size_t total,
                 size_t cur,
                 PerfClock::duration_t max_allowed_time,
                 Callable fun);

#include "perf.inl"

#endif
