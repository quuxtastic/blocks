#include "config.hh"

#include <fstream>
#include <cstdlib>
#include <sstream>

#include "file.hh"
#include "string-utils.hh"
#include "log.hh"

static const char* DEFAULT_SECTION = "main";

void ConfigRegistry::load(const std::string& in_path) {
    if (!file_exists(in_path)) {
        INFO("config", "Configuration file %s does not exist", in_path);
        return;
    }

    std::string cur_section_name = DEFAULT_SECTION;
    load_file_by_line(in_path,
        [&] (const std::string& in_line, unsigned int line_num) {
            std::string line = trim(in_line);
            if (line[0] == '[') {
                std::string sec_name = trim(slice(line, 1, -1));
                if (sec_name.size() <= 0) {
                    WARN("config", "Can't parse line %s: '%s'",
                        line_num, line);
                    return;
                }
                cur_section_name = sec_name;
            } else {
                get_section(cur_section_name, true).load(line, line_num);
            }
        });
}
void ConfigRegistry::load() {
    load(default_conf_path());
}

void ConfigRegistry::save(const std::string& in_path) {
    std::ofstream out(in_path);

    for (auto cur : sections) {
        out << '[' << cur.first << "]\n";
        cur.second.save(out);
    }

    out.flush();
}
void ConfigRegistry::save() {
    save(default_conf_path());
}

bool ConfigRegistry::Section::exists(const std::string& key) {
    auto iter = keys.find(key);
    return iter != keys.end();
}

void ConfigRegistry::Section::clear(const std::string& key) {
    auto iter = keys.find(key);
    if (iter != keys.end() && !iter->second.locked) keys.erase(iter);
}

void ConfigRegistry::Section::save(std::ostream& out) {
    for (auto cur : keys) {
        out << cur.first << " = " << cur.second.value << "\n";
    }
}

void ConfigRegistry::Section::load(const std::string& line,
                                   unsigned int line_num) {
    auto parts = split(line, '=');
    if (parts.size() != 2) {
        WARN("config", "Can't parse line %s: '%s'",
            line_num, line);
        return;
    }

    set(trim(parts[0]), trim(parts[1]), false, false);
}

bool ConfigRegistry::section_exists(const std::string& section) {
    return sections.find(section) != sections.end();
}

ConfigRegistry::Section& ConfigRegistry::get_section(const std::string& section,
                                                     bool create) {
    auto iter = sections.find(section);
    if (iter != sections.end()) return iter->second;

    if (!create) THROW("config", "Missing section %s", section);

    return sections.insert({section, Section()}).first->second;
}

ConfigRegistry::Section& ConfigRegistry::default_section() {
    return get_section(DEFAULT_SECTION, true);
}

void ConfigRegistry::dump() {
    std::stringstream ss;
    for (auto cur_section : sections) {
        ss << "[" << cur_section.first << "] ";
        for (auto cur_key : cur_section.second.keys) {
            ss << cur_key.first << " = " << cur_key.second.value << ", ";
        }
    }

    INFO("config", "Dump: %s", ss.str());
}

#define STRINGIZE(s) #s

std::string ConfigRegistry::default_conf_path() {
    std::stringstream ss;
#ifdef PLATFORM_LINUX
    const char* xdg_config = std::getenv("XDG_CONFIG_HOME");
    if (xdg_config) {
        ss << xdg_config << "/";
    } else {
        const char* home = std::getenv("HOME");
        ss << home << "/" << ".config/";
    }

    ss << STRINGIZE(APPLICATION_NAME) "/settings.conf";
#else
#error No implementation for this platform
#endif

    return ss.str();
}

#undef STRINGIZE
