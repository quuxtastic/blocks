#include <cassert>

#include "format.hh"
#include "log.hh"

template<typename... Args>
Error::Error(const char* str, Args&&... args) throw() :
    error_msg(fmt(str, args...)), is_clone(false) {}

inline const char* Error::what() const throw() {
    return error_msg.c_str();
}

inline Error* Error::clone() const {
    Error* p = new Error(*this);
    p->is_clone = true;
    return p;
}

inline bool Error::destroy() {
    if (is_clone) {
        delete this;
        return true;
    } else {
        return false;
    }
}

template<typename... Args>
void throw_exception(const char* component,
        const char* func, const char* file, int line,
        const char* fmt_str, Args&&... args) {
    msglog::write(msglog::MSG_ERROR, func, file, line, component,
        fmt_str, args...);
    throw Error(fmt_str, args...);
}

#define THROW_DETAIL_(component, ...) throw_exception(component, \
    __PRETTY_FUNCTION__, __FILE__, __LINE__, __VA_ARGS__)

#define ASSERT_DETAIL_(expr) assert(expr)
