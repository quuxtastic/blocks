#ifndef UTIL_STATE_HH_
#define UTIL_STATE_HH_ 1

#include <unordered_map>
#include <functional>

class StateMachine {
public:
    typedef std::function<int (int)> state_func_t;

    void add(int state, state_func_t func);

    void run(int start);

    int current() { return current_state; }
private:
    std::unordered_map<int, state_func_t> state_map;

    int current_state = 0;
};

#endif
