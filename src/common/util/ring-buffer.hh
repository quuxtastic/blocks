#ifndef UTIL_RING_BUFFER_HH_
#define UTIL_RING_BUFFER_HH_ 1

#include <atomic>
#include <array>

template<typename T, size_t BufferSize>
class RingBufferSPSC {
public:
    RingBufferSPSC() : read(0), write(0) {}

    RingBufferSPSC(const RingBufferSPSC&) = delete;
    RingBufferSPSC& operator=(const RingBufferSPSC&) = delete;

    bool try_push(const T& in) {
        const auto cur_tail = write.load();
        const auto next_tail = increment(cur_tail);

        if (next_tail != read.load(std::memory_order_acquire)) {
            buffer[cur_tail] = in;
            write.store(next_tail, std::memory_order_release);

            return true;
        }

        return false;
    }

    void push(const T& in) {
        while (!try_push(in));
    }

    bool try_pop(T& out) {
        auto cur_head = read.load();
        if (head == write.load(std::memory_order_acquire)) {
            return false;
        }

        out = buffer[cur_head];
        read.store(increment(cur_head), std::memory_order_release);

        return true;
    }

    void pop(T& out) {
        while (!try_pop(out));
    }

private:
    std::array<T, BufferSize> buffer;
    std::atomic<size_t> read, write;

    size_t increment(size_t cur) {
        return (cur + 1) % buffer.size();
    }
};

#endif
