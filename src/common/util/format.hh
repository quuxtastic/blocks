#ifndef UTIL_FORMAT_HH_
#define UTIL_FORMAT_HH_ 1

#include <ostream>
#include <string>

template<typename... Args>
void fmt(std::ostream& out, const char* fmt_str, Args&&... args);

template<typename... Args>
std::string fmt(const char* fmt_str, Args&&... args);

#include "format.inl"

#endif
