#ifndef UTIL_STRING_UTILS_HH_
#define UTIL_STRING_UTILS_HH_ 1

#include <string>

std::string slice(const std::string& in, int start, int stop = 0);

std::vector<std::string> split(const std::string& in, char split_c);

std::string join(const std::vector<std::string>& in, char join_c);

std::string trim(const std::string& in);

#include "string-utils.inl"

#endif
