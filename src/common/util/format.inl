#include <sstream>

namespace detail {

inline void fmt(std::ostream& out, const char* fs) {
    out << fs;
}

template<typename Arg, typename... Args>
void fmt(std::ostream& out, const char* fs, const Arg& arg, Args&&... args) {
    for (; *fs; ++fs) {
        if (*fs == '%') {
            fs++;
            if (*fs != '%') {
                out << arg;
                fmt(out, fs + 1, args...);
                return;
            }
        }

        out << *fs;
    }
}

template<typename... Args>
std::string fmt(const char* fs, Args&&... args) {
    std::stringstream ss;
    fmt(ss, fs, args...);
    return ss.str();
}

}

template<typename... Args>
void fmt(std::ostream& out, const char* fmt_str, Args&&... args) {
    detail::fmt(out, fmt_str, args...);
}

template<typename... Args>
std::string fmt(const char* fmt_str, Args&&... args) {
    return detail::fmt(fmt_str, args...);
}
