#include <algorithm>

#include <glm/gtc/matrix_access.hpp>

template<typename T>
T clamp(T value, T min, T max) {
    if (value < min) return min;
    if (value > max) return max;

    return value;
}

template<typename T>
T quantize(T value, T q) {
    return std::floor(std::floor(value) / q);
}

template<typename T, typename U>
bool get_bits(T flags, U mask) {
    return flags & mask;
}

template<typename T, typename U>
void set_bits(T& flags, U mask, bool enable) {
    if (enable) {
        flags |= mask;
    } else {
        flags &= ~mask;
    }
}

// http://dmauro.com/post/77011214305/a-hashing-function-for-x-y-z-coordinates
inline size_t hash_xyz(int x, int y, int z) {
    x = x >= 0 ? x * 2 : -2 * x - 1;
    y = y >= 0 ? y * 2 : -2 * y - 1;
    z = z >= 0 ? z * 2 : -2 * z - 1;

    size_t max_val = std::max(x, std::max(y, z));
    size_t hash = max_val * max_val * max_val + (2 * max_val * z) + z;

    if (max_val == static_cast<size_t>(z)) {
        hash += std::max(x, y) * std::max(x, y);
    }
    if (y >= x) {
        hash += x + y;
    } else {
        hash += y;
    }

    return hash;
}

inline float distance(const glm::vec4& normalized_plane, glm::vec3 point) {
    return normalized_plane.x * point.x + normalized_plane.y * point.y +
        normalized_plane.z * point.z + normalized_plane.w;
}

inline int classify(const glm::vec4& plane, glm::vec3 point) {
    float d = plane.x * point.x + plane.y * point.y + plane.z * point.z +
        plane.w;

    if (d < 0) return -1;
    if (d > 0) return 1;
    return 0;
}

inline Frustum frustum_from_mvp_matrix(const glm::mat4& mat) {
    Frustum out;

    out.left   = glm::column(mat, 3) + glm::column(mat, 0);
    out.right  = glm::column(mat, 3) - glm::column(mat, 0);
    out.top    = glm::column(mat, 3) - glm::column(mat, 1);
    out.bottom = glm::column(mat, 3) + glm::column(mat, 1);
    out.far    = glm::column(mat, 3) - glm::column(mat, 2);
    out.near   = glm::column(mat, 3) + glm::column(mat, 2);

    return out;
}
