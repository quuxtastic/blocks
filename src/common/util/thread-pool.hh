#ifndef UTIL_THREAD_POOL_H_
#define UTIL_THREAD_POOL_H_ 1

#include <future>
#include <functional>
#include <list>
#include <map>

class AsyncWorkPool {
public:
    AsyncWorkPool() {}
    virtual ~AsyncWorkPool() {
        flush();
    }

    AsyncWorkPool(const AsyncWorkPool&) = delete;
    AsyncWorkPool& operator=(const AsyncWorkPool&) = delete;

    template<typename Callable, typename... Args>
    int add_work(Callable in_fun, Args&&... args) {
        int work_id = ++next_id;

        auto future = std::async(std::launch::async, in_fun, work_id, args...);
        work_items.insert(std::make_pair(work_id, std::move(future)));

        return work_id;
    }

    template<typename Callable>
    void poll(Callable fun) {
        for (auto cur = work_items.begin(); cur != work_items.end(); ) {
            if (poll(cur->second)) {
                int id = cur->first;
                cur = work_items.erase(cur);

                fun(id);
            } else {
                ++cur;
            }
        }
    }

    template<typename Callable>
    bool poll(int id, Callable fun) {
        auto cur = work_items.find(id);
        if (cur != work_items.end() && poll(cur->second)) {
            work_items.erase(cur);

            fun(id);
            return true;
        }

        return false;
    }

    size_t queue_count() const { return work_items.size(); }

    void flush();

private:
    std::map<int, std::future<void>> work_items;

    int next_id = 0;

    bool poll(std::future<void>& work_item);
};

#endif
