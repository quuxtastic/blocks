#ifndef UTIL_MATH_HH_
#define UTIL_MATH_HH_

#include <glm/glm.hpp>

template<typename T>
T clamp(T value, T min, T max);

template<typename T>
T quantize(T value, T q);

template<typename T, typename U>
bool get_bits(T flags, U mask);

template<typename T, typename U>
void set_bits(T& flags, U mask, bool enable);

size_t hash_xyz(int x, int y, int z);

float distance(const glm::vec4& normalized_plane, glm::vec3 point);

int classify(const glm::vec4& plane, glm::vec3 point);

struct Frustum {
    glm::vec4 near, far, top, bottom, right, left;
};

Frustum frustum_from_mvp_matrix(const glm::mat4& mat);

#include "math.inl"

#endif
