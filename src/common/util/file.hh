#ifndef UTIL_FILE_HH_
#define UTIL_FILE_HH_ 1

#include <string>

bool file_exists(const std::string& path);

std::string load_file_to_string(const std::string& path);

template<typename Callable>
void load_file_by_line(const std::string& path, Callable fun);

#include "file.inl"

#endif
