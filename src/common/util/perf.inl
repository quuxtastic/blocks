PerfClock::PerfClock() {
    start();
}

PerfClock::time_point_t PerfClock::now() {
    return std::chrono::high_resolution_clock::now();
}

void PerfClock::start() {
    start_time = now();
}

PerfClock::duration_t PerfClock::elapsed() {
    return now() - start_time;
}

template<typename Callable>
std::tuple<size_t, size_t, PerfClock::duration_t>
do_iterable_work(size_t total, size_t cur,
        PerfClock::duration_t max_allowed_time, Callable fun) {
    PerfClock clock;
    while(total > 0 && cur < total && clock.elapsed() < max_allowed_time) {
        total -= fun(cur++);
    }

    return std::make_tuple(total, cur, clock.elapsed());
}
