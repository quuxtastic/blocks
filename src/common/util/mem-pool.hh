#ifndef UTIL_MEM_POOL_HH_
#define UTIL_MEM_POOL_HH_ 1

#include <vector>
#include <queue>
#include <utility>
#include <cstddef>
#include <cstdint>

namespace detail {

class PagedPoolAllocator {
public:
    PagedPoolAllocator(size_t in_block_size, size_t in_alignment);
    virtual ~PagedPoolAllocator();

    std::pair<void*, std::uint8_t> allocate_block();
    void free_block(void* ptr, std::uint8_t page);

    size_t page_count() const { return pages.size(); }
    size_t used_blocks() const;
    float mem_usage() const;

private:
    struct page_t {
        void* page_ptr;
        void* tail;
        size_t size;

        std::queue<void*> free_blocks;
        size_t used_blocks = 0;
    };
    std::vector<page_t> pages;

    void* allocate_from_page(page_t& page);

    size_t block_size, page_size;

    static const size_t MAX_PAGES = 256;
};

}

template<typename T>
class MemoryPool {
private:
    struct pool_rec_t {
        std::uint8_t page;
        T data;

        template<typename... Args>
        pool_rec_t(std::uint8_t in_page, Args&&... args) :
            page(in_page), data(args...) {}
    };

    detail::PagedPoolAllocator allocator;
public:
    MemoryPool() : allocator(sizeof(pool_rec_t), alignof(pool_rec_t)) {}

    template<typename... Args>
    T* allocate(Args&&... args) {
        auto rec = allocator.allocate_block();
        return new (rec.first) pool_rec_t(rec.second, args...);
    }

    void free(void* ptr) {
        pool_rec_t* rec = reinterpret_cast<pool_rec_t*>(ptr);
        allocator.free_block(rec.data, rec.page);
    }

    size_t page_count() const { return allocator.page_count(); }
    size_t used_blocks() const { return allocator.used_blocks(); }
    size_t mem_usage() const { return allocator.mem_usage(); }
};

#endif
