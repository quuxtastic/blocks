#include <sstream>

#include "error.hh"
#include "string-utils.hh"

namespace detail {

template<typename T> T string_to_value(const std::string& s) {
    return T(s);
}

template<> inline std::string
string_to_value<std::string>(const std::string& s) {
    return s;
}
template<> inline int
string_to_value<int>(const std::string& s) {
    return std::stoi(s);
}
template<> inline unsigned int
string_to_value<unsigned int>(const std::string& s) {
    return std::stoi(s);
}
template<> inline float
string_to_value<float>(const std::string& s) {
    return std::stof(s);
}
template<> inline double
string_to_value<double>(const std::string& s) {
    return std::stod(s);
}

template<typename T> std::string to_string(const T& val) {
    return std::to_string(val);
}

inline std::string to_string(const std::string& val) {
    return val;
}

inline std::string to_string(const char* val) {
    return std::string(val);
}

}

template<typename T>
T ConfigRegistry::Section::get(const std::string& key) {
    auto iter = keys.find(key);
    if (iter == keys.end()) THROW("config", "Missing key '%s'", key);
    return detail::string_to_value<T>(iter->second.value);
}

template<typename T>
T ConfigRegistry::Section::get(const std::string& key, const T& def_val,
                               bool set_default) {
    auto iter = keys.find(key);
    if (iter != keys.end()) {
        return detail::string_to_value<T>(iter->second.value);
    }

    if (set_default) {
        keys.insert({key, {detail::to_string(def_val), false}});
    }

    return def_val;
}

template<typename T>
void ConfigRegistry::Section::set(const std::string& key, const T& val,
                                  bool set_as_default, bool lock) {
    auto iter = keys.find(key);

    if (iter != keys.end()) {
        if (!set_as_default && !iter->second.locked) {
            iter->second = {detail::to_string(val), lock};
        }
    } else {
        keys.insert({key, {detail::to_string(val), lock}});
    }
}
