#include "mem-pool.hh"

#include <memory>
#include <unistd.h>
#include <sys/mman.h>

detail::PagedPoolAllocator::PagedPoolAllocator(size_t in_block_size,
        size_t in_alignment) {
    page_size = sysconf(_SC_PAGESIZE);
    block_size = in_block_size;
    if (block_size > page_size) {
        // sanity check
        THROW("mempool", "Block size %d > system page size %d",
              block_size, page_size);
    }

    block_alignment = in_alignment;
}

detail::PagedPoolAllocator::~PagedPoolAllocator() {
    for (auto cur : pages) {
        munmap(cur.page_ptr, page_size);
    }
}

void* detail::PagedPoolAllocator::allocate_from_page(
        detail::PagedPoolAllocator::page_t& page) {

    if (!page.free_blocks.empty()) {
        void* ret = page.free_blocks.front();
        page.free_blocks.pop();
        ++page.used_blocks;

        return ret;
    }

    if (std::align(block_alignment, block_size, page.tail, page.size)) {
        if (page.size > block_size) {
            void* ret = page.tail;

            page.tail =
                reinterpret_cast<unsigned char*>(page.tail) + block_size;
            page.size -= block_size;
            ++page.used_blocks;

            return ret;
        }
    }

    return nullptr;
}

std::pair<void*, std::uint8_t> PagedPoolAllocator::allocate_block() {
    for (std::uint8_t i = 0; i < pages.size(); ++i) {
        void* ret = allocate_from_page(pages[i]);
        if (ret) return std::make_pair(ret, i);
    }

    // if we get here, no pages with free space
    if (pages.size() >= MAX_PAGES) {
        THROW("allocator", "Ran out of pages!");
    }

    void* new_page_ptr = mmap(nullptr, page_size,
        PROT_READ | PROT_WRITE,
        MAP_PRIVATE | MAP_ANONYMOUS,
        0, 0);
    if (new_page_ptr == MMAP_FAILED) {
        THROW("allocator", "mmap failed!");
    }

    page_t new_page = {new_page_ptr, new_page_ptr, page_size};
    pages.push_back(new_page);

    return std::make_pair(
        allocate_from_page(pages[pages.size()-1]),
        pages.size()-1);
}

void PagedPoolAllocator::free_block(void* ptr, std::uint8_t page) {
    if (page > pages.size()-1) {
        THROW("allocator", "Invalid page number");
    }

    pages[page].free_blocks.push_back(ptr);
    std::memset(ptr, 0, page_size);
    --pages[page].used_blocks;
}

size_t PagedPoolAllocator::used_blocks() const {
    size_t count = 0;
    for (auto cur : pages) {
        count += cur.used_blocks;
    }

    return count;
}

float PagedPoolAllocator::mem_usage() const {
    return (used_blocks() * block_size) / (MAX_PAGES * page_size);
}
