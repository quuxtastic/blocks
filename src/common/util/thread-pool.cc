#include "thread-pool.hh"

#include <chrono>

bool AsyncWorkPool::poll(std::future<void>& work_item) {
    return work_item.wait_for(std::chrono::milliseconds(0)) ==
        std::future_status::ready;
}

void AsyncWorkPool::flush() {
    for (auto cur = work_items.begin(); cur != work_items.end(); ++cur) {
        cur->second.wait();
    }
    work_items.clear();
}
