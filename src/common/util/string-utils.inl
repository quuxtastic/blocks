#include <sstream>

inline std::string slice(const std::string& in, int start, int stop) {
    if (start < 0) start = in.size() + start;
    if (stop < 0) stop = in.size() + stop;

    return in.substr(start, (stop - start));
}

inline std::vector<std::string> split(const std::string& in, char split_c) {
    std::vector<std::string> out;
    size_t start = 0;
    for (size_t cur = 0; cur < in.size(); ++cur) {
        if (in[cur] == split_c && cur != start) {
            out.push_back(slice(in, start, cur));
            start = cur + 1;
        }
    }
    if (start < in.size()) {
        out.push_back(slice(in, start));
    }

    return out;
}

inline std::string join(const std::vector<std::string>& in, char join_c) {
    std::stringstream out;

    bool first = true;
    for (auto cur : in) {
        if (!first) out << join_c;
        first = false;

        out << cur;
    }

    return out.str();
}

inline std::string trim(const std::string& in) {
    size_t start_off = 0, end_off = in.size()-1;
    for (; std::isspace(in[start_off]) && ++start_off < in.size(); );
    for (; std::isspace(in[end_off]) && --end_off > 0; );

    return slice(in, start_off, end_off+1);
}
