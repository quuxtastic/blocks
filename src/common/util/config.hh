#ifndef UTIL_CONFIG_HH_
#define UTIL_CONFIG_HH_ 1

#include <string>
#include <map>
#include <vector>
#include <ostream>

class ConfigRegistry {
public:
    void load();
    void load(const std::string& in_path);
    void save();
    void save(const std::string& in_path);

    class Section {
    public:
        bool exists(const std::string& key);

        template<typename T>
        T get(const std::string& key);
        template<typename T>
        T get(const std::string& key, const T& def_val,
              bool set_default = false);

        template<typename T>
        void set(const std::string& key, const T& val,
                 bool set_as_default = false, bool lock = false);

        void clear(const std::string& key);

    private:
        struct keyval_t {
            std::string value;
            bool locked;
        };
        std::map<std::string, keyval_t> keys;

        void save(std::ostream& out);
        void load(const std::string& line, unsigned int line_num);

        friend ConfigRegistry;
    };

    bool section_exists(const std::string& section);

    Section& get_section(const std::string& section, bool create = false);
    Section& default_section();

    void dump();

private:
    std::map<std::string, Section> sections;

    static std::string default_conf_path();
};

#include "config.inl"

#endif
