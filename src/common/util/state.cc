#include "state.hh"

#include "util/log.hh"

void StateMachine::add(int state, StateMachine::state_func_t func) {
    // state 0 can never be changed
    if (state != 0) state_map[state] = func;
}

void StateMachine::run(int start) {
    current_state = start;
    int prev_state = 0;

    while(current_state != 0) {
        TRACE("state", "cur: %s prev: %s", current_state, prev_state);
        int n = state_map[current_state](prev_state);
        prev_state = current_state;
        current_state = n;
    }
}
