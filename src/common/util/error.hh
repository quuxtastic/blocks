#ifndef UTIL_ERROR_HH_
#define UTIL_ERROR_HH_ 1

#include <string>
#include <exception>

class Error : public std::exception {
public:
    template<typename... Args>
    explicit Error(const char* s, Args&&... args) throw();
    virtual ~Error() {}

    virtual const char* what() const throw();

    virtual Error* clone() const;
    virtual bool destroy();

private:
    std::string error_msg;
    bool is_clone;
};

template<typename... Args>
void throw_exception(const char* component,
    const char* func, const char* file, int line,
    const char* s, Args&&... args);

#define THROW(comp, ...) THROW_DETAIL_(comp, __VA_ARGS__)

#define ASSERT(expr) ASSERT_DETAIL_(expr)

#include "error.inl"

#endif
