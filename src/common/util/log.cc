#include "log.hh"

#include <iomanip>
#include <fstream>
#include <ctime>
#include <string>
#include <iostream>
#include <functional>
#include <time.h> // for localtime_r
#include <execinfo.h> // for backtrace

namespace msglog {

static std::ostream* out = &(std::cout);

static const char* translate_log_message_type(MessageType type) {
    switch(type) {
    case MSG_ERROR:
        return "###";
    case MSG_WARN:
        return "!!!";
    case MSG_INFO:
        return "+++";
    case MSG_STATUS:
        return "===";
    case MSG_DEBUG:
        return "---";
    case MSG_TRACE:
        return "~~~";
    default:
        return "???";
    }
}

void setup(const char* filename) {
    static std::ofstream file;
    file.open("./" + std::string(filename) + ".log");

    out = &file;
}

void setup() {
    out = &(std::cout);
}

void write(MessageType type,
        const char* func, const char* file, int line,
        const char* component,
        const std::string& s) {

    std::time_t raw_time = std::time(nullptr);
    std::tm timeinfo;
    localtime_r(&raw_time, &timeinfo);
    char time_buf[32];
    std::strftime(time_buf, sizeof(time_buf), "%D %T", &timeinfo);

    *out    << translate_log_message_type(type) << " " << time_buf
            << " " << func << ":" << file << ":" << line << std::endl;
    *out    << translate_log_message_type(type)
            << " [" << component << "] " << s << std::endl;

    if (type == MSG_ERROR) write_backtrace();
}

void write_backtrace() {
    std::array<void*, 15> symbols;

    std::size_t sym_count = backtrace(symbols.data(), symbols.size());
    std::unique_ptr<char*, std::function<void(void*)>> strings(
        backtrace_symbols(symbols.data(), sym_count),
        free);

    for (std::size_t i = 0; i < sym_count; ++i) {
        *out << "\t" << strings.get()[i] << std::endl;
    }
}

}
