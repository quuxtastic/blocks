#include <string>

#include "format.hh"

namespace msglog {

template<typename... Args>
void write(MessageType type,
        const char* func, const char* file, int line,
        const char* component,
        const char* fmt_str, Args&&... args) {
    ::msglog::write(type, func, file, line, component, fmt(fmt_str, args...));
}

}

#define LOG_WRITE_DETAIL_(type, comp, ...) ::msglog::write(type, \
    __PRETTY_FUNCTION__, __FILE__, __LINE__, comp, __VA_ARGS__)

#define ERR_DETAIL_(...) LOG_WRITE_DETAIL_(::msglog::MSG_ERROR, __VA_ARGS__)
#define WARN_DETAIL_(...) LOG_WRITE_DETAIL_(::msglog::MSG_WARN, __VA_ARGS__)
#define INFO_DETAIL_(...) LOG_WRITE_DETAIL_(::msglog::MSG_INFO, __VA_ARGS__)
#define STATUS_DETAIL_(...) LOG_WRITE_DETAIL_(::msglog::MSG_INFO, __VA_ARGS__)

#ifdef DEBUG
#define DBG_DETAIL_(...) LOG_WRITE_DETAIL_(::msglog::MSG_DEBUG, __VA_ARGS__)
#define TRACE_DETAIL_(...) LOG_WRITE_DETAIL_(::msglog::MSG_TRACE, __VA_ARGS__)
#else
#define DBG_DETAIL_(...)
#define TRACE_DETAIL_(...)
#endif
