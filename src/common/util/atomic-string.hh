#ifndef UTIL_ATOMIC_STRING_HH_
#define UTIL_ATOMIC_STRING_HH_ 1

class AtomicString {
public:
    AtomicString() : data(nullptr), offset(0), len(0) {}
    AtomciString(const char* in_str) : offset(0) {
        alloc(std::strlen(in_str));
        len = data->len;

        std::strcpy(data->ptr, in_str);
    }
    AtomciString(const std::string& in_str) : offset(0) {
        alloc(in_str.size());
        len = in_str.size();

        std::copy(data->ptr, in_str.begin(), in_str.end());
    }

    AtomicString(const AtomicString& a, const AtomicString& b) {
        alloc(a.size() + b.size());

        std::strcpy(data->ptr, a);
        std::strcpy(data->ptr, b + a.size());
    }

    ~AtomicString() {
        free();
    }

    AtomicString(const AtomicString& in) :
            data(in.data), len(in.len), offset(in.offset) {
        if (data) ++data->ref_count;
    }

    AtomicString& operator=(const AtomicString& in) {
        ++in.data->ref_count;

        free();

        data = in.data;
        offset = in.offset;
        len = in.len;

        return *this;
    }

    char operator[](size_t index) const {
        return *(data->ptr + offset + index);
    }

    size_t size() const {
        return len;
    }

    AtomicString slice(int start, int stop) const {
        return AtomicString(*this, start, stop);
    }

    operator std::string() const {
        return std::string(data->ptr + offset);
    }

    operator const char*() const {
        return data->ptr + offset;
    }

private:
    void alloc(size_t len) {
        data = new data_t;
        data->ptr = new char[len + 1];
        data->len = len;
        data->ref_count = 1;
    }
    void free() {
        if (data && --data->ref_count <= 0) {
            delete data->ptr;
            delete data;
            data = nullptr;
        }
    }

    struct data_t {
        char* ptr;
        size_t len;
        size_t ref_count;
    };

    data_t* data;
    size_t offset, len;
};

AtomicString operator+(const AtomicString&& a, const AtomicString&& b) {
    return AtomicString(a, b);
}

#endif
