#ifndef UTIL_LOG_HH_
#define UTIL_LOG_HH_ 1

#include <string>

#include "format.hh"

namespace msglog {

void setup(const char* filename);
void setup();

enum MessageType {
    MSG_ERROR,
    MSG_WARN,
    MSG_INFO,
    MSG_STATUS,
    MSG_DEBUG,
    MSG_TRACE,
    MSG_OTHER
};

void write(MessageType type,
    const char* func, const char* file, int line,
    const char* component,
    const std::string& s);

template<typename... Args>
void write(MessageType type,
        const char* func, const char* file, int line,
        const char* component,
        const char* fs, Args&&... args);

void write_backtrace();

}

#define LOG_WRITE(type, comp, ...) LOG_WRITE_DETAIL_(type, comp, __VA_ARGS__)

#define ERR(...)    ERR_DETAIL_(__VA_ARGS__)
#define WARN(...)   WARN_DETAIL_(__VA_ARGS__)
#define INFO(...)   INFO_DETAIL_(__VA_ARGS__)
#define STATUS(...) STATUS_DETAIL_(__VA_ARGS__)
#define DBG(...)    DBG_DETAIL_(__VA_ARGS__)
#define TRACE(...)  TRACE_DETAIL_(__VA_ARGS__)

#include "log.inl"

#endif
