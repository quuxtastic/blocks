#version 430 core

in vec3 FragPos;
in vec3 Normal;
in vec2 TexCoord;
in uint TexIndex;

out vec4 color;

uniform sampler2DArray textures;

void main() {
    color = texture(textures, vec3(TexCoord.xy, TexIndex));
}
