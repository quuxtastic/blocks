#version 430 core

in vec4 PositionNormal;
in vec4 TextureData;

out vec3 FragPos;
out vec3 Normal;
out vec2 TexCoord;
out uint TexIndex;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

const vec3 normals[6] = vec3[6](
    vec3( 0.0,  0.0, -1.0), // north
    vec3( 1.0,  0.0,  0.0), // east
    vec3( 0.0,  0.0,  1.0), // south
    vec3(-1.0,  0.0,  0.0), // west
    vec3( 0.0,  1.0,  0.0), // up
    vec3( 0.0, -1.0,  0.0)  // down
);

void main() {
    vec4 model_pos = model * vec4(PositionNormal.xyz, 1.0);
    FragPosition = vec3(model_pos);

    Normal = normals[uint(PositionNormal.w)];

    TexCoord = TextureData.xy / 255.0;
    TexIndex = uint(TextureData.z);

    gl_Position = model_pos * view * projection;
}
