if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU" OR
    "${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang")
    set(c_flags
        "-Wall -Wextra -Werror -std=c++11 -g -DDEBUG -D_DEBUG -DPLATFORM_LINUX")
elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "MSVC")
    # Disabled warnings:
    #   4512: Assignment operator could not be generated
    #   <http://msdn.microsoft.com/en-us/library/hsyx7kbz.aspx>
    set(c_flags "/W4 /wd4512 /WX /EHsc")
endif()
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${c_flags}")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${c_flags}")

add_subdirectory(common)
add_subdirectory(server)
add_subdirectory(client)

add_subdirectory(demos)
