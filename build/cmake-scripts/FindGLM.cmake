# Work-around for broken glm CMake package definition file

IF(glm_INCLUDE_DIR)
    SET(glm_FIND_QUIETLY TRUE)
ENDIF(glm_INCLUDE_DIR)

FIND_PATH(glm_INCLUDE_DIR glm.hpp PATH_SUFFIXES include/glm include)

SET(glm_INCLUDE_DIR "${glm_INCLUDE_DIR}")

INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(GLM DEFAULT_MSG glm_INCLUDE_DIR)
